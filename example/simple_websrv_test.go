package example

import (
	"bytes"
	"io"
	"log"
	"net"
	"strings"
	"testing"

	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/errors"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/profile"
	lkeap "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/lkeap/v20240522"
)

func TestTcp1(t *testing.T) {

	connFunc := func(conn net.Conn) {
		stringBuilder := strings.Builder{}
		buffer := bytes.NewBuffer(make([]byte, 5))
		for {
			if _, err := conn.Read(buffer.Bytes()); err != nil {
				log.Println(err)
				if err == io.EOF {
					log.Println("client closed connection.")
					break
				}
			} else {
				stringBuilder.WriteString(buffer.String())
			}
		}
		log.Println(stringBuilder.String())
		t.Log(stringBuilder.String())
	}

	if tcpListener, err := net.ListenTCP("tcp", &net.TCPAddr{IP: net.ParseIP("127.0.0.1"), Port: 8000, Zone: ""}); err != nil {
		t.Error(err)
	} else {
		defer tcpListener.Close()
		for {
			if conn, err := tcpListener.Accept(); err != nil {
				log.Fatal(err)
			} else {
				go connFunc(conn)
			}
		}
	}
}

func TestOssSvgaFile(t *testing.T) {

}

func TestTencentDeepSeekV3(t *testing.T) {
	// 硬编码密钥到代码中有可能随代码泄露而暴露，有安全隐患，并不推荐。
	// 为了保护密钥安全，建议将密钥设置在环境变量中或者配置文件中，请参考本文凭证管理章节。

	// 1. 可以使用 NewCredential 来创建一个普通的密钥
	credential := common.NewCredential(
		"AKIDAAxUnsiWGxVp4FE2rW0NYaVL6iBYllX6",
		"RebM8teDYijXO9iCWp14wc7axAC1d7QX",
	)
	cpf := profile.NewClientProfile()
	cpf.HttpProfile.Endpoint = "lkeap.tencentcloudapi.com"
	t.Log(credential, cpf)
	client, err := lkeap.NewClient(credential, "ap-guangzhou", cpf)
	if err != nil {
		t.Error("lkeap.NewClient err: ", err.Error())
		return
	}
	// lkeap.NewClient(credential, "ap-guangzhou")
	// 实例化一个请求对象,每个接口都会对应一个request对象
	request := lkeap.NewChatCompletionsRequest()

	request.Model = common.StringPtr("deepseek-v3")
	request.Messages = []*lkeap.Message{
		&lkeap.Message{
			Role:    common.StringPtr("user"),
			Content: common.StringPtr("hello"),
			// Content: common.StringPtr("class FriendListFragment : BaseListFragment<ShareMemberInfo>(), ISearchEvent { private lateinit var mViewModel: FriendFollowFansVM companion object { @JvmStatic fun newInstance() = FriendListFragment() } override fun onCreate(savedInstanceState: Bundle?) { super.onCreate(savedInstanceState) mViewModel = ViewModelProvider(this).get(FriendFollowFansVM::class.java) } override fun onFindViews() { super.onFindViews() collect() refreshOrLoadMore() } override fun onResume() { super.onResume() //        onRefresh() } /** * 接口监听 */ private fun collect() { viewLifecycleOwner.lifecycleScope.launch { launch { mViewModel.friendListInfo.collect { dealResponse(it) } } launch { mViewModel.getError().collect { dealResponse(emptyList<ShareMemberInfo>()) } } } } override fun refreshOrLoadMore() { if (searchMarkContent.isNotEmpty()) { //type = \"类型：1好友，2关注，3粉丝\" mViewModel.getFriendFunListInfo(currentPage, 1, searchMarkContent) } else { mViewModel.getFriendListInfo(currentPage) } } override fun initManager() = LinearLayoutManager(context, RecyclerView.VERTICAL, false) override fun initAdapter() = FriendListAdapter().apply { setOnItemClickListener { adapter, _, position -> val item = adapter.data[position] as ShareMemberInfo?  item ?: return@setOnItemClickListener UserInfoActivity.start(mContext, item.uid) } setOnItemChildClickListener { adapter, view, position -> val item = adapter.data[position] as ShareMemberInfo?  item ?: return@setOnItemChildClickListener when (view.id) { R.id.tv_news -> { // 私信 lifecycleScope.launch { ActivityProvider.get().getActivity(PersonalChatActivity::class.java) ?.finish() delay(200) NimUIKit.startP2PSession(mContext, item.uid.toString()) } } R.id.tv_to_find -> { val userInRoom = item.userInRoom ?: return@setOnItemChildClickListener RoomFrameHandle.start( mContext, userInRoom.uid, userInRoom.type, userInRoom.avatar) } R.id.iv_avatar -> { val userInRoom = item.userInRoom ?: kotlin.run { UserInfoActivity.start(mContext, item.uid) return@setOnItemChildClickListener } RoomFrameHandle.start( mContext, userInRoom.uid, userInRoom.type, userInRoom.avatar) } } } } override fun onSearch(content: String?) { if (searchMarkContent.equals(content)) { return } if (content.isNullOrEmpty()) { searchMarkContent = \"\" // Handle null or empty content onRefresh() return } val longValue = content.toLongOrNull() if (longValue != null) { searchMarkContent = content onRefresh() refreshOrLoadMore(false) } else { // Handle non-Long content } } override fun restoreDisplayAll() { } override fun getCurrentSearchContent(): String { return searchMarkContent } private var searchMarkContent = \"\" } interface ISearchEvent { fun onSearch(content: String?) fun restoreDisplayAll() fun getCurrentSearchContent(): String } \n排查是否有运行时异常,只分析运行时异常"),
		},
	}
	request.Temperature = common.Float64Ptr(0.2)
	request.MaxTokens = common.Int64Ptr(500)

	// 返回的resp是一个ChatCompletionsResponse的实例，与请求对象对应
	response, err := client.ChatCompletions(request)
	if _, ok := err.(*errors.TencentCloudSDKError); ok {
		t.Log("An API error has returned: ", err)
		return
	}
	if err != nil {
		panic(err)
	}

	// 输出json格式的字符串回包
	if response.Response != nil {
		// 非流式响应
		t.Log(response.ToJsonString())
	} else {
		// 流式响应
		for event := range response.Events {
			t.Log(string(event.Data))
		}
	}
}
