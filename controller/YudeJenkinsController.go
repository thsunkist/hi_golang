package controller

import (
	"bytes"
	"encoding/json"
	"fmt"
	"hi_golang/tools/lop"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

type JenkinsRequest struct {
	AppName string `json:APP_NAME`
	Intro   string `json:intro`
}

func PostJenkins1(c *gin.Context) {
	// urlParams := c.Request.URL.Query()
	// lop.D("URL parameters: ", urlParams)

	// headers := c.Request.Header
	// lop.D("Headers: ", headers)

	// c.Request.ParseForm()
	// formParams := c.Request.PostForm
	// lop.D("Form parameters: ", formParams)

	bodyBytes, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		lop.E(err)
	}
	bodyContent := string(bodyBytes)
	lop.D("Body content: ", bodyContent)

	// 重置body，因为ReadAll函数会消耗掉body内容
	// c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

	//输出post body内容
	// postBody, _ := ioutil.ReadAll(c.Request.Body)
	// msg := c.Query("msg")
	// proejct := c.Query("proejct")
	// lop.I(string(postBody), msg, proejct)
	// webhook http://tools.shijianline.cn:9001/generic-webhook-trigger/invoke?token=yolo_android
	//http://tools.shijianline.cn:9001/job/Yolo-Android/build?token=yolo_android
	appName := bodyContent //urlParams.Get("APP_NAME")
	// lop.D(appName)

	//http://47.119.175.88:8080/generic-webhook-trigger/invoke?token=lightchat-web-46ef2dacdc21a9b1&APP_NAME= svga%111123131131&intro= svga%111123131131
	appName = strings.TrimSpace(appName)
	params := strings.Split(appName, "%")
	lop.D(params)
	_appName := params[0]
	_intro := params[1]

	jenkinsURL := fmt.Sprintf("http://47.119.175.88:8080/generic-webhook-trigger/invoke?token=lightchat-web-46ef2dacdc21a9b1&APP_NAME=%[1]s&intro=%[2]s", _appName, _intro)

	var jenkinsRequest = JenkinsRequest{AppName: _appName, Intro: _intro}
	jenkinsRequestBytes, _ := json.Marshal(jenkinsRequest)
	lop.D(string(jenkinsRequestBytes))
	lop.D(jenkinsURL)
	resp, err := http.Post(jenkinsURL, "application/json", bytes.NewBuffer(jenkinsRequestBytes))

	if err != nil {
		log.Fatalf("Failed to trigger the Jenkins job: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		log.Fatalf("Failed to trigger the Jenkins job, status: %s", resp.Status)
	}
	log.Println("Jenkins job triggered successfully")
}
