package mars

import (
	"bytes"
	"fmt"
	"hi_golang/tools/lop"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func GetIndex(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "mars_index2.html", nil)
}

func Decode(ctx *gin.Context) {
	//python decode_mars_crypt_log_file.py logs_20220822.xlog
	originalXLogFilePath := "./tmp/xlog/upload/"
	if err := os.MkdirAll(originalXLogFilePath, os.ModePerm); err != nil {
		lop.E(err.Error())
	}
	file, _ := ctx.FormFile("file")
	pyRoot := "./tools/android/company/sjzxLogDecode/crypt/"
	if strings.HasSuffix(file.Filename, ".xlog") {
		newFileName := fmt.Sprintf("%s.%d", file.Filename, time.Now().UnixNano()/int64(time.Microsecond))
		newAbsoluteFilePath := fmt.Sprintf("%s%s", originalXLogFilePath, newFileName)
		ctx.SaveUploadedFile(file, newAbsoluteFilePath)

		cmd := exec.Command(
			"python",
			pyRoot+"decode_mars_crypt_log_file.py",
			newAbsoluteFilePath)
		lop.I(newAbsoluteFilePath)

		var stdout bytes.Buffer
		var stderr bytes.Buffer
		cmd.Stdout = &stdout
		cmd.Stderr = &stderr

		err := cmd.Run()

		if err != nil {
			fmt.Printf("Stdout: %s\n", stdout.String())
			fmt.Printf("Stderr: %s\n", stderr.String())
			fmt.Printf("Stderr: %s\n", err.Error())
		} else {
			fmt.Printf("Stdout: %s\n", stdout.String())
		}
		ctx.Header("File-Name", file.Filename+".log")
		ctx.Writer.Header().Set("Content-Disposition", "attachment; filename="+file.Filename+".log")
		ctx.FileAttachment(newAbsoluteFilePath+".log", file.Filename+".log")
		//c.String(http.StatusOK, fmt.Sprintf("%s\n%s", stdout.String(), stderr.String()))
		// c.FileAttachment(originalXLogFilePath+file.Filename, file.Filename+".log")

	} else if strings.HasSuffix(file.Filename, ".zip") {
		ctx.String(http.StatusOK, "TODO, 只支持.xlog后缀的文件名")
		return
	} else {
		ctx.String(http.StatusOK, "文件格式错误, 只支持.xlog后缀的文件名")
		return
	}

}
