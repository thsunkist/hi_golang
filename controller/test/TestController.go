package test

import (
	"github.com/brianvoe/gofakeit/v6"
	"github.com/gin-gonic/gin"
)

type TestResponse struct {
	Content string
}

func TestGetController(ctx *gin.Context) {
	ctx.Set("data", gofakeit.Person())
}

type TestPostReponse struct {
}

func TestPostController(ctx *gin.Context) {
	ctx.Set("data", gofakeit.Person())
}
