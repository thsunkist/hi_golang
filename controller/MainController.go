package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetIndex(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "main.html", nil)
}
