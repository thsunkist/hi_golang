package jenkins

import (
	"bytes"
	"encoding/json"
	"fmt"
	"hi_golang/tools/lop"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func PostSpeciallyFlavorCompile(ctx *gin.Context) {

	rawJsonBytes, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
		lop.E(err.Error())
	}
	rawJson := string(rawJsonBytes)
	lop.D(string(rawJsonBytes))
	rawJson = strings.TrimSpace(rawJson)

	genericWebhookTriggerToken := ctx.Request.Header.Get("GenericWebhookTriggerToken")
	username := ctx.Request.Header.Get("Author")
	lop.D("genericWebhookTriggerToken: ", genericWebhookTriggerToken)
	/*
		{"flavor":"huawei","branch_name":"release","ref":"refs/heads/消息内容","user_name":"taohui"}
		{"ref":"refs/heads/消息内容"}
		http://tools.shijianline.cn:9001/generic-webhook-trigger/invoke?token=fancy_android&from=dingding
	*/

	paramsMap := make(map[string]string)
	rawJson = strings.ReplaceAll(rawJson, " ", "")
	if params := strings.Split(rawJson, ","); len(params) == 3 {
		/* google,release,develop
		   google,debug,develop
		*/
		product_flavor := params[0]
		build_type := params[1]
		branch := params[2]
		paramsMap["ref"] = fmt.Sprintf(`refs/heads/%[1]s`, branch)
		paramsMap["product_flavor"] = product_flavor
		paramsMap["build_type"] = build_type
	} else if params := strings.Split(rawJson, ","); len(params) == 2 {
		/* google,develop */
		product_flavor := params[0]
		branch := params[1]
		paramsMap["ref"] = fmt.Sprintf(`refs/heads/%[1]s`, branch)
		paramsMap["product_flavor"] = product_flavor
	} else {
		/* develop */
		branch := rawJson
		paramsMap["ref"] = fmt.Sprintf(`refs/heads/%[1]s`, branch)
	}
	paramsMap["user_name"] = username

	paramsJsonBytes, err := json.Marshal(&paramsMap)
	if err != nil {
		lop.E(err.Error())
	} else {
		lop.D(string(paramsJsonBytes))
	}
	httpUrl := fmt.Sprintf("http://taohui:jenkins@tools.shijianline.cn:9001/generic-webhook-trigger/invoke?token=%[1]s&from=dingding", genericWebhookTriggerToken)
	resp, err := http.Post(httpUrl, "application/json", bytes.NewBuffer(paramsJsonBytes))

	if err != nil {
		lop.E(err.Error())
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		lop.E("Error reading response body: " + err.Error())
	} else {
		lop.D("Response: " + string(body))
	}
	defer resp.Body.Close()
}
