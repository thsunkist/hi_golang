package jenkins

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hi_golang/constants"
	"hi_golang/model"
	"hi_golang/tools/lop"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"

	"github.com/CatchZeng/dingtalk/pkg/dingtalk"
	"github.com/gin-gonic/gin"
)

/* record_latest_commit */
func RecordLatestCommit(ctx *gin.Context) {
	rawJsonBytes, err := io.ReadAll(ctx.Request.Body)
	codeFileRootPath := "/data/soft/jenkins_data/workspace/"
	// Create the directory if it does not exist
	if _, err := os.Stat("logs/record_latest_commit"); os.IsNotExist(err) {
		os.Mkdir("logs/record_latest_commit", 0755)
	}
	lop.D(constants.ANDROID_PROJECT_ROOT_PATH, string(rawJsonBytes))
	if err != nil {
		lop.E(err.Error())
	}

	var recordLatestCommitModel model.RecordLatestCommitModel

	if err := json.Unmarshal(rawJsonBytes, &recordLatestCommitModel); err != nil {
		lop.E(err.Error())
		return
	}
	lop.D("1111111111111111111111111111111")

	if strings.HasSuffix(recordLatestCommitModel.Repository.Name, "Flutter") ||
		strings.HasSuffix(recordLatestCommitModel.Repository.Name, "Android") ||
		strings.HasSuffix(recordLatestCommitModel.Repository.Name, "Android-Test") {
		if err = os.WriteFile("logs/record_latest_commit/"+recordLatestCommitModel.Repository.Name+".json", rawJsonBytes, 0644); err != nil {
			lop.E(err)
		}
	}
	lop.D("1111111111111111111111111111111")

	// git diff --name-only HEAD~1
	gitRepoAbsolutelyPath := constants.ANDROID_PROJECT_ROOT_PATH + "/" + recordLatestCommitModel.Repository.Name

	// 调用 Git 命令获取文件变动
	gitDiffOutput, err := getGitDiffFiles(gitRepoAbsolutelyPath)
	if err != nil {
		lop.E(fmt.Sprintf("Failed to get git diff files: %s", err.Error()))
		return
	}
	lop.D("1111111111111111111111111111111")

	gitLogFolderPath := "logs/git_diff_files/" + recordLatestCommitModel.Repository.Name
	gitLogFilePath := gitLogFolderPath + "/" + recordLatestCommitModel.CheckoutSHA + ".log"
	if err := os.MkdirAll(gitLogFolderPath, 0755); err != nil {
		lop.E(fmt.Sprintf("Failed to create directory for git diff logs: %s", err.Error()))
		return
	}
	// 将变动文件写入日志文件
	// 将输出按行分割
	files := strings.Split(gitDiffOutput, "\n")
	lop.D("1111111111111111111111111111111")

	// 定义一个切片存储过滤后的文件
	var filteredFiles []string
	var fileTypeName = "这是一个"
	// 遍历文件列表，过滤出 .kt 和 .java 文件
	for _, file := range files {
		if strings.HasSuffix(file, ".kt") || strings.HasSuffix(file, ".java") {
			filteredFiles = append(filteredFiles, file)
		}

		if strings.HasSuffix(file, ".kt") {
			fileTypeName += "Kotlin文件,"
		} else if strings.HasSuffix(file, ".java") {
			fileTypeName += "java文件,"
		} else {
			continue
		}
	}
	/* TODO TIPS: delete it!!! */
	filteredFiles = filteredFiles[:1]
	gitDiffOutput = fmt.Sprintf("%1s %2s", "####", recordLatestCommitModel.Repository.Name+" FileChanges")
	lop.I(gitDiffOutput, len(filteredFiles))
	// 输出过滤后的文件
	lop.D("1111111111111111111111111111111")

	if len(filteredFiles) == 0 {
		return
	}
	for _, file := range filteredFiles {
		sourceCodeFileURL := fmt.Sprintf("%1s/%2s/ws/%3s/", "http://tools.shijianline.cn:9001/job", recordLatestCommitModel.Repository.Name, file)
		file = strings.ReplaceAll(file, "/src/main/java/", " ")
		file = strings.ReplaceAll(file, "/src/main/kotlin/", " ")
		gitDiffOutput += ("\r\n[" + file + "](" + sourceCodeFileURL + ") ✅")
		lop.D(("\r\n[" + file + "](" + sourceCodeFileURL + ") ✅"))
	}
	var codeFileAbsolutelyPath = ""
	for _, file := range filteredFiles {
		codeFileAbsolutelyPath = codeFileRootPath + recordLatestCommitModel.Repository.Name + string(os.PathSeparator) + file
		lop.I(codeFileAbsolutelyPath)
	}
	lop.D("1111111111111111111111111111111")

	if err := os.WriteFile(gitLogFilePath, []byte(gitDiffOutput), 0644); err != nil {
		lop.E(fmt.Sprintf("Failed to write git diff log file: %s", err.Error()))
		return
	}
	// codepalPostUrl := "https://api.codepal.ai/v1/bug-detector/query"
	// codepalPostUrl := "https://api.codepal.ai/v1/code-reviewer/query"
	// curl -s http://175.178.52.207:6399/api/chat  -d '{"model": "deepseek-r1:32b","messages": [{ "role": "user", "content": "你好?" }],"stream": false}' | jq
	// bugDetectorResultRawJson := codepalCodeHelperPost(codepalPostUrl, "55906df0-af12-46d7-9500-5e3ef27528b6", codeFileAbsolutelyPath)
	// var codePalBugDetectorQueryResult CodePalBugDetectorQueryResult

	// if err := json.Unmarshal([]byte(bugDetectorResultRawJson), &codePalBugDetectorQueryResult); err != nil {
	// log.Fatalf("Error parsing JSON: %v", err)
	// }
	/* ----------------------deepseek---------------------- */

	deepseekR1_32bResult := deepseekR1_32b(codeFileAbsolutelyPath, fileTypeName+"排查是否有运行时异常,只分析运行时异常,不需要论述你的分析流程,简短精要回复,不要重复我的要求")
	/* ------------------------end------------------------- */

	var dingtalkContent = gitDiffOutput + "\r\n" + deepseekR1_32bResult.Message.Content
	lop.D("Git diff files successfully recorded in:", gitLogFolderPath)
	// https://oapi.dingtalk.com/robot/send?access_token=b9ee7acc687c7c0831de52e85d9f59a97ff7876b1892b4b26cea865c52c5717c
	accessToken := "b9ee7acc687c7c0831de52e85d9f59a97ff7876b1892b4b26cea865c52c5717c"
	secret := "FileChange"
	client := dingtalk.NewClient(accessToken, secret)
	// msg := dingtalk.NewMarkdownMessage().SetContent().SetAt([]string{""}, false)
	var msg1 = dingtalk.NewMarkdownMessage()
	msg1.SetMarkdown("Result", dingtalkContent)
	msg1.SetAt([]string{""}, false)
	// msg.MsgType = dingtalk.MsgTypeMarkdown
	result, resp, err := client.Send(msg1)
	if err != nil {
		fmt.Printf("Error sending request: %v\n", err)
		return
	}
	lop.E(result, resp.ErrCode, resp.ErrMsg)
}

func GetNdkResult(rawJson string, ding_talk_robot_token string) {
	url := fmt.Sprintf(ding_talk_robot_token)
	data := rawJson

	resp, err := http.Post(url, "text/plain", bytes.NewBufferString(data))
	if err != nil {
		lop.E(err)
	}
	lop.D(rawJson)
	defer resp.Body.Close()

	lop.I("Response status:", resp.Status)
}
func (this CodePalBugDetectorQueryResult) extractRuntimeErrors() string {
	// 使用正则表达式提取 Runtime Errors 部分
	re := regexp.MustCompile(`(?s)#### Runtime Errors(.*?)####`) // 匹配 "Runtime Errors" 部分直到下一个标题

	matches := re.FindStringSubmatch(this.Result)
	if len(matches) > 1 {
		return "Runtime Errors\r\n" + strings.TrimSpace(matches[1]) // 返回匹配到的内容并去除首尾空格
	}
	return "No Runtime Errors found"
}

// 获取 Git 变动的文件列表
func getGitDiffFiles(repoPath string) (string, error) {
	// 确保仓库路径存在
	if _, err := os.Stat(repoPath); os.IsNotExist(err) {
		return "", fmt.Errorf("repository path does not exist: %s", repoPath)
	}

	// 执行 git diff --name-only HEAD~1 命令
	cmd := exec.Command("git", "diff", "--name-only", "HEAD~1")
	cmd.Dir = repoPath // 指定工作目录为仓库路径
	output, err := cmd.Output()
	if err != nil {
		return "", fmt.Errorf("error executing git diff: %w", err)
	}

	// 返回变动文件列表（以字符串形式）
	return string(output), nil
}

type Job struct {
	Class string `json:"_class"`
	Name  string `json:"name"`
}

type Hudson struct {
	Class string `json:"_class"`
	Jobs  []Job  `json:"jobs"`
}

func getJenkinsAllJobs() string {
	url := "http://tools.shijianline.cn:9001/api/json?tree=jobs[name]"
	username := "taohui"
	password := "jenkins"

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		lop.E(err)
		return ""
	}

	req.Header.Add("Authorization", "Basic "+base64.StdEncoding.EncodeToString([]byte(username+":"+password)))

	resp, err := client.Do(req)
	if err != nil {
		lop.E(err)
		return ""
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		lop.E(err)
		return ""
	}

	return string(body)
}

/*
记录最新的commit值, 用来每天晚上做dev和release的全量构建, 降低增量编译导致的问题

	{
	    "total_commits_count": 1,
	    "user_email": "thsunkist@gmail.com",
	    "before": "71996b60462a059b4aad90ff6e55cc52e423d2a5",
	    "user_extern_uid": "5f61841f18c17e77486f7404",
	    "user_name": "陶辉",
	    "checkout_sha": "f9dd6364e1466bb3e795a109a3602a0d6c2457bd",
	    "repository": {
	        "name": "Yolo-Android-Test",
	        "description": "海外-中东版本-安卓",
	        "visibility_level": 10,
	        "git_http_url": "https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Yolo-Android-Test.git",
	        "git_ssh_url": "git@codeup.aliyun.com:6289ac98487c500c27f5b92e/tl-onotoone/Yolo-Android-Test.git",
	        "url": "git@codeup.aliyun.com:6289ac98487c500c27f5b92e/tl-onotoone/Yolo-Android-Test.git",
	        "homepage": "https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Yolo-Android-Test"
	    },
	    "object_kind": "push",
	    "ref": "refs/heads/master",
	    "project_id": 2379415,
	    "user_id": 75105,
	    "commits": [
	        {
	            "author": {
	                "name": "taohui",
	                "email": "mac@MacBook-Pro-5.local"
	            },
	            "id": "f9dd6364e1466bb3e795a109a3602a0d6c2457bd",
	            "message": "[upd]更新回1970发布的版本项目配置\n",
	            "url": "https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Yolo-Android-Test/commit/f9dd6364e1466bb3e795a109a3602a0d6c2457bd",
	            "timestamp": "2024-04-02T09:45:34.000Z"
	        }
		],
	    "after": "f9dd6364e1466bb3e795a109a3602a0d6c2457bd",
	    "aliyun_pk": "1956350239634089"
	}
*/
func codepalCodeHelperPost(url, apiKey, filePath string) string {

	// API URL

	// 打开文件
	fileContent, err := os.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("failed to read file: %w", err).Error()
	}
	code := string(fileContent)

	// 创建一个 buffer 和 multipart writer
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// 添加 "code" 字段
	err = writer.WriteField("code", code)
	if err != nil {
		return fmt.Errorf("failed to write code field: %w", err).Error()
	}

	// 关闭 writer
	err = writer.Close()
	if err != nil {
		return fmt.Errorf("failed to close writer: %w", err).Error()
	}

	// 创建 HTTP 请求
	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		return fmt.Errorf("failed to create request: %w", err).Error()
	}

	// 设置请求头
	req.Header.Set("Authorization", "Bearer "+apiKey)
	req.Header.Set("Content-Type", writer.FormDataContentType())

	// 发送请求
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("failed to send request: %w", err).Error()
	}
	defer resp.Body.Close()

	// 读取响应
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("failed to read response: %w", err).Error()
	}

	// 打印响应
	fmt.Println("Response Status:", resp.Status)
	fmt.Println("Response Body:", string(respBody))

	return string(respBody)
}

// func main() {
// 	// 示例调用
// 	apiKey := "YOUR_API_KEY" // 替换为你的实际 API Key
// 	filePath := "/absolute/path/to/your/code.py" // 替换为代码文件的绝对路径

// 	err := sendCodeFile(apiKey, filePath)
// 	if err != nil {
// 		fmt.Println("Error:", err)
// 	}
// }

/*
	curl -X POST -H 'Content-Type: application/json' -d '{"total_commits_count":8,"user_email":"accounts_5eff258bb23b8d003696b003@mail.teambition.com","before":"1f866325c1290dbbb442ff6aa5d1444f429f08ca","user_extern_uid":"5eff258b94513bd17039ba02","user_name":"童寿洳","checkout_sha":"0a24261e1f16c6077ea380d97e5a70c7afd2d90b","repository":{"name":"Lami-Android","visibility_level":0,"git_http_url":"https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android.git","git_ssh_url":"git@codeup.aliyun.com:6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android.git","url":"git@codeup.aliyun.com:6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android.git","homepage":"https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android"},"object_kind":"push","ref":"refs/heads/dev","project_id":4412237,"user_id":32944,"commits":[{"author":{"name":"Tongsr","email":"1009956987@qq.com"},"id":"0f509150378e56a9aa2a45bb009a36270add6516","message":"扭蛋游戏的奖励列表 viewModel 的作用域修改为 requireParentFragment。\n","url":"https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android/commit/0f509150378e56a9aa2a45bb009a36270add6516","timestamp":"2024-06-18T04:13:47.000Z"},{"author":{"name":"Tongsr","email":"1009956987@qq.com"},"id":"ed150587afab9bcddb409d17b41e6cf7d9f9b0c5","message":"扭蛋游戏的排行榜显示财富等级，特效的size调整，房间主题列表界面完成80%\n","url":"https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android/commit/ed150587afab9bcddb409d17b41e6cf7d9f9b0c5","timestamp":"2024-06-18T10:52:40.000Z"},{"author":{"name":"Tongsr","email":"1009956987@qq.com"},"id":"49119f85cacfef91fb0b5a2f97fc633b803df401","message":"房间主题列表的 ui 全部完成。\n","url":"https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android/commit/49119f85cacfef91fb0b5a2f97fc633b803df401","timestamp":"2024-06-19T03:02:33.000Z"},{"author":{"name":"Tongsr","email":"1009956987@qq.com"},"id":"a5e7708fc09c54f2394de16fb1b5994e9866e4b4","message":"房间主题列表的接口数据，删除接口，选择和反选的逻辑\n","url":"https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android/commit/a5e7708fc09c54f2394de16fb1b5994e9866e4b4","timestamp":"2024-06-19T12:26:17.000Z"},{"author":{"name":"Tongsr","email":"1009956987@qq.com"},"id":"d45af0feb93f0d2426360f18295ff6981036dd47","message":"扭蛋游戏排行榜单的 mavericksViewModel 实例增加 key 和 args。\n","url":"https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android/commit/d45af0feb93f0d2426360f18295ff6981036dd47","timestamp":"2024-06-20T02:22:20.000Z"},{"author":{"name":"Tongsr","email":"1009956987@qq.com"},"id":"9437ab41616462e96c145c3664cd06666614b6d7","message":"房间主题列表、删除主题、续费主题弹窗等业务完成，包括数据对接。\n","url":"https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android/commit/9437ab41616462e96c145c3664cd06666614b6d7","timestamp":"2024-06-20T09:03:03.000Z"},{"author":{"name":"Tongsr","email":"1009956987@qq.com"},"id":"22d74f1765991679280cb0b28d92f3cfce495e39","message":"房间主题自定义 activity，以及主题列表的单选。\n","url":"https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android/commit/22d74f1765991679280cb0b28d92f3cfce495e39","timestamp":"2024-06-20T12:18:40.000Z"},{"author":{"name":"Tongsr","email":"1009956987@qq.com"},"id":"0a24261e1f16c6077ea380d97e5a70c7afd2d90b","message":"房间主题列表续费后刷新整个列表。\n上传自定义主题的部分 ui。\n","url":"https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Lami-Android/commit/0a24261e1f16c6077ea380d97e5a70c7afd2d90b","timestamp":"2024-06-21T06:30:49.000Z"}],"after":"0a24261e1f16c6077ea380d97e5a70c7afd2d90b","aliyun_pk":"1855500342455388"}' 'http://127.0.0.1:8080/jenkins/record_latest_commit'
*/

// fmt.Println(filteredJobs)
/* 记录最新的commit值, 用来每天晚上做dev和release的全量构建, 降低增量编译导致的问题
{
    "total_commits_count": 1,
    "user_email": "thsunkist@gmail.com",
    "before": "71996b60462a059b4aad90ff6e55cc52e423d2a5",
    "user_extern_uid": "5f61841f18c17e77486f7404",
    "user_name": "陶辉",
    "checkout_sha": "f9dd6364e1466bb3e795a109a3602a0d6c2457bd",
    "repository": {
        "name": "Yolo-Android-Test",
        "description": "海外-中东版本-安卓",
        "visibility_level": 10,
        "git_http_url": "https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Yolo-Android-Test.git",
        "git_ssh_url": "git@codeup.aliyun.com:6289ac98487c500c27f5b92e/tl-onotoone/Yolo-Android-Test.git",
        "url": "git@codeup.aliyun.com:6289ac98487c500c27f5b92e/tl-onotoone/Yolo-Android-Test.git",
        "homepage": "https://codeup.aliyun.com/6289ac98487c500c27f5b92e/tl-onotoone/Yolo-Android-Test"
    },
    "object_kind": "push",
    "ref": "refs/heads/master",
    "project_id": 2379415,
    "user_id": 75105,
    "after": "f9dd6364e1466bb3e795a109a3602a0d6c2457bd",
    "aliyun_pk": "1956350239634089"
}
*/

func deepseekR1_32b(filePath, promptContent string) *DeepSeekR1_32bResult {

	// 定义请求的 URL
	url := "http://175.178.52.207:6399/api/chat"
	fileContent, err := os.ReadFile(filePath)
	if err != nil {
		lop.E(fmt.Sprintf("failed to read file: %s", err.Error()))
	}
	code := string(fileContent) + "\r\n" + promptContent

	// 定义请求的结构体
	type Message struct {
		Role    string `json:"role"`
		Content string `json:"content"`
	}

	type Request struct {
		Model    string    `json:"model"`
		Messages []Message `json:"messages"`
		Stream   bool      `json:"stream"`
	}

	request := Request{
		Model: "deepseek-r1:32b",
		Messages: []Message{
			{
				Role:    "user",
				Content: code,
			},
		},
		Stream: false,
	}

	// 序列化为 JSON
	requestRawJson, err := json.MarshalIndent(request, "", "  ")
	if err != nil {
		lop.E("Error serializing request:", err)
		return nil
	}
	// 定义请求的 JSON 数据

	// 创建 HTTP 请求
	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(requestRawJson)))
	if err != nil {
		lop.E("Error creating request:", err)
		return nil
	}

	// 设置请求头
	req.Header.Set("Content-Type", "application/json")

	// 发送 HTTP 请求
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		lop.E("Error sending request:", err)
		return nil
	}
	defer resp.Body.Close()

	// 读取响应
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		lop.E("Error reading response:", err)
		return nil
	}

	jsonData := string(body)
	// 输出响应
	lop.I("Response Status:", resp.Status)
	lop.I("Response Body:", jsonData)

	// 解析 JSON 数据
	var response DeepSeekR1_32bResult

	if err := json.Unmarshal([]byte(jsonData), &response); err != nil {
		lop.E("Error parsing JSON:", err)
		return nil
	}
	return &response
}

type CodePalBugDetectorQueryResult struct {
	Created int64  `json:"created"`
	Error   string `json:"error"`
	ID      string `json:"id"`
	ModelID string `json:"model_id"`
	Result  string `json:"result"`
}

type DeepSeekR1_32bResult struct {
	Model              string                      `json:"model"`
	CreatedAt          string                      `json:"created_at"`
	Message            DeepSeekR1_32bMessageResult `json:"message"`
	DoneReason         string                      `json:"done_reason"`
	Done               bool                        `json:"done"`
	TotalDuration      int64                       `json:"total_duration"`
	LoadDuration       int64                       `json:"load_duration"`
	PromptEvalCount    int                         `json:"prompt_eval_count"`
	PromptEvalDuration int64                       `json:"prompt_eval_duration"`
	EvalCount          int                         `json:"eval_count"`
	EvalDuration       int64                       `json:"eval_duration"`
}

type DeepSeekR1_32bMessageResult struct {
	Role    string `json:"role"`
	Content string `json:"content"`
}
