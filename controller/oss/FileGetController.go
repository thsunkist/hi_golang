package oss

import (
	"fmt"
	"hi_golang/tools/lop"
	"io"
	"net/url"
	"os"
	"os/exec"
	"strings"

	"github.com/gin-gonic/gin"
)

func GetOssFilesGet(ctx *gin.Context) {
	envVars := os.Environ()

	for _, envVar := range envVars {
		lop.D(envVar)
	}
	
	ossline := ctx.Query("ossline")
	bucketname := ctx.Query("bucketname")
	var err error

	if ossline, err = url.QueryUnescape(ossline); err != nil {
		lop.E(err.Error())
	}
	if bucketname, err = url.QueryUnescape(bucketname); err != nil {
		lop.E(err.Error())
	}

	hashkey := strings.Split(ossline, `,`)[0]
	lop.D("-----------", bucketname)
	lop.D("-----------", hashkey)
	// 执行qshell命令下载文件
	filepath := fmt.Sprintf("logs/%s", hashkey)
	cmd := exec.Command("/usr/local/bin/qshell", "get", bucketname, hashkey, "-o", filepath)
	err = cmd.Run()
	if err != nil {
		lop.E(err.Error())
	}

	// Set the header
	ctx.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%s", hashkey))
	ctx.Header("Content-Type", "application/octet-stream")
	f, err := os.Open(filepath)
	if err != nil {
		lop.E(err.Error())
	}
	// Write the body to the response
	io.Copy(ctx.Writer, f)
}
