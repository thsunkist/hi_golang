package oss

import (
	"bufio"
	"bytes"
	"fmt"
	"hi_golang/controller/bugly"
	"hi_golang/tools/lop"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
)

type OssAccessor struct {
	id         byte
	bucketName string
	project    string
	space      string
	region     string
	strategy   string
	date       string
	time       string
}

func GetIndex(ctx *gin.Context) {
	// oss_file_momery_calculate
	ctx.HTML(http.StatusOK, "oss_file_momery_calculate.html", nil)
}

/*
"origin-yolo","yolo-project","自有空间","亚太-新加坡","公开","2024-03-04","14:37:19",
"origin-cocco","cocco-project","自有空间","亚太-新加坡","公开","2024-03-01","14:45:39",
"origin-wego","wego-project","自有空间","亚太-新加坡","公开","2023-11-17","15:30:38",
"origin-game","互乐小游戏研发组","自有空间","亚太-新加坡","公开","2023-10-30","11:47:54",
"origin-olamet","olamet-project后h5","web端","自有空间","亚太-新加坡","公开","2023-10-24","11:50:53",
"origin-popomet","bobbo","自有空间","亚太-新加坡","公开","2023-10-19","11:52:11",
"origin-gimme","gimme-project","自有空间","亚太-新加坡","公开","2023-07-18","16:06:07",
"origin-ditto","ditto-project","自有空间","亚太-新加坡","公开","2023-05-17","15:07:08",
"origin-games","小飞机捕鱼","自有空间","亚太-新加坡","公开","2023-04-04","14:17:57",
"origin-rocket","rocket-project","自有空间","亚太-新加坡","公开","2023-03-22","14:54:38",
"yobi","yobi-project","自有空间","亚太-新加坡","公开","2023-02-24","18:45:15",
"kiyo","kiyo-project","自有空间","亚太-新加坡","公开","2023-02-13","17:53:05",
"habby","habby-project","自有空间","亚太-新加坡","公开","2022-12-02","10:14:15",
"olamet-online","olamet-project","自有空间","亚太-新加坡","公开","2022-10-13","15:47:29",
"olamet","olamet-project","自有空间","亚太-新加坡","公开","2022-09-21","14:49:18",
"haki","haki","自有空间","亚太-新加坡","公开","2022-02-28","11:26:06",
"appfancy","fancy-project","自有空间","亚太-新加坡","公开","2021-12-16","14:28:12",
"applami","lami-project","自有空间","亚太-新加坡","公开","2021-11-01","10:56:32",
"oohla","oohla-project","自有空间","亚太-新加坡","公开","2021-02-24","18:25:52",
"appwooya","ligo-project","自有空间","亚太-新加坡","公开","2020-06-28","17:09:12",
"oeapp","lc-project","自有空间","亚太-新加坡","公开","2020-05-27","17:21:15",
*/
func CommitOssFilesCheckTask(ctx *gin.Context) {
	go func() {
		envVars := os.Environ()

	for _, envVar := range envVars {
		lop.D(envVar)
	}

		var ossAccessors []OssAccessor
		ossAccessors = append(ossAccessors, OssAccessor{
			1, "origin-yolo", "Yolo-Project", "自有空间", "亚太-新加坡", "公开", "2024-03-04", "14:37:19",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			2, "origin-cocco", "Cocco-Project", "自有空间", "亚太-新加坡", "公开", "2024-03-01", "14:45:39",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			3, "origin-wego", "Wego-Project", "自有空间", "亚太-新加坡", "公开", "2023-11-17", "15:30:38",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			4, "origin-game", "Game-Group", "自有空间", "亚太-新加坡", "公开", "2023-10-30", "11:47:54",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			5, "origin-olamet", "Olamet-Project-H5", "web端", "自有空间", "亚太-新加坡", "公开", "2023-10-24",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			6, "origin-popomet", "Bobbo-Project", "自有空间", "亚太-新加坡", "公开", "2023-10-19", "11:52:11",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			7, "origin-gimme", "Gimme-Project", "自有空间", "亚太-新加坡", "公开", "2023-07-18", "16:06:07",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			8, "origin-ditto", "Ditto-Project", "自有空间", "亚太-新加坡", "公开", "2023-05-17", "15:07:08",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			9, "origin-games", "Games-Project", "自有空间", "亚太-新加坡", "公开", "2023-04-04", "14:17:57",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			10, "origin-rocket", "Rocket-Project", "自有空间", "亚太-新加坡", "公开", "2023-03-22", "14:54:38",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			11, "yobi", "Yobi-Project", "自有空间", "亚太-新加坡", "公开", "2023-02-24", "18:45:15",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			12, "kiyo", "Kiyo-Project", "自有空间", "亚太-新加坡", "公开", "2023-02-13", "17:53:05",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			13, "habby", "Habby-project", "自有空间", "亚太-新加坡", "公开", "2022-12-02", "10:14:15",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			14, "olamet-online", "Olamet-Online-Project", "自有空间", "亚太-新加坡", "公开", "2022-10-13", "15:47:29",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			15, "olamet", "Olamet-Project", "自有空间", "亚太-新加坡", "公开", "2022-09-21", "14:49:18",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			16, "haki", "Haki-Project", "自有空间", "亚太-新加坡", "公开", "2022-02-28", "11:26:06",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			17, "appfancy", "Fancy-Project", "自有空间", "亚太-新加坡", "公开", "2021-12-16", "14:28:12",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			17, "applami", "Lami-Project", "自有空间", "亚太-新加坡", "公开", "2021-11-01", "10:56:32",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			18, "oohla", "Oohla-Project", "自有空间", "亚太-新加坡", "公开", "2021-02-24", "18:25:52",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			19, "appwooya", "Ligo-Project", "自有空间", "亚太-新加坡", "公开", "2020-06-28", "17:09:12",
		})
		ossAccessors = append(ossAccessors, OssAccessor{
			20, "oeapp", "LightChat-Project", "自有空间", "亚太-新加坡", "公开", "2020-05-27", "17:21:15",
		})

		startDate := time.Now().AddDate(0, 0, -1).Format("2006-01-02")
		reportFolderPath := fmt.Sprintf("logs%[1]soss_file_check_result%[2]s%[3]s",
			string(filepath.Separator),
			string(filepath.Separator),
			startDate,
		)

		err := os.MkdirAll(reportFolderPath, 0755)
		if err != nil {
			lop.E("Failed to create directory", err.Error())
		}

		type Result struct {
			lineCount int
			project   string
			fileName  string
		}

		// ossAccessors = ossAccessors[:4]
		var resultsChan = make(chan Result, len(ossAccessors))
		var wg sync.WaitGroup
		for _, ossAccessor := range ossAccessors {
			ossAccessor := ossAccessor
			wg.Add(1)
			go func(ossAccessor OssAccessor, wg *sync.WaitGroup, resultsChan chan<- Result) {
				defer wg.Done() // 在goroutine结束时，表示这个goroutine已经完成
				fmt.Sprintln(ossAccessor.project)

				// 创建一个新的文件，文件名格式为project_date.log
				fileName := fmt.Sprintf("%[1]s%[2]s%[3]s.log", reportFolderPath, string(filepath.Separator), ossAccessor.project)

				originalFile, err := os.Create(fileName)
				if err != nil {
					lop.E("Failed to create log file", fileName, err.Error())
				}
				defer originalFile.Close()
				// qshell listbucket2 --min-file-size 8000000 --readable --mimetypes application/octet-stream,video/mp4,audio/mp3,image/jpeg,image/gif --output-fields-sep ',' yobi
				//application/octet-stream,video/mp4,audio/mp3,image/jpeg,image/gif
				cmd := exec.Command("/usr/local/bin/qshell", "listbucket2", "--start", startDate, "--min-file-size", "2097152", "--readable", "--mimetypes", "application/octet-stream,video/mp4,audio/mp3,image/gif", "--output-fields-sep", ",", ossAccessor.bucketName)

				cmd.Stdout = originalFile
				cmd.Stderr = originalFile
				lop.D(cmd.String())

				if err := cmd.Run(); err != nil {
					lop.E(ossAccessor.bucketName, "cmd.Run() failed with", err.Error())
				}

				// 创建一个新的HTML文件
				newFileName := fmt.Sprintf("%s/%s_filtered.html", reportFolderPath, ossAccessor.project)
				newFile, err := os.Create(newFileName)
				if err != nil {
					lop.E("Failed to create filtered HTML file", newFileName, err.Error())
				}
				defer newFile.Close()

				// 写入HTML开头部分
				_, err = newFile.WriteString("<html><body>\n")
				if err != nil {
					lop.E("Failed to write to filtered HTML file", newFileName, err.Error())
				}

				// 重新打开原始日志文件以便读取
				originalFile, err = os.Open(fileName)
				if err != nil {
					lop.E("Failed to open log file", fileName, err.Error())
				}

				defer os.Remove(originalFile.Name())
				defer originalFile.Close()

				// 创建一个新的扫描器
				fatalContentLineScanner := bufio.NewScanner(originalFile)

				lineCount := 0
				// 读取文件的每一行
				// 读取文件的每一行
				for fatalContentLineScanner.Scan() {
					line := fatalContentLineScanner.Text()
					if !strings.HasPrefix(line, "[I]") {
						// 将每一行包装在<p>标签中，并添加一个<a>标签使其可点击
						ossline := url.QueryEscape(line)
						bucketName := url.QueryEscape(ossAccessor.bucketName)
						_, err := newFile.WriteString(fmt.Sprintf("<p><a href='https://tools.shijianline.cn:9004/oss/file_get?ossline=%s&bucketname=%s'>%s</a></p>\n", ossline, bucketName, line))
						if err != nil {
							lop.E("Failed to write to filtered HTML file", newFileName, err.Error())
						}
						lineCount += 1
					}
				}
				// 写入HTML结尾部分
				_, err = newFile.WriteString("</body></html>\n")
				if err != nil {
					lop.E("Failed to write to filtered HTML file", newFileName, err.Error())
				}

				log.Println(ossAccessor.project, "oss check succeed, item count:", lineCount)
				var result = Result{lineCount: lineCount, project: ossAccessor.project}
				result.fileName = newFileName
				resultsChan <- result
			}(ossAccessor, &wg, resultsChan)
		}

		wg.Wait()
		close(resultsChan)

		markdownText := "OSS-Android-Files Check Result\n\n"
		for result := range resultsChan {
			lop.D(result, " all task completed.")
			markdownText += fmt.Sprintf("[%[1]s File-Count: %[2]d](%[3]s)\n\n",
				result.project,
				result.lineCount,
				fmt.Sprintf("https://tools.shijianline.cn:9004/%s", result.fileName))
		}
		// http://127.0.0.1:8080/oss/commit_oss_files_check_task
		// https://oapi.dingtalk.com/robot/send?access_token=b9ee7acc687c7c0831de52e85d9f59a97ff7876b1892b4b26cea865c52c5717c
		/* 钉钉通知一下咯 */
		var lightChatCurlEntity = bugly.CurlEntity{
			AppName:        "OSS-Android-Files Check Result",
			RobotAccessUrl: `https://oapi.dingtalk.com/robot/send?access_token=b9ee7acc687c7c0831de52e85d9f59a97ff7876b1892b4b26cea865c52c5717c`,
		}

		var dingdingCmd = exec.Command(`curl`, lightChatCurlEntity.RobotAccessUrl,
			`-H`, `Content-Type: application/json`,
			`-d`, fmt.Sprintf(`{
							"msgtype": "markdown",
							"markdown": {
								"title": "Android-OSS Check Result",
								"text": "%[1]s"
							}
						}`, markdownText))

		lop.D(dingdingCmd.String())
		var stdout bytes.Buffer
		var stderr bytes.Buffer

		dingdingCmd.Stdout = &stdout
		dingdingCmd.Stderr = &stderr

		if err := dingdingCmd.Run(); err != nil {
			lop.E(err.Error())
		}
	}()
}
