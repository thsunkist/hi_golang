package chatbubble

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// 1.
// 上传模版文件
// 批量上传要处理的文件
// 批量锚点
func BatchGenerate9PatchFileByTemplate(ctx *gin.Context) {

	ctx.HTML(http.StatusOK, "process_9patch_index.html", nil)
}

// 2.
// 批量去除9patch-borders
// 显示所有已处理的测试用的点9下载地址
func Clear9PathBorders(ctx *gin.Context) {
	// 1.
	// 上传模版文件
	// 批量上传要处理的文件
	// 批量锚点
	// 2.
	// 批量去除9patch-borders
	// 显示所有已处理的测试用的点9下载地址
	ctx.HTML(http.StatusOK, "process_9patch_index.html", nil)
}
