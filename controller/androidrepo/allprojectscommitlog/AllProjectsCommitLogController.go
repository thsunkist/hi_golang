package allprojectscommitlog

import (
	"hi_golang/tools/lop"
	"io"

	"github.com/gin-gonic/gin"
)

func AllProjectsCommitLogController(ctx *gin.Context) {
	rawJsonBytes, err := io.ReadAll(ctx.Request.Body)
	if err != nil {
	}
	rawJson := string(rawJsonBytes)
	lop.I(rawJson)
}
