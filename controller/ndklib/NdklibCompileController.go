package ndklib

import (
	"bytes"
	"encoding/json"
	"fmt"
	"hi_golang/tools/lop"
	"io"
	"math/rand"
	"net/http"
	"os/exec"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

func GetIndex(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "ndklib_configure.html", nil)
}

func GetIndexNdklibGenerateHtml(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "ndklib_generate.html", nil)
}

func PostNdklibGenerate(ctx *gin.Context) {
	/* NOTHING */
}

/* 网页配置ndk编译入口 */
func CompileFromWeb(ctx *gin.Context) {

	rawJsonBytes, err := io.ReadAll(ctx.Request.Body)
	// job := ctx.PostForm("job")
	// job := ctx.Request.Header.Get("job")

	if err != nil {
		lop.E(err.Error())
	}

	rawJson := string(rawJsonBytes)
	lop.I(rawJson)
	var requestParams map[string]string
	json.Unmarshal(rawJsonBytes, &requestParams)
	var job = requestParams["job"]
	var appversion = requestParams["version"]
	var deskey = requestParams["key"]
	lop.I("job: ", job, " version: ", appversion, " key: ", deskey)
	dingTalkRobotToken := "102a9d3a094f213c6a90000l"

	if len(appversion) >= 3 && len(deskey) == 32 {
		deskey := generateRandom2Chars() + deskey

		viper.SetConfigName(job)
		viper.AddConfigPath("./config")

		err := viper.ReadInConfig()
		if err != nil {
			lop.E("Error reading config file, ", err)
			GetNdkResult(fmt.Sprintf("%s-NDK-LIB Build Failed: Error reading config file", job), dingTalkRobotToken)
			ctx.String(200, "NDK-LIB Config Failed: Error reading config file")
		}
		// 获取所有的键
		keys := viper.AllKeys()

		lop.D("\njob:", job, "\nappversion:", appversion, "\ndeskey:", deskey)

		ndkCompileParamsMap := make(map[string]string, 0)
		for _, key := range keys {
			ndkCompileParamsMap[key] = viper.GetString(key)
			lop.D(fmt.Sprintf("%s: %v\n", key, viper.Get(key)))
		}
		dingTalkRobotToken = ndkCompileParamsMap["ding_talk_robot_token"]
		ndklibCompileCmdOut, ndklibCompileCmdErr := executeGradlewCompile(job, deskey, appversion, ndkCompileParamsMap)

		if ndklibCompileCmdErr != nil {
			lop.E("ndklibCompileCmdOut Failed Out:\n", ndklibCompileCmdErr.Error(), ndklibCompileCmdOut)
			GetNdkResult(fmt.Sprintf("%s-NDK-LIB Build Failed:%s\nVersion: %s\n", job, ndklibCompileCmdErr.Error(), appversion), dingTalkRobotToken)
			ctx.String(200, fmt.Sprintf("%s-NDK-LIB Build Failed:%s\nVersion: %s\n", job, ndklibCompileCmdErr.Error(), appversion))
		} else {
			lop.I("ndklibCompileCmdOut Combined Out:\n", ndklibCompileCmdOut)
			GetNdkResult(fmt.Sprintf("%s-NDK-LIB Build Successful\nVersion: %s\nUpdate Time: %s", job, appversion, time.Now().Format("2006-01-02 15:04:05")), dingTalkRobotToken)
			ctx.String(200, fmt.Sprintf("%s-NDK-LIB Build Successful\nVersion: %s", job, appversion))
		}

	} else {
		lop.E("No match found")
		GetNdkResult(fmt.Sprintf("%s-NDK-LIB Build Failed: key-length not matched", job), dingTalkRobotToken)
		ctx.String(200, fmt.Sprintf("%s-NDK-LIB Build Failed: key-length not matched", job))
	}

	ctx.JSON(200, "CompileFromWeb Successful!!!.")
}

/* 打包助手ndk编译入口 */
func Compile(ctx *gin.Context) {
	ndk_content := ctx.Request.Header.Get("ndk_content")
	if ndk_content == "" {
		lop.E("ndk_content is empty")
	}
	lop.D("ndk_content is", ndk_content)

	rawJsonBytes, err := io.ReadAll(ctx.Request.Body)
	job := ctx.Request.Header.Get("job")
	if err != nil {
		lop.E(err.Error())
	}
	rawJson := string(rawJsonBytes)
	rawJson = strings.ReplaceAll(rawJson, " ", "") 
	lop.D(rawJson)

	str := rawJson //strings.TrimSpace(rawJson) //rawJson
	parts := strings.Split(str, ",")

	versionParts := strings.Split(parts[0], ":")
	keyParts := strings.Split(parts[1], ":")

	version := versionParts[1]
	key := keyParts[1]
	dingTalkRobotToken := "102a9d3a094f213c6a90000l"

	if len(version) >= 3 && len(key) == 32 {
		appversion := version
		deskey := generateRandom2Chars() + key

		viper.SetConfigName(job)
		viper.AddConfigPath("./config")

		err := viper.ReadInConfig()
		if err != nil {
			lop.E("Error reading config file, ", err)
			GetNdkResult(fmt.Sprintf("%s-NDK-LIB Build Failed: Error reading config file", job), dingTalkRobotToken)
			ctx.String(200, "NDK-LIB Config Failed: Error reading config file")
		}
		// 获取所有的键
		keys := viper.AllKeys()

		lop.D("\njob:", job, "\nappversion:", appversion, "\ndeskey:", deskey)

		ndkCompileParamsMap := make(map[string]string, 0)
		for _, key := range keys {
			ndkCompileParamsMap[key] = viper.GetString(key)
			lop.D(fmt.Sprintf("%s: %v\n", key, viper.Get(key)))
		}
		dingTalkRobotToken = ndkCompileParamsMap["ding_talk_robot_token"]
		ndklibCompileCmdOut, ndklibCompileCmdErr := executeGradlewCompile(job, deskey, appversion, ndkCompileParamsMap)

		if ndklibCompileCmdErr != nil {
			lop.E("ndklibCompileCmdOut Failed Out:\n", ndklibCompileCmdErr.Error(), ndklibCompileCmdOut)
			GetNdkResult(fmt.Sprintf("%s-NDK-LIB Build Failed:%s\nVersion: %s\n", job, ndklibCompileCmdErr.Error(), appversion), dingTalkRobotToken)
			ctx.String(200, fmt.Sprintf("%s-NDK-LIB Build Failed:%s\nVersion: %s\n", job, ndklibCompileCmdErr.Error(), appversion))
		} else {
			lop.I("ndklibCompileCmdOut Combined Out:\n", ndklibCompileCmdOut)
			GetNdkResult(fmt.Sprintf("%s-NDK-LIB Build Successful\nVersion: %s\nUpdate Time: %s", job, appversion, time.Now().Format("2006-01-02 15:04:05")), dingTalkRobotToken)
			ctx.String(200, fmt.Sprintf("%s-NDK-LIB Build Successful\nVersion: %s", job, appversion))
		}

	} else {
		lop.E("No match found")
		GetNdkResult(fmt.Sprintf("%s-NDK-LIB Build Failed: key-length not matched", job), dingTalkRobotToken)
		ctx.String(200, fmt.Sprintf("%s-NDK-LIB Build Failed: key-length not matched", job))
	}
}

func executeGradlewCompile(job string, deskey string, appversion string, ndkCompileParamsMap map[string]string) (string, error) {
	ndklibCompileCmd := exec.Command("./gradlew", "ndkdevlib_common:actionBuildAndUploadLib",
		"-Dorg.gradle.console=plain",
		"-Dorg.gradle.daemon=false",
		"-Purl=http://192.168.20.8:9000/repository/juxiao_android/",
		"-Pname=taohui",
		"-Ppassword=4hjBvKn8sV2D6g",
		fmt.Sprintf("-Psignature_code=%s", ndkCompileParamsMap["signature_code"]),
		fmt.Sprintf("-Psignature_code_local=%s", ndkCompileParamsMap["signature_code_local"]),
		fmt.Sprintf("-Psignature_code_develop=%s", ndkCompileParamsMap["signature_code_develop"]),
		fmt.Sprintf("-Pdes_encrypt_key=%s", ndkCompileParamsMap["des_encrypt_key"]),
		fmt.Sprintf("-Paes_encrypt_key=%s", ndkCompileParamsMap["aes_encrypt_key"]),
		fmt.Sprintf("-Paes_encrypt_iv=%s", ndkCompileParamsMap["aes_encrypt_iv"]),
		fmt.Sprintf("-Pndk_package_name=%s", ndkCompileParamsMap["ndk_package_name"]),
		fmt.Sprintf("-Pnetease_key=%s", ndkCompileParamsMap["netease_key"]),
		fmt.Sprintf("-Pgroup_id=%s", ndkCompileParamsMap["group_id"]),
		fmt.Sprintf("-Pdes_key=\"%s\"", deskey),
		fmt.Sprintf("-Pdes_key_halo_local=%s", ndkCompileParamsMap["des_key_halo_local"]),
		fmt.Sprintf("-Pagora_key=%s", ndkCompileParamsMap["agora_key"]),
		fmt.Sprintf("-Papp_version=%s", appversion),
		fmt.Sprintf("-Pflavor=%s", ndkCompileParamsMap["flavor"]),
		"--gradle-user-home=../common-android-ndk-BuildCache",
		"--parallel")

	ndklibCompileCmd.Dir = fmt.Sprintf("/data/soft/jenkins_data/workspace/%s-common-android-ndk", job)
	lop.D(job, ndklibCompileCmd.String())
	ndklibCompileCmdOut, ndklibCompileCmdErr := ndklibCompileCmd.CombinedOutput()

	if ndklibCompileCmdErr != nil {
		return "", ndklibCompileCmdErr
	}

	return string(ndklibCompileCmdOut), nil
}
func generateRandom2Chars() string {
	rand.Seed(time.Now().UnixNano())
	chars := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	result := make([]byte, 2)
	for i := range result {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return string(result)
}

func GetNdkResult(rawJson string, ding_talk_robot_token string) {
	url := fmt.Sprintf("https://connector.dingtalk.com/webhook/flow/%s", ding_talk_robot_token)
	data := rawJson

	resp, err := http.Post(url, "text/plain", bytes.NewBufferString(data))
	if err != nil {
		lop.E(err)
	}
	lop.D(rawJson)
	defer resp.Body.Close()

	lop.I("Response status:", resp.Status)
}

/*
./gradlew clean ndkdevlib_common:actionBuildAndUploadLib \
-Dorg.gradle.console=plain \
-Dorg.gradle.daemon=false \
-Purl=http://192.168.20.8:9000/repository/juxiao_android/ \
-Pname=taohui \
-Ppassword=4hjBvKn8sV2D6g \
-Psignature_code=2059764945 \
-Psignature_code_local=-2022688233 \
-Psignature_code_develop=-2022688233 \
-Pdes_encrypt_key='"9ze030ce22"' \
-Paes_encrypt_key='"2A2sxAeYb8cf8PkQim"' \
-Paes_encrypt_iv='"q4wK8ED5HeMfeRsP1J"' \
-Pndk_package_name='"com.wegolive.android"' \
-Pnetease_key='"FP25c9802248c05427d589e59fa8e3c653"' \
-Pgroup_id=com.wego \
-Pdes_key='"lZ955763fcfffc468c239a8051489c4458"' \
-Pdes_key_halo_local="" \
-Pagora_key='"b9bef192a327d54dfcb4f0b1e314e0e0f6"' \
-Papp_version=1.0.6.3 \
--gradle-user-home=../common-android-ndk-BuildCache \
--parallel


./gradlew -Dandroid.enableR8.fullMode=false \
-Dorg.gradle.workers.max=60 \
-Dorg.gradle.jvmargs='-Xmx8192m -Xms4096m -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8 -XX:+UseParallelGC -XX:MaxMetaspaceSize=1024m' \
-Dorg.gradle.console=plain \
-Dorg.gradle.daemon=false \
-DDOKIT_METHOD_SWITCH=false \
clean assemblegoogleDebug \
--build-cache \
--gradle-user-home=../Lami-Android-DebugBuildCache \
--profile \
--max-workers=60
*/
