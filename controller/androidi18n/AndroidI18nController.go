package androidi18n

import (
	"encoding/csv"
	"hi_golang/tools"
	csv2i18n "hi_golang/tools/android/csv2I18n"
	"hi_golang/tools/compress"
	"hi_golang/tools/lop"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/xuri/excelize/v2"
)

func GetIndex(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "androidi18n_index.html", nil)
}

func Parse(ctx *gin.Context) {

	originalXLogFilePath := "./tmp/androidi18n/parse/"
	os.MkdirAll(originalXLogFilePath, os.ModePerm)

	file, _ := ctx.FormFile("file")
	if file.Size <= 0 {
		ctx.String(http.StatusOK, "文件内容错误")
		return
	}
	if strings.HasSuffix(file.Filename, ".xlsx") || strings.HasSuffix(file.Filename, ".csv") {
		version := ctx.Request.FormValue("version")
		abbrs := ctx.Request.FormValue("abbrs")
		countryAbbrArray := strings.Split(abbrs, ",")
		countryAbbrNotEmptyArray := make([]string, 0)
		for _, countryAbbr := range countryAbbrArray {
			if len(countryAbbr) != 0 {
				countryAbbrNotEmptyArray = append(countryAbbrNotEmptyArray, countryAbbr)
			}
		}

		lop.I(version, countryAbbrNotEmptyArray, originalXLogFilePath+file.Filename)
		ctx.SaveUploadedFile(file, originalXLogFilePath+file.Filename)
		path := originalXLogFilePath + file.Filename

		outputFileTar := tools.GenFileName(file.Filename)
		outputFolder := "tmp/androidi18n/outputs/" + outputFileTar

		if hasSuffix := strings.HasSuffix(file.Filename, ".xlsx"); hasSuffix {
			/* 处理excel表格的翻译 */
			if csvFilePath, err := excelToCsv(file.Filename, path, 0, countryAbbrNotEmptyArray); err == nil {
				if err := csv2i18n.Csv2AndroidI18n(outputFolder, *csvFilePath, version, countryAbbrNotEmptyArray); err != nil {
					ctx.String(http.StatusOK, err.Error())
					return
				}
			} else {
				ctx.String(http.StatusOK, "xlsx转csv出错"+err.Error())
				return
			}
		} else {
			/* 处理表格的翻译 */
			if err := csv2i18n.Csv2AndroidI18n(outputFolder, path, version, countryAbbrNotEmptyArray); err != nil {
				ctx.String(http.StatusOK, err.Error())
				return
			}
		}
		compress.Tar(outputFolder, originalXLogFilePath)
		ctx.FileAttachment(originalXLogFilePath+outputFileTar+".tar", "outputs.tar")

	} else {
		ctx.String(http.StatusOK, "文件格式错误, 只支持csv和xlsx后缀的文件名")
		return
	}
}

func excelToCsv(filename, path string, sheetIndex int, countryAbbrNotEmptyArray []string) (*string, error) {
	defer func() {
		if r := recover(); r != nil {
			lop.E("Recovered from panic:", r)
		}
	}()
	excel, err := excelize.OpenFile(path)
	if err != nil {
		return nil, err
	}

	rows, err := excel.Rows(excel.GetSheetName(sheetIndex))
	if err != nil {
		return nil, err
	}

	csvFileName := tools.GenFileName(filename) + ".csv"
	csvFilePath := "./tmp/androidi18n/parse/" + csvFileName

	file, _ := os.OpenFile(csvFilePath,
		os.O_CREATE|os.O_RDWR|os.O_TRUNC,
		0777)
	defer file.Close()
	csvw := csv.NewWriter(file)
	defer csvw.Flush()

	for rows.Next() {
		cols, err := rows.Columns()
		line := ``

		for index, col := range cols {
			/* 需要特殊处理一下换行的问题, 但是这样硬编码可能不兼容一些其他的 */
			cols[index] = strings.ReplaceAll(col, "\n", "\\n")
			/* 去除最后一个逗号 */
			if index < len(cols)-1 {
				line += col + `,`
			} else {
				line += col
			}
		}

		// line = strings.ReplaceAll(line, "\n", "\\n")
		// file.WriteString(line + "\n")
		if err != nil {
			return nil, err
		}
		csvw.Write(cols)
	}
	return &csvFilePath, nil
}
