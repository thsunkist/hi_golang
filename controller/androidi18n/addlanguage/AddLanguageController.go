package addlanguage

import (
	"archive/zip"
	"encoding/csv"
	"encoding/xml"
	"fmt"
	csv2i18nwithkey "hi_golang/tools/android/i18nprocessor/csv2I18nwithkey"
	"hi_golang/tools/compress"
	"hi_golang/tools/lop"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

type HostProperties struct {
	XMLName xml.Name `xml:"resources"`
	Tags    []Tag    `xml:"string"`
}

type Tag struct {
	Key string `xml:"name,attr"`
	/* <tag>value</tag> */
	Value string `xml:",innerxml"`
}

func GetAddLanguageIndex(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "androidi18n_add_language.html", nil)
}

/* POST */
func Upload(ctx *gin.Context) {
	/* TODO */
	originalXLogFilePath := filepath.Join("tmp", "androidi18n", "add_language")
	os.MkdirAll(originalXLogFilePath, os.ModePerm)
	uploadFile, _ := ctx.FormFile("file")
	if uploadFile.Size <= 0 {
		ctx.String(http.StatusOK, "文件内容错误")
		return
	}

	if !strings.HasSuffix(uploadFile.Filename, ".zip") {
		ctx.String(http.StatusOK, "文件格式错误")
		return
	}

	newFileName := fmt.Sprintf("%s.%d", uploadFile.Filename, time.Now().UnixNano()/int64(time.Microsecond))

	newAbsoluteFilePath := filepath.Join(originalXLogFilePath, newFileName)
	ctx.SaveUploadedFile(uploadFile, newAbsoluteFilePath)
	filterZipFile(newAbsoluteFilePath, "values")
	/* 解压缩 */

	unzipPath := filepath.Join("tmp", "androidi18n", "add_language", "upload", newFileName+"_unzip")

	// fd, err := os.OpenFile(unzipPath, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0777)

	// if err != nil {

	unzipSourceErr := unzipSource(newAbsoluteFilePath, unzipPath)
	if unzipSourceErr != nil {
		ctx.String(http.StatusOK, "解压缩错误")
		return
	}
	dir, _ := WalkMatch(unzipPath, "*.xml")

	var map1 = make(map[string]string)
	for i := 0; i < len(dir); i++ {
		lop.D(dir[i])
		abspath := filepath.Dir(dir[i])
		if strings.Contains(abspath, "zh") {
			map1["values-zh"] = abspath
		} else {
			map1["values-en"] = abspath
		}
	}
	lop.D(map1)

	cnI18nCsvMap := getZhI18nMap(map1["values-zh"])
	enI18nCsvMap := getZhI18nMap(map1["values-en"])

	allCsvDataArray := [][3]string{}
	titles := [3]string{}
	titles[0] = "key"
	titles[1] = "en"
	titles[2] = "cn"
	allCsvDataArray = append(allCsvDataArray, titles)

	for k, enValue := range enI18nCsvMap {
		enCnArray := [3]string{}

		cnValue, exist := cnI18nCsvMap[k]
		if exist {
			enCnArray[0] = k
			enCnArray[1] = enValue[0]
			enCnArray[2] = cnValue[0]
		} else {
			enCnArray[0] = k
			enCnArray[1] = enValue[0]
			enCnArray[2] = ""
		}

		allCsvDataArray = append(allCsvDataArray, enCnArray)
	}
	os.MkdirAll("outputs", 0777)

	csvFile, _ := os.Create(filepath.Join(unzipPath, "result.csv"))
	defer csvFile.Close()
	csvwriter := csv.NewWriter(csvFile)
	for _, empRow := range allCsvDataArray {
		// lop.I(empRow[0], empRow[1], empRow[2])
		_ = csvwriter.Write([]string{empRow[0], empRow[1], empRow[2]})
	}
	csvwriter.Flush()
	ctx.FileAttachment(filepath.Join(unzipPath, "result.csv"), "result.csv")
}

func WalkMatch(root, pattern string) ([]string, error) {
	var matches []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if matched, err := filepath.Match(pattern, filepath.Base(path)); err != nil {
			return err
		} else if matched {
			matches = append(matches, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return matches, nil
}

func Generate(ctx *gin.Context) {
	/* TODO */

	originalXLogFilePath := filepath.Join("tmp", "androidi18n", "add_language", "generate")
	os.MkdirAll(originalXLogFilePath, os.ModePerm)
	uploadFile, _ := ctx.FormFile("file")
	if uploadFile.Size <= 0 {
		ctx.String(http.StatusOK, "文件内容错误")
		return
	}

	if !strings.HasSuffix(uploadFile.Filename, ".csv") {
		ctx.String(http.StatusOK, "文件格式错误")
		return
	}

	newFileName := fmt.Sprintf("%s.%d", uploadFile.Filename, time.Now().UnixNano()/int64(time.Microsecond))

	newAbsoluteFilePath := filepath.Join(originalXLogFilePath, newFileName)
	ctx.SaveUploadedFile(uploadFile, newAbsoluteFilePath)

	outputPath := csv2i18nwithkey.Csv2I18nWithKey(newAbsoluteFilePath)

	lop.D(outputPath)
	compress.Tar(outputPath, originalXLogFilePath)
	ctx.FileAttachment(outputPath+".tar", "output.tar")

}

func getZhI18nMap(zhDirPath string) map[string][]string {
	cnI18nCsvMap := make(map[string][]string, 0)

	zhDir, _ := ioutil.ReadDir(zhDirPath)
	zhHostProperties := HostProperties{}

	cnHostPropertiesArray := make([]interface{}, 0)
	for _, fi := range zhDir {
		filePtr, _ := os.OpenFile(zhDirPath+"/"+fi.Name(), os.O_RDONLY, 0777)
		contentBytes, _ := ioutil.ReadAll(filePtr)
		xml.Unmarshal(contentBytes, &zhHostProperties)
		cnHostPropertiesArray = append(cnHostPropertiesArray, zhHostProperties)
	}
	for _, hostProperties := range cnHostPropertiesArray {
		var cn = hostProperties.(HostProperties)
		for _, t := range cn.Tags {
			cnI18nCsvMap[t.Key] = append(cnI18nCsvMap[t.Key], t.Value)
		}
	}
	return cnI18nCsvMap
}

func unzipSource(source, destination string) error {
	// 1. Open the zip file
	reader, err := zip.OpenReader(source)
	if err != nil {
		return err
	}
	defer reader.Close()

	// 2. Get the absolute destination path
	destination, err = filepath.Abs(destination)
	if err != nil {
		return err
	}

	// 3. Iterate over zip files inside the archive and unzip each of them
	for _, f := range reader.File {
		err := unzipFile(f, destination)
		if err != nil {
			return err
		}
	}

	return nil
}

func unzipFile(f *zip.File, destination string) error {
	// 4. Check if file paths are not vulnerable to Zip Slip
	filePath := filepath.Join(destination, f.Name)
	if !strings.HasPrefix(filePath, filepath.Clean(destination)+string(os.PathSeparator)) {
		return fmt.Errorf("invalid file path: %s", filePath)
	}

	// 5. Create directory tree
	if f.FileInfo().IsDir() {
		if err := os.MkdirAll(filePath, os.ModePerm); err != nil {
			return err
		}
		return nil
	}

	if err := os.MkdirAll(filepath.Dir(destination), os.ModePerm); err != nil {
		return err
	}

	// 6. Create a destination file for unzipped content
	destinationFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
	if err != nil {
		return err
	}
	defer destinationFile.Close()

	// 7. Unzip the content of a file and copy it to the destination file
	zippedFile, err := f.Open()
	if err != nil {
		return err
	}
	defer zippedFile.Close()
	lop.D(destinationFile)
	if _, err := io.Copy(destinationFile, zippedFile); err != nil {
		return err
	}
	return nil
}

func filterZipFile(zipFilePath string, prefix string) error {
	// 打开zip文件
	r, err := zip.OpenReader(zipFilePath)
	if err != nil {
		return err
	}
	defer r.Close()

	// 创建一个新的zip文件
	newZipFile, err := os.Create(zipFilePath + ".tmp")
	if err != nil {
		return err
	}
	defer newZipFile.Close()

	// 创建一个zip writer
	w := zip.NewWriter(newZipFile)
	defer w.Close()

	// 遍历原始zip文件中的所有文件
	for _, f := range r.File {
		// 如果文件名以指定前缀开头，则将其添加到新的zip文件中
		if strings.HasPrefix(f.Name, prefix) {
			rc, err := f.Open()
			if err != nil {
				return err
			}
			defer rc.Close()

			// 创建一个新文件并将其添加到新的zip文件中
			newFile, err := w.Create(f.Name)
			if err != nil {
				return err
			}

			// 将原始文件内容复制到新文件中
			_, err = io.Copy(newFile, rc)
			if err != nil {
				return err
			}
		}
	}

	// 删除原始zip文件并将新的zip文件重命名为原始文件名
	err = os.Remove(zipFilePath)
	if err != nil {
		return err
	}

	err = os.Rename(zipFilePath+".tmp", zipFilePath)
	if err != nil {
		return err
	}

	return nil
}
