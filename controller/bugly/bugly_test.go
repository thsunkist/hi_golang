package bugly

import (
	"fmt"
	"strings"
	"testing"
)

func TestGetAppIcon(_ *testing.T) {

	var host = "https://tools.shijianline.cn:9004/assets/images/controller/bugly/ic_%[1]s_launcher.png"
	var appName = "lami"

	if strings.Contains(strings.ToLower(appName), strings.ToLower("habby")) {
		fmt.Println(fmt.Sprintf(host, "habby"))
	} else if strings.Contains(strings.ToLower(appName), "kiyo") {
		fmt.Println(fmt.Sprintf(host, "kiyo"))
	} else if strings.Contains(strings.ToLower(appName), "lami") {
		fmt.Println(fmt.Sprintf(host, "lami"))
	} else if strings.Contains(strings.ToLower(appName), "ohla") {
		fmt.Println(fmt.Sprintf(host, "ohla"))
	} else if strings.Contains(strings.ToLower(appName), "olamet") {
		fmt.Println(fmt.Sprintf(host, "olamet"))
	} else if strings.Contains(strings.ToLower(appName), "ligo") {
		fmt.Println(fmt.Sprintf(host, "ligo"))
	} else if strings.Contains(strings.ToLower(appName), "haki") {
		fmt.Println(fmt.Sprintf(host, "haki"))
	} else if strings.Contains(strings.ToLower(appName), "sweet") {
		fmt.Println(fmt.Sprintf(host, "sweet"))
	} else if strings.Contains(strings.ToLower(appName), "yobi") {
		fmt.Println(fmt.Sprintf(host, "yobi"))
	} else {
		fmt.Println(fmt.Sprintf(host, "default"))
	}

}
