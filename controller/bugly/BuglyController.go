package bugly

import (
	"bytes"
	"encoding/json"
	"fmt"
	"hi_golang/tools/lop"
	"io/ioutil"
	"os/exec"
	"sort"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

/*
可以将Webhook消息自定义POST到指定的地址，具体的参数信息可参考API文档。
https://oapi.dingtalk.com/robot/send?access_token=09e8c31de48dbcb69a597aafb86a3501d2b229487ce3bbe80bd81c79edc73124
keyword: LightChat

https://bugly.qq.com/docs/user-guide/webhook-bugly/?v=1684825573504
*/

type CurlEntity struct {
	Rate           string
	AppName        string
	AppIcon        string
	RobotAccessUrl string
}

func (this CurlEntity) GetAppIcon() string {
	var host = "https://tools.shijianline.cn:9004/assets/images/controller/bugly/ic_%[1]s_launcher.png"

	if strings.Contains(strings.ToLower(strings.ToLower(this.AppName)), "habby") {
		return fmt.Sprintf(host, "habby")
	} else if strings.Contains(strings.ToLower(this.AppName), "kiyo") {
		return fmt.Sprintf(host, "kiyo")
	} else if strings.Contains(strings.ToLower(this.AppName), "lami") {
		return fmt.Sprintf(host, "lami")
	} else if strings.Contains(strings.ToLower(this.AppName), "ohla") {
		return fmt.Sprintf(host, "ohla")
	} else if strings.Contains(strings.ToLower(this.AppName), "olamet") {
		return fmt.Sprintf(host, "olamet")
	} else if strings.Contains(strings.ToLower(this.AppName), "ligo") {
		return fmt.Sprintf(host, "ligo")
	} else if strings.Contains(strings.ToLower(this.AppName), "haki") {
		return fmt.Sprintf(host, "haki")
	} else if strings.Contains(strings.ToLower(this.AppName), "sweet") {
		return fmt.Sprintf(host, "lightchat")
	} else if strings.Contains(strings.ToLower(this.AppName), "yobi") {
		return fmt.Sprintf(host, "yobi")
	} else if strings.Contains(strings.ToLower(this.AppName), "fancy") {
		return fmt.Sprintf(host, "fancy")
	} else if strings.Contains(strings.ToLower(this.AppName), "olla") {
		return fmt.Sprintf(host, "olla")
	} else if strings.Contains(strings.ToLower(this.AppName), "wego") {
		return fmt.Sprintf(host, "wego")
	} else if strings.Contains(strings.ToLower(this.AppName), "yolo") {
		return fmt.Sprintf(host, "yolo")
	} else if strings.Contains(strings.ToLower(this.AppName), "larki") {
		return fmt.Sprintf(host, "larki")
	} else if strings.Contains(strings.ToLower(this.AppName), "nyko") {
		return fmt.Sprintf(host, "nyko")
	} else {
		return fmt.Sprintf(host, "default")
	}
}

func BuglyAnalytics(ctx *gin.Context) {
	rawJsonBytes, err := ioutil.ReadAll(ctx.Request.Body)
	if err != nil {
	}
	rawJson := string(rawJsonBytes)

	var entity = new(BuglyWebhookRequestEntity)
	if err := json.Unmarshal([]byte(rawJson), &entity); err != nil {
		fmt.Println("err: " + err.Error())
	}

	/* 手动反转一下 */
	entity.EventContent.Datas = reverseArray(entity.EventContent.Datas)
	/* 过滤掉用户小于1000的版本 */
	var datas = make([]struct {
		AccessUser int    `json:"accessUser"`
		CrashCount int    `json:"crashCount"`
		CrashUser  int    `json:"crashUser"`
		Version    string `json:"version"`
		URL        string `json:"url"`
	}, 0)
	for _, element := range entity.EventContent.Datas {
		if element.AccessUser > 1000 {
			datas = append(datas, element)
		}
	}
	/* TODO TIPS: 排序拿到用户量最高的前三进行崩溃率判定 */
	sort.Slice(datas, func(i, j int) bool {
		return datas[i].AccessUser > datas[j].AccessUser
	})
	entity.EventContent.Datas = datas

	lop.I("Android", rawJson)

	var sb strings.Builder
	var isNotify bool = false
	var isAtDeveloper bool = false
	lop.D(isAtDeveloper)
	for index, data := range entity.EventContent.Datas {
		rate := (float64(data.CrashUser) / float64(data.AccessUser)) * 100

		if index == 0 && rate >= 2.0 {
			isNotify = true
			isAtDeveloper = true
		}

		rate2 := strconv.FormatFloat(rate, 'f', 2, 32)

		lop.D("Android", fmt.Sprintf(`版本号[%[1]s] 用户量: %[3]v 崩溃率: %[3]v%%`, data.Version, data.AccessUser, rate2))
		sb.WriteString(fmt.Sprintf("版本号[%[1]s] 崩溃率: %[2]v%%\n", data.Version, rate2))
	}

	if !isNotify {
		return
	}

	var lightChatCurlEntity = CurlEntity{
		Rate:           sb.String(),
		AppName:        entity.EventContent.AppName,
		RobotAccessUrl: `https://oapi.dingtalk.com/robot/send?access_token=943d423d9373a69a83c60e18df825584065c9f7d2c26cd1e60a9971da5e0b0e4`,
	}
	/*
	   Ligo-IOS
	   https://bugly.qq.com/v2/crash-reporting/dashboard/f4ab857764?pid=2
	   f4ab857764
	*/
	var cmd = exec.Command(`curl`, lightChatCurlEntity.RobotAccessUrl,
		`-H`, `Content-Type: application/json`,
		`-d`, fmt.Sprintf(`{
								"msgtype": "link",
								"link": {
									"text": "%[1]s",
									"title": "%[2]s",
									"picUrl": "%[3]s",
									"messageUrl": "%[4]s"
								}
							}`,
			lightChatCurlEntity.Rate,
			fmt.Sprintf("%s\n(bugly crash trend)\n⚠️崩溃率过高,请及时修复", lightChatCurlEntity.AppName),
			lightChatCurlEntity.GetAppIcon(), entity.EventContent.AppURL))

	var stdout bytes.Buffer
	var stderr bytes.Buffer

	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	lop.D(cmd.String())
	err = cmd.Run()

	if err != nil {
		fmt.Printf("Stdout: %s\n", stdout.String())
		fmt.Printf("Stderr: %s\n", stderr.String())
	} else {
		fmt.Printf("Stdout: %s\n", stdout.String())
	}
	fmt.Println(err)

	ctx.JSON(200, gin.H{"message": "successfully."})
}

func reverseArray(arr []struct {
	AccessUser int    `json:"accessUser"`
	CrashCount int    `json:"crashCount"`
	CrashUser  int    `json:"crashUser"`
	Version    string `json:"version"`
	URL        string `json:"url"`
}) []struct {
	AccessUser int    `json:"accessUser"`
	CrashCount int    `json:"crashCount"`
	CrashUser  int    `json:"crashUser"`
	Version    string `json:"version"`
	URL        string `json:"url"`
} {
	for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 {
		arr[i], arr[j] = arr[j], arr[i]
	}
	return arr
}

/**
curl -X POST 'https://tools.shijianline.cn:9004/bugly/analytics' \
   -H 'Content-Type: application/json' \
   -d '{
  "eventType": "bugly_crash_trend",
  "timestamp": 1462780713515,
  "isEncrypt": 0,
  "eventContent": {
    "datas": [
      {
        "accessUser": 12972,
        "crashCount": 21,
        "crashUser": 20,
        "version": "1.2.3",
        "url": "http://bugly.qq.com/realtime?app=1104512706&pid=1&ptag=1005-10003&vers=0.0.0.12.12&time=last_7_day&tab=crash"
      }
    ],
    "appId": "1104512706",
    "platformId": 1,
"appName": "AF",
    "date": "20160508",
"appUrl":"http://bugly.qq.com/issueIndex?app=1104512706&pid=1&ptag=1005-10000"
  },
  "signature": "ACE346A4AE13A23A52A0D0D19350B466AF51728A"
}'
*/

func BuglyAnalyticsIOS(ctx *gin.Context) {
	rawJsonBytes, err := ioutil.ReadAll(ctx.Request.Body)
	if err != nil {
		lop.E("IOS", err.Error())
	}
	rawJson := string(rawJsonBytes)
	lop.I("IOS", rawJson)
	var entity = new(BuglyWebhookRequestEntity)
	if err := json.Unmarshal([]byte(rawJson), &entity); err != nil {
		lop.E("IOS", "err: "+err.Error())
	}

	/* 手动反转一下 */
	entity.EventContent.Datas = reverseArray(entity.EventContent.Datas)
	/* 过滤掉用户小于1000的版本 */
	var datas = make([]struct {
		AccessUser int    `json:"accessUser"`
		CrashCount int    `json:"crashCount"`
		CrashUser  int    `json:"crashUser"`
		Version    string `json:"version"`
		URL        string `json:"url"`
	}, 0)

	datas = append(datas, entity.EventContent.Datas...)

	/* TODO TIPS: 排序拿到用户量最高的前三进行崩溃率判定 */
	sort.Slice(datas, func(i, j int) bool {
		return datas[i].AccessUser > datas[j].AccessUser
	})
	entity.EventContent.Datas = datas

	lop.I("IOS", rawJson)

	var sb strings.Builder
	var isNotify bool = false

	for index, data := range entity.EventContent.Datas {
		rate := (float64(data.CrashUser) / float64(data.AccessUser)) * 100

		if index == 0 && rate >= 0.1 {
			isNotify = true
		}

		rate2 := strconv.FormatFloat(rate, 'f', 2, 32)

		lop.D("IOS", fmt.Sprintf(`版本号[%[1]s] 用户量: %[3]v 崩溃率: %[3]v%%`, data.Version, data.AccessUser, rate2))
		sb.WriteString(fmt.Sprintf("版本号[%[1]s] 崩溃率: %[2]v%%\n", data.Version, rate2))
	}

	if !isNotify {
		return
	}

	var lightChatCurlEntity = CurlEntity{
		Rate:           sb.String(),
		AppName:        entity.EventContent.AppName,
		RobotAccessUrl: `https://oapi.dingtalk.com/robot/send?access_token=a44f270b462fefcd9f83f2f23477b2c9386e5052bee9b50a11e19d2ebbe00b57`,
	}

	var cmd = exec.Command(`curl`, lightChatCurlEntity.RobotAccessUrl,
		`-H`, `Content-Type: application/json`,
		`-d`, fmt.Sprintf(`{
"msgtype": "link",
"link": {
"text": "%[1]s",
"title": "%[2]s",
"picUrl": "%[3]s",
"messageUrl": "%[4]s"
}
}`,
			lightChatCurlEntity.Rate,
			fmt.Sprintf("%s\n(bugly crash trend)\n⚠️崩溃率过高,请及时修复", lightChatCurlEntity.AppName),
			lightChatCurlEntity.GetAppIcon(), entity.EventContent.AppURL))
	//exec.Command(`curl`, commandBody)

	var stdout bytes.Buffer
	var stderr bytes.Buffer

	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err = cmd.Run()

	if err != nil {
		fmt.Printf("Stdout: %s\n", stdout.String())
		fmt.Printf("Stderr: %s\n", stderr.String())
	} else {
		fmt.Printf("Stdout: %s\n", stdout.String())
	}
	fmt.Println(err)

	ctx.JSON(200, gin.H{"message": "successfully."})
}
