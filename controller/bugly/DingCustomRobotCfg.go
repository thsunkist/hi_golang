package bugly

import (
	"fmt"
	"os/exec"
)

var buglyReporterCmd = []*exec.Cmd{}

func init() {
	buglyReporterCmd = append(buglyReporterCmd, lightChatBuglyReporterCmd)
}

var lightChatBuglyReporterCmd = exec.Command(`curl`, `https://oapi.dingtalk.com/robot/send?access_token=09e8c31de48dbcb69a597aafb86a3501d2b229487ce3bbe80bd81c79edc73124`,
	`-H`, `Content-Type: application/json`,
	`-d`, fmt.Sprintf(`{
"msgtype": "link",
"link": {
"text": "%[1]s",
"title": "LightChat(bugly crash trend)",
"picUrl": "http://i1.img.969g.com/down/imgx2015/02/27/289_095120_f0936.jpg",
"messageUrl": "https://bugly.qq.com/v2/workbench"
}
}`, "***%"))
