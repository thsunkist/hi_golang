package bugly

type BuglyWebhookRequestEntity struct {
	EventType    string `json:"eventType"`
	Timestamp    int64  `json:"timestamp"`
	IsEncrypt    int    `json:"isEncrypt"`
	EventContent struct {
		Datas []struct {
			AccessUser int    `json:"accessUser"`
			CrashCount int    `json:"crashCount"`
			CrashUser  int    `json:"crashUser"`
			Version    string `json:"version"`
			URL        string `json:"url"`
		} `json:"datas"`
		AppID      string `json:"appId"`
		PlatformID int    `json:"platformId"`
		Date       string `json:"date"`
		AppName    string `json:"appName"`
		AppURL     string `json:"appUrl"`
	} `json:"eventContent"`
	Signature string `json:"signature"`
}
