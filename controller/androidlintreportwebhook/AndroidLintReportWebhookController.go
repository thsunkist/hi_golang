package androidlintreportwebhook

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hi_golang/tools/lop"
	"io"
	"net/http"
	"os/exec"
	"strings"

	"github.com/gin-gonic/gin"
)

type Job struct {
	Class string `json:"_class"`
	Color string `json:"color"`
	Name  string `json:"name"`
	URL   string `json:"url"`
}
type JenkinsJobsApiResults struct {
	Class          string `json:"_class"`
	AssignedLabels []struct {
		Name string `json:"name"`
	} `json:"assignedLabels"`
	Description     interface{} `json:"description"`
	Jobs            []Job       `json:"jobs"`
	Mode            string      `json:"mode"`
	NodeDescription string      `json:"nodeDescription"`
	NodeName        string      `json:"nodeName"`
	NumExecutors    int64       `json:"numExecutors"`
	OverallLoad     struct{}    `json:"overallLoad"`
	PrimaryView     struct {
		Class string `json:"_class"`
		Name  string `json:"name"`
		URL   string `json:"url"`
	} `json:"primaryView"`
	QuietDownReason interface{} `json:"quietDownReason"`
	QuietingDown    bool        `json:"quietingDown"`
	SlaveAgentPort  int64       `json:"slaveAgentPort"`
	UnlabeledLoad   struct {
		Class string `json:"_class"`
	} `json:"unlabeledLoad"`

	URL         string `json:"url"`
	UseCrumbs   bool   `json:"useCrumbs"`
	UseSecurity bool   `json:"useSecurity"`
	Views       []struct {
		Class string `json:"_class"`
		Name  string `json:"name"`
		URL   string `json:"url"`
	} `json:"views"`
}

func Commit(c *gin.Context) {

	url := "http://127.0.0.1:9001/api/json?pretty=true"
	username := "taohui"
	password := "jenkins"
	jenkinsDataWorkspacePath := "/data/soft/jenkins_data/workspace"

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		c.String(http.StatusInternalServerError, "Error creating request")
		return
	}

	auth := username + ":" + password
	basicAuth := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))
	req.Header.Set("Authorization", basicAuth)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		c.String(http.StatusInternalServerError, "Error sending request")
		return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		c.String(http.StatusInternalServerError, "Error reading response")
		return
	}
	var jenkinsJobsApiResults JenkinsJobsApiResults
	if err := json.Unmarshal(body, &jenkinsJobsApiResults); err != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("Error unmarshalling response: %s", err.Error()))
		return
	}
	c.String(http.StatusOK, string(body))
	JDK21 := ""
	JDK17 := "/usr/local/jdk-17.0.10/bin/java"
	JDK11 := ""

	lop.I(JDK11, JDK17, JDK21)
	for _, job := range jenkinsJobsApiResults.Jobs {
		currentJob := job
		if strings.HasSuffix(job.Name, "Kiyo-Android") ||
			strings.HasSuffix(job.Name, "Ligo-Android") {
			go func(job *Job) {
				command := fmt.Sprintf("cd %s/%s && nohup ./gradlew clean lintgoogledebug -Dorg.gradle.daemon=false > ../%s_lint_report.log &", jenkinsDataWorkspacePath, job.Name, job.Name)
				lop.I(job.Name)
				lop.I(command)
				cmd := exec.Command("sh", "-c", command)
				if output, err := cmd.CombinedOutput(); err != nil {
					c.String(http.StatusInternalServerError, fmt.Sprintf("Error lint report generated: %s", err.Error()))
					return
				} else {
					lop.I("Error executing command: ", err, string(output))
				}
				cpLintReportFileCmd := exec.Command("sh", "-c", fmt.Sprintf("cp --force /data/soft/jenkins_data/workspace/%s/build/lint-reports.html /data/package/%s/lint-reports.html", job.Name, job.Name))
				if output, err := cpLintReportFileCmd.CombinedOutput(); err != nil {
					c.String(http.StatusInternalServerError, fmt.Sprintf("Error lint report generated: %s", err.Error()))
					return
				} else {
					lop.I("Error executing command: ", err, string(output))
				}
			}(&currentJob)
		}
	}
}
