package androidlintreportwebhook

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"hi_golang/tools/lop"
	"io"
	"net/http"
	"os"
	"os/exec"

	"github.com/gin-gonic/gin"
)

func GenerateAndroidDepsUpdateReportWebhook(c *gin.Context) {

	url := "http://127.0.0.1:9001/api/json?pretty=true"
	username := "taohui"
	password := "jenkins"
	jenkinsDataWorkspacePath := "/data/soft/jenkins_data/workspace"

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		c.String(http.StatusInternalServerError, "Error creating request")
		return
	}

	auth := username + ":" + password
	basicAuth := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))
	req.Header.Set("Authorization", basicAuth)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		c.String(http.StatusInternalServerError, "Error sending request")
		return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		c.String(http.StatusInternalServerError, "Error reading response")
		return
	}
	var jenkinsJobsApiResults JenkinsJobsApiResults
	if err := json.Unmarshal(body, &jenkinsJobsApiResults); err != nil {
		c.String(http.StatusInternalServerError, fmt.Sprintf("Error unmarshalling response: %s", err.Error()))
		return
	}
	c.String(http.StatusOK, string(body))

	for _, job := range jenkinsJobsApiResults.Jobs {
		currentJob := job
		// if strings.HasSuffix(job.Name, "LightChat-Android") {
		// strings.HasSuffix(job.Name, "Yolo-Android") {
		go func(job *Job) {
			command := fmt.Sprintf("cd %s/%s && nohup ./gradlew dependencyUpdates -Dorg.gradle.configuration-cache=false -DoutputFormatter=html &", jenkinsDataWorkspacePath, job.Name)
			lop.I(job.Name)
			lop.I(command)
			cmd := exec.Command("sh", "-c", command)
			if output, err := cmd.CombinedOutput(); err != nil {
				c.String(http.StatusInternalServerError, fmt.Sprintf("Error lint report generated: %s", err.Error()))
				return
			} else {
				lop.I("Error executing command: ", err, string(output))
			}
			os.MkdirAll(fmt.Sprintf("/data/package/%s", job.Name),os.ModePerm)
			cpLintReportFileCmd := exec.Command("sh", "-c", fmt.Sprintf("cp --force /data/soft/jenkins_data/workspace/%s/build/dependencyUpdates/report.html /data/package/%s/dependencyUpdates.html", job.Name, job.Name))
			if output, err := cpLintReportFileCmd.CombinedOutput(); err != nil {
				c.String(http.StatusInternalServerError, fmt.Sprintf("Error lint report generated: %s", err.Error()))
				return
			} else {
				lop.I(fmt.Sprintf("https://tools.shijianline.cn:9002/download/%s/dependencyUpdates.html", job.Name))
				lop.I("Error executing command: ", err, string(output))
			}
		}(&currentJob)
		// }
	}
}
func GetAndroidDepsReportHtml(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "android_deps_report.html", nil)
}
