package sonarqube_webhook

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

func Post(ctx *gin.Context) {
	//接受sonarqube webhook数据
	// rawJsonBytes, err := io.ReadAll(ctx.Request.Body)
	var rawJson = `{
  "serverUrl": "http://localhost:9000",
  "taskId": "AZRqsKMlV3Pt65MB6XnB",
  "status": "SUCCESS",
  "analysedAt": "2025-01-15T15:57:05+0000",
  "revision": "29dcb4cfa1836c603f2d98e0c50d9012a3f8ba22",
  "changedAt": "2025-01-15T15:57:05+0000",
  "project": {
    "key": "Kiyo-Android",
    "name": "Kiyo-Android",
    "url": "http://localhost:9000/dashboard?id=Kiyo-Android"
  },
  "branch": {
    "name": "master",
    "type": "BRANCH",
    "isMain": true,
    "url": "http://localhost:9000/dashboard?id=Kiyo-Android"
  },
  "qualityGate": {
    "name": "Sonar way",
    "status": "OK",
    "conditions": [
      {
        "metric": "new_reliability_rating",
        "operator": "GREATER_THAN",
        "value": "1",
        "status": "OK",
        "errorThreshold": "1"
      },
      {
        "metric": "new_security_rating",
        "operator": "GREATER_THAN",
        "value": "1",
        "status": "OK",
        "errorThreshold": "1"
      },
      {
        "metric": "new_maintainability_rating",
        "operator": "GREATER_THAN",
        "value": "1",
        "status": "OK",
        "errorThreshold": "1"
      },
      {
        "metric": "new_coverage",
        "operator": "LESS_THAN",
        "status": "NO_VALUE",
        "errorThreshold": "80"
      },
      {
        "metric": "new_duplicated_lines_density",
        "operator": "GREATER_THAN",
        "status": "NO_VALUE",
        "errorThreshold": "3"
      },
      {
        "metric": "new_security_hotspots_reviewed",
        "operator": "LESS_THAN",
        "status": "NO_VALUE",
        "errorThreshold": "100"
      }
    ]
  },
  "properties": {
    "sonar.analysis.detectedscm": "git",
    "sonar.analysis.detectedci": "undetected"
  }
}`
	// 创建结构体实例
	var webhook SonarQubeWebhook

	// 解析 JSON 数据
	err := json.Unmarshal([]byte(rawJson), &webhook)
	if err != nil {
		fmt.Println("解析 JSON 失败:", err)
	}

	// 打印解析结果
	fmt.Printf("Server URL: %s\n", webhook.ServerURL)
	fmt.Printf("Task ID: %s\n", webhook.TaskID)
	fmt.Printf("Quality Gate Status: %s\n", webhook.QualityGate.Status)
	fmt.Printf("Project Name: %s\n", webhook.Project.Name)
	fmt.Printf("Branch Name: %s\n", webhook.Branch.Name)

	// 遍历 QualityGate 的 Conditions
	fmt.Println("Quality Gate Conditions:")
	for _, condition := range webhook.QualityGate.Conditions {
		fmt.Printf("- Metric: %s, Status: %s, Error Threshold: %s\n",
			condition.Metric, condition.Status, condition.ErrorThreshold)
	}

	// 拼接 Markdown 字符串
	var markdown strings.Builder

	// 基本信息
	markdown.WriteString("# SonarQube Webhook 解析结果\n\n")
	markdown.WriteString(fmt.Sprintf("## 基本信息\n"))
	markdown.WriteString(fmt.Sprintf("- **Server URL**: [%s](%s)\n", webhook.ServerURL, webhook.ServerURL))
	markdown.WriteString(fmt.Sprintf("- **Task ID**: `%s`\n", webhook.TaskID))
	markdown.WriteString(fmt.Sprintf("- **状态**: `%s`\n", webhook.Status))
	markdown.WriteString(fmt.Sprintf("- **分析时间**: `%s`\n", webhook.AnalysedAt))
	markdown.WriteString(fmt.Sprintf("- **修订版本**: `%s`\n", webhook.Revision))
	markdown.WriteString(fmt.Sprintf("- **变更时间**: `%s`\n\n", webhook.ChangedAt))

	// 项目信息
	markdown.WriteString(fmt.Sprintf("## 项目信息\n"))
	markdown.WriteString(fmt.Sprintf("- **项目 Key**: `%s`\n", webhook.Project.Key))
	markdown.WriteString(fmt.Sprintf("- **项目名称**: `%s`\n", webhook.Project.Name))
	markdown.WriteString(fmt.Sprintf("- **项目 URL**: [%s](%s)\n\n", webhook.Project.URL, webhook.Project.URL))

	// 分支信息
	markdown.WriteString(fmt.Sprintf("## 分支信息\n"))
	markdown.WriteString(fmt.Sprintf("- **分支名称**: `%s`\n", webhook.Branch.Name))
	markdown.WriteString(fmt.Sprintf("- **分支类型**: `%s`\n", webhook.Branch.Type))
	markdown.WriteString(fmt.Sprintf("- **是否为主分支**: `%v`\n", webhook.Branch.IsMain))
	markdown.WriteString(fmt.Sprintf("- **分支 URL**: [%s](%s)\n\n", webhook.Branch.URL, webhook.Branch.URL))

	// 质量门
	markdown.WriteString(fmt.Sprintf("## 质量门 (Quality Gate)\n"))
	markdown.WriteString(fmt.Sprintf("- **名称**: `%s`\n", webhook.QualityGate.Name))
	markdown.WriteString(fmt.Sprintf("- **状态**: `%s`\n\n", webhook.QualityGate.Status))
	markdown.WriteString("### 条件详情\n")
	for _, condition := range webhook.QualityGate.Conditions {
		markdown.WriteString(fmt.Sprintf("- **Metric**: `%s`, **Operator**: `%s`, **Value**: `%s`, **Status**: `%s`, **Error Threshold**: `%s`\n",
			condition.Metric, condition.Operator, condition.Value, condition.Status, condition.ErrorThreshold))
	}
	markdown.WriteString("\n")

	// 属性信息
	markdown.WriteString(fmt.Sprintf("## 属性 (Properties)\n"))
	markdown.WriteString(fmt.Sprintf("- **SCM 检测**: `%s`\n", webhook.Properties.DetectedSCM))
	markdown.WriteString(fmt.Sprintf("- **CI 检测**: `%s`\n", webhook.Properties.DetectedCI))

	// 输出 Markdown 字符串
	var result = markdown.String()
	fmt.Println(result)
}

// 定义结构体
type SonarQubeWebhook struct {
	ServerURL   string      `json:"serverUrl"`
	TaskID      string      `json:"taskId"`
	Status      string      `json:"status"`
	AnalysedAt  string      `json:"analysedAt"`
	Revision    string      `json:"revision"`
	ChangedAt   string      `json:"changedAt"`
	Project     Project     `json:"project"`
	Branch      Branch      `json:"branch"`
	QualityGate QualityGate `json:"qualityGate"`
	Properties  Properties  `json:"properties"`
}

type Project struct {
	Key  string `json:"key"`
	Name string `json:"name"`
	URL  string `json:"url"`
}

type Branch struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	IsMain bool   `json:"isMain"`
	URL    string `json:"url"`
}

type QualityGate struct {
	Name       string      `json:"name"`
	Status     string      `json:"status"`
	Conditions []Condition `json:"conditions"`
}

type Condition struct {
	Metric         string `json:"metric"`
	Operator       string `json:"operator"`
	Value          string `json:"value,omitempty"`
	Status         string `json:"status"`
	ErrorThreshold string `json:"errorThreshold"`
}

type Properties struct {
	DetectedSCM string `json:"sonar.analysis.detectedscm"`
	DetectedCI  string `json:"sonar.analysis.detectedci"`
}
