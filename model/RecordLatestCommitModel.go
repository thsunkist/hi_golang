package model

import "time"

type Repository struct {
	Name            string `json:"name"`
	VisibilityLevel int    `json:"visibility_level"`
	GitHTTPURL      string `json:"git_http_url"`
	GitSSHURL       string `json:"git_ssh_url"`
	URL             string `json:"url"`
	Homepage        string `json:"homepage"`
}

type Author struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type Commit struct {
	Author    Author    `json:"author"`
	ID        string    `json:"id"`
	Message   string    `json:"message"`
	URL       string    `json:"url"`
	Timestamp time.Time `json:"timestamp"`
}

type RecordLatestCommitModel struct {
	TotalCommitsCount int        `json:"total_commits_count"`
	UserEmail         string     `json:"user_email"`
	Before            string     `json:"before"`
	UserExternUID     string     `json:"user_extern_uid"`
	UserName          string     `json:"user_name"`
	CheckoutSHA       string     `json:"checkout_sha"`
	Repository        Repository `json:"repository"`
	ObjectKind        string     `json:"object_kind"`
	Ref               string     `json:"ref"`
	ProjectID         int        `json:"project_id"`
	UserID            int        `json:"user_id"`
	Commits           []Commit   `json:"commits"`
	After             string     `json:"after"`
	AliyunPK          string     `json:"aliyun_pk"`
}
