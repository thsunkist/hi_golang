package model

type OApiDingtalkRobotModel struct {
	Text       string `json:"text"`
	Title      string `json:"title"`
	PicURL     string `json:"picUrl"`
	MessageURL string `json:"messageUrl"`
}
type OApiDingtalkRobotTextModel struct {
	Content string `json:"content"`
}
type OApiDingtalkRobotMarkdownModel struct {
	Title string `json:"title"`
	Text  string `json:"text"`
}

type OApiDingtalkRobotRequestModel struct {
	Msgtype                        string                         `json:"msgtype"`
	OApiDingtalkRobotModel         OApiDingtalkRobotModel         `json:"link"`
	OApiDingtalkRobotTextModel     OApiDingtalkRobotTextModel     `json:"text"`
	OApiDingtalkRobotMarkdownModel OApiDingtalkRobotMarkdownModel `json:"markdown"`
}
