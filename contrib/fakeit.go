package contrib

// https://github.com/brianvoe/gofakeit#functions
// gofakeit.Name()             // Markus Moen
// gofakeit.Email()            // alaynawuckert@kozey.biz
// gofakeit.Phone()            // (570)245-7485
// gofakeit.BS()               // front-end
// gofakeit.BeerName()         // Duvel
// gofakeit.Color()            // MediumOrchid
// gofakeit.Company()          // Moen, Pagac and Wuckert
// gofakeit.CreditCardNumber() // 4287271570245748
// gofakeit.HackerPhrase()     // Connecting the array won't do anything, we need to generate the haptic COM driver!
// gofakeit.JobTitle()         // Director
// gofakeit.CurrencyShort()    // USD
