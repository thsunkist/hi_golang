package main

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"hi_golang/controller"
	"hi_golang/controller/android/jenkins"
	"hi_golang/controller/androidi18n"
	"hi_golang/controller/androidi18n/addlanguage"
	"hi_golang/controller/androidlintreportwebhook"
	"hi_golang/controller/androidrepo/allprojectscommitlog"
	"hi_golang/controller/androidrepo/libraryprojectandroidx"
	"hi_golang/controller/androidrepo/yuque"
	"hi_golang/controller/bugly"
	"hi_golang/controller/chatbubble"
	"hi_golang/controller/dingding2jenkins"
	"hi_golang/controller/mars"
	"hi_golang/controller/ndklib"
	"hi_golang/controller/oss"
	sonarqube_webhook "hi_golang/controller/sonarqube"
	"hi_golang/controller/test"
	"hi_golang/middleware"
	"hi_golang/service/thsunkistsrv/contrib/logger"
	"hi_golang/tools/lop"
	"io/ioutil"
	"os"
	"os/exec"

	"github.com/gin-gonic/gin"
	// "github.com/gin-contrib/gzip"
	"math/rand"
	"net/http"
	"time"
)

func main() {
	//GOPROXY='https://proxy.golang.org,direct'

	logger.Debug("hi_golang Running")
	logger.Info("hi_golang Running")
	logger.Warn("hi_golang Running")
	logger.Error("hi_golang Running")

	envVars := os.Environ()

	for _, envVar := range envVars {
		lop.D(envVar)
	}

	gin.SetMode(gin.DebugMode)

	r := gin.Default()
	r.Use(gin.Recovery())
	// r.Use(middleware.RecoveryMiddleware())
	// r.Use(middleware.Recovery2)

	newRouter := gin.New()
	newRouter.Use(middleware.JSONMiddleware())

	// 加载模板及html文件，注意这种写法下，templates根目录的模板无法加载
	r.LoadHTMLGlob("templates/html/*")
	// 加载静态文件
	r.Static("/assets", "./assets")
	r.Static("/js", "./templates/js")
	r.Static("/node_modules", "./templates/node_modules")
	//创建一个静态文件服务器
	r.StaticFS("logs/oss_file_check_result", http.Dir("./logs/oss_file_check_result"))
	r.StaticFS("logs/generatehuleandroidprojectcache", http.Dir("./logs/generatehuleandroidprojectcache"))
	// r.StaticFS("/more_static", http.Dir("my_file_system"))
	// r.StaticFile("/favicon.ico", "./resources/favicon.ico")
	/* r.Static("/images", "./images") */

	tesGroup := newRouter.Group("api/v1/test")
	{
		tesGroup.GET("/get", test.TestGetController)
		tesGroup.POST("/post", test.TestPostController)
		tesGroup.GET("/panic", func(ctx *gin.Context) {
			panic("test recovery")
		})
	}
	/*r.Any("api/*any", func(ctx *gin.Context) {
		newRouter.ServeHTTP(ctx.Writer, ctx.Request)
	})*/
	r.POST("/yude/jenkins", controller.PostJenkins1)
	r.POST("/oss/commit_oss_files_check_task", oss.CommitOssFilesCheckTask)
	r.GET("/oss/file_get", oss.GetOssFilesGet)
	r.GET("/oss/index", oss.GetIndex)

	mainRouting := r.Group("")
	{
		mainRouting.GET("", controller.GetIndex)
	}
	androidI18nRouting := r.Group("/androidi18n")
	{
		androidI18nRouting.Use(middleware.RecoveryMiddleware())
		/* 国际化翻译生产 */
		androidI18nRouting.GET("/upload", androidi18n.GetIndex)
		androidI18nRouting.POST("/parse", androidi18n.Parse)

		/* 全量添加语言 */
		androidI18nRouting.GET("/add_language", addlanguage.GetAddLanguageIndex)
		androidI18nRouting.POST("/upload", addlanguage.Upload)
		androidI18nRouting.POST("/generate", addlanguage.Generate)
	}

	marsRouting := r.Group("/mars")
	{
		marsRouting.GET("/upload", mars.GetIndex)
		marsRouting.POST("/decode", mars.Decode)
	}

	ndkRouting := r.Group("/ndklib")
	{

		ndkRouting.GET("/configure", ndklib.GetIndex)
		ndkRouting.GET("/generate", ndklib.GetIndexNdklibGenerateHtml)
		ndkRouting.POST("/compile", ndklib.Compile)
		ndkRouting.POST("/compile_from_web", ndklib.CompileFromWeb)
		ndkRouting.POST("/post_ndklib_generate", ndklib.PostNdklibGenerate)
		// ndkRouting.GET("/get_ndk_result", ndklib.GetNdkResult)
	}

	jenkinsRouting := r.Group("/jenkins")
	{
		jenkinsRouting.POST("/specially_flavor_compile", jenkins.PostSpeciallyFlavorCompile)
		jenkinsRouting.POST("/record_latest_commit", jenkins.RecordLatestCommit)
		jenkinsRouting.GET("/record_latest_commit", jenkins.RecordLatestCommit)
		jenkinsRouting.GET("/get_all_jobs", jenkins.GetAllJobs)
	}
	r.GET("/ping", func(c *gin.Context) { c.JSON(200, gin.H{"message": "pong"}) })
	androidlintreportwebhookRouting := r.Group(`/androidlintreportwebhook`)
	{
		androidlintreportwebhookRouting.GET("/get_android_deps_report", androidlintreportwebhook.GetAndroidDepsReportHtml)
		androidlintreportwebhookRouting.GET("/commit", androidlintreportwebhook.Commit)
		androidlintreportwebhookRouting.GET(`/generateandroiddepsupdatereportwebhook`, androidlintreportwebhook.GenerateAndroidDepsUpdateReportWebhook)

	}
	r.POST("/dingding2jenkins/from_dingding_robot_by_get", dingding2jenkins.FromDingDingRobotByPost)
	r.POST("/android/sonarqube/webhook", sonarqube_webhook.Post)
	buglyAnalyticsRouting := r.Group("/bugly")
	{
		buglyAnalyticsRouting.POST("/analytics", bugly.BuglyAnalytics)
		buglyAnalyticsRouting.POST("/analytics_ios", bugly.BuglyAnalyticsIOS)
	}
	r.POST("/repo/android/library_project_androidx_update2", libraryprojectandroidx.UpdateTipsController)
	r.POST("/repo/android/all_projects_commit", allprojectscommitlog.AllProjectsCommitLogController)
	r.POST("/repo/android/yuque", yuque.UpdateTipsController)
	r.POST("/repo/android/library_project_androidx_update", func(c *gin.Context) {

		var cmd = exec.Command(`curl`, `https://oapi.dingtalk.com/robot/send?access_token=319568134cc83c90ff1c5cacc27d6dec1a027060e87f46ad544a74d9e2d0b2be&pc_slide=true`,
			`-H`, `Content-Type: application/json`,
			`-d`, `{
	"msgtype": "link",
	"link": {
		"text": "仓库更新",
		"title": "⚠️library-project-androidX⚠️️️",
		"picUrl": "http://i1.img.969g.com/down/imgx2015/02/27/289_095120_f0936.jpg",
		"messageUrl": "https://codeup.aliyun.com/6289ac98487c500c27f5b92e/timeOnlines/library-project-androidX"
	}
}`)
		var stdout bytes.Buffer
		var stderr bytes.Buffer
		cmd.Stdout = &stdout
		cmd.Stderr = &stderr

		err := cmd.Run()

		if err != nil {
			fmt.Printf("Stdout: %s\n", stdout.String())
			fmt.Printf("Stderr: %s\n", stderr.String())
		} else {
			fmt.Printf("Stdout: %s\n", stdout.String())
		}
		fmt.Println(err)

		c.JSON(200, gin.H{
			"message": "update",
		})
	})

	// r.Use(gzip.Gzip(gzip.BestCompression))
	r.GET("/large-string", func(c *gin.Context) {
		// 生成100KB大小的随机字符串
		const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		b := make([]byte, 100000)
		rand.Seed(time.Now().UnixNano())
		for i := range b {
			b[i] = letterBytes[rand.Intn(len(letterBytes))]
		}
		// 设置Content-Type为text/plain，并将随机字符串写入响应体中
		c.Header("Content-Type", "text/plain")
		c.Writer.WriteHeader(http.StatusOK)
		c.Writer.Write(b)
	})
	_9patchFile := r.Group("/9patch_file")
	{
		_9patchFile.GET("/batch_generate_by_template", chatbubble.BatchGenerate9PatchFileByTemplate)
		_9patchFile.GET("/clear_borders", chatbubble.Clear9PathBorders)
	}
	// ndklib.GetNdkResult("NDK BUILD 快乐六一 翔总出击")
	// getJenkinsAllJobs()
	// r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")}

	s := &http.Server{
		Addr:           ":8080",
		Handler:        r,
		ReadTimeout:    0, // No timeout
		WriteTimeout:   0, // No timeout
		MaxHeaderBytes: 1 << 20,
	}

	s.ListenAndServe()
}

func getJenkinsAllJobs() {
	url := "http://tools.shijianline.cn:9001/api/json?tree=jobs[name]"
	username := "taohui"
	password := "jenkins"

	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println(err)
		return
	}

	req.Header.Add("Authorization", "Basic "+base64.StdEncoding.EncodeToString([]byte(username+":"+password)))

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(body))
}

