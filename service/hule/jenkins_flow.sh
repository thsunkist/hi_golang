#cd /srv/hi_golang/tools
COMMIT_MESSAGE=$(git log -1 --pretty=format:"%s")
VERSION_NAME=$(grep 'versionName' ./version.gradle | grep -o '[0-9.]\+' | grep 1)
DOWNLOAD_URL="http://192.168.20.8:9002/download/${JOB_BASE_NAME}/${BUILD_NUMBER}.apk"
QRENCODE_URL="http://183.6.55.35:9002/download/${JOB_BASE_NAME}/${BUILD_NUMBER}.png"
# 后台配置活动Jenkins LightChat Android更新通知机器人
# ACCESS_TOKEN="7f9b3e68ec89a746bb2ab398ac1ba1d7fa353ced267bb14367a9c2c72c7b1dd0"
# LC消极分子们Jenkins LightChat Android更新通知机器人
ACCESS_TOKEN="25ae6f60a61a0c10eb9db6c8f4f983bc3cf8485a7cbf54bd9e4f1adb734df5f3"
IS_AT_ALL = false

echo 当前分支：$BRANCH_NAME
echo 当前用户：${GIT_BRANCH}
echo 当前tag：${GIT_LOCAL_BRANCH}
echo jenkins工作路径： $WORKSPACE
echo "$GIT_COMMIT"
echo  "$BUILD_TAG"
echo "$refs"
echo SCM_CHANGELOG $SCM_CHANGELOG

#%3$s 为 git commit message
if [! "${BUILD_TYPE}" == "googleRelease"]; then
    IS_AT_ALL = true
    else 
    IS_AT_ALL = false
    fi

if [ ! -n "$SCM_CHANGELOG" ] ;then
    echo "没有commit记录 请注意 发送邮件通知运维"
else
    echo "SCM_CHANGELOG  如下 $SCM_CHANGELOG"
fi

mkdir -p /data/package/${JOB_BASE_NAME}/${BUILD_NUMBER}
qrencode -o /data/package/${JOB_BASE_NAME}/${BUILD_NUMBER}.png -s 2 "${DOWNLOAD_URL}"
cp --force /data/soft/jenkins_data/workspace/buildFiles/${JOB_BASE_NAME}/${BUILD_NUMBER}/${JOB_BASE_NAME}_${BUILD_NUMBER}.apk /data/package/${JOB_BASE_NAME}/${BUILD_NUMBER}.apk


#--------------------------------------------------
curl "https://oapi.dingtalk.com/robot/send?access_token=${ACCESS_TOKEN}" \
-H 'Content-Type: application/json' \
-d "{
  \"msgtype\": \"markdown\",
  \"markdown\": {
    \"title\": \"Jenkins ${JOB_BASE_NAME} ${BUILD_TYPE} update 🎉\",
    \"text\": \"
    \n[${JOB_BASE_NAME}_${BUILD_TYPE}_${VERSION_NAME}](${DOWNLOAD_URL})
    \n[🎉版本更新🎉]($DOWNLOAD_URL)
    \n![avatar](${QRENCODE_URL})
    \n${SCM_CHANGELOG}
    \"
  },
  \"at\": {
    \"atMobiles\": [
      \"15201457762\"
    ],
    \"atUserIds\": [
      \"user123\"
    ],
    \"isAtAll\": false
  }
}"
#--------------------------------------------------
# TODO TIPS # 如果是release包，直接上传或者公开bugly/Google的mapping文件
#--------------------------------------------------
# TODO TIPS: trigger common ndk build and upload
#--------------------------------------------------