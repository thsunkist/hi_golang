import { FFmpeg } from './node_modules/@ffmpeg/ffmpeg/dist/esm/index.js';
import { fetchFile } from './node_modules/@ffmpeg/util/dist/esm/index.js';
import './node_modules/svga/svga.js';

const ffmpeg = new FFmpeg();
let ffmpegLoaded = false;

async function calculate(file) {
  console.log(file)
  if (!file) {
    console.error("No file provided");
    return null;
  }

  const { name } = file;

  try {
    if (name.endsWith(".svga")) {
      return await calculateSVGA(file);
    } else if (name.endsWith(".gif")) {
      return await calculateGif(file);
    } else if (name.endsWith(".mp4") || name.endsWith(".vap")) {
      return await calculateMP4VAPMemoryUsage(file);
    } else {
      // 如果文件类型未知，默认等待所有方法完成后返回结果
      const result = await Promise.any([calculateSVGA(file), calculateGif(file), calculateMP4VAPMemoryUsage(file)]);
      return result
    }
  } catch (error) {
    console.error("Error calculating memory usage:", error);
    throw error; // 抛出错误以便外部代码能够捕获并处理
  }
}

async function calculateSVGA(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = async function (e) {
      const parser = new SVGA.Parser();
      parser.load(file, async videoItem => {
        const memoryUsage = await calculateSVGAMemoryUsage(videoItem);
        resolve(memoryUsage);
      });
    };
    reader.readAsArrayBuffer(file);
  });
}

function calculateGif(file) {
  return parseGif(file)
    .then(info => {
      const memoryUsage = calculateGifMemoryUsage(info.width, info.height, info.frameCount);
      return memoryUsage;
    })
    .catch(error => {
      console.error(`Unable to calculate GIF memory usage: ${error}`);
      throw error;
    });
}

async function calculateSVGAMemoryUsage(videoItem) {
  // const jsonSize = JSON.stringify(videoItem).length * 2;
  let totalTextureSize = 0;
  let totalSize = 0
  return new Promise(resolve => {
    Object.keys(videoItem.images).map(imageKey => {
      const image = videoItem.images[imageKey];
      const img = new Image();
      img.src = "data:image/png;base64," + image;
      img.onload = () => {
        const textureSize = img.width * img.height * 4;
        totalTextureSize += textureSize;
        if (Object.keys(videoItem.images).indexOf(imageKey) === Object.keys(videoItem.images).length - 1) {
          totalSize = totalTextureSize / 1024 / 1024;
          console.log(`SVGA file total memory usage: ${totalSize.toFixed(2)} MB`);
          resolve(totalSize.toFixed(2));
        }
      };
    });
  });
}

function calculateGifMemoryUsage(width, height, frameCount, colorDepth = 8) {
  const bytesPerPixel = colorDepth / 8;
  const bytesPerFrame = width * height * bytesPerPixel;
  const totalMemoryUsage = bytesPerFrame * frameCount;
  return (totalMemoryUsage / (1024 * 1024)).toFixed(2);
}

function parseGif(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = function (event) {
      const data = new Uint8Array(event.target.result);
      if (data[0] !== 0x47 || data[1] !== 0x49 || data[2] !== 0x46) {
        reject("Not a valid GIF file");
      }
      const width = data[6] + (data[7] << 8);
      const height = data[8] + (data[9] << 8);
      const frameCount = Array.from(data).filter(
        (byte, i) => byte === 0x21 && data[i + 1] === 0xf9 && data[i + 2] === 0x04
      ).length;
      resolve({ width, height, frameCount });
    };
    reader.onerror = reject;
    reader.readAsArrayBuffer(file);
  });
}

async function calculateMP4VAPMemoryUsage(file) {
  try {
    if (!ffmpegLoaded) {
      await ffmpeg.load({
        coreURL: "/node_modules/@ffmpeg/core/dist/esm/ffmpeg-core.js"
      });
      ffmpegLoaded = true; // 设置加载状态为已加载
    }

    await ffmpeg.writeFile(file.name, await fetchFile(file));
    const fileInfo = await ffmpeg.readFile(file.name);
    const metadata = new TextDecoder().decode(fileInfo);
    const result = await ffmpeg.exec(["-i", file.name]);
    const memoryUsed = calculateMP4VAPMemory(metadata);
    return memoryUsed;
  } catch (error) {
    console.error("Error calculating MP4/VAP memory usage:", error);
    throw error;
  }
}

function calculateMP4VAPMemory(metadata) {
  const json = parseMP4VAPMetadata(metadata);
  if (!json) {
    throw new Error("Failed to parse MP4/VAP metadata");
  }
  const memoryUsage = getMP4VAPMemoryUsage(json.info.videoW, json.info.videoH, json.info.fps);
  return memoryUsage;
}

function parseMP4VAPMetadata(metadata) {
  const startIndex = 0;
  const maxLength = Infinity;
  const targetStr = "vapc";
  const targetStrLength = targetStr.length;
  let i = 0;
  let jsonStr = "";
  const symbolCountMap = { "{": 0, "[": 0 };

  const setSymbolCount = str => {
    const map = {
      "{": () => symbolCountMap["{"]++,
      "}": () => symbolCountMap["{"]--,
      "[": () => symbolCountMap["["]++,
      "]": () => symbolCountMap["["]--
    };
    map[str]?.();
  };

  for (let index = startIndex; index < metadata.length; index++) {
    if (index > startIndex + maxLength) {
      throw new Error(`No JSON info found. Adjust configuration: { startIndex: ${startIndex}, maxLength: ${maxLength} }`);
    }
    const str = metadata[index];
    if (i === targetStrLength) {
      jsonStr += str;
      setSymbolCount(str);
      if (symbolCountMap["["] === 0 && symbolCountMap["{"] === 0) {
        try {
          return JSON.parse(jsonStr);
        } catch (error) {
          console.error("Error parsing MP4/VAP JSON info:", error);
          throw error;
        }
      }
      continue;
    }
    i = targetStr[i] === str ? i + 1 : 0;
  }
}

function getMP4VAPMemoryUsage(
  videoWidth,
  videoHeight,
  frameRate,
  audioSampleRate = 44100,
  audioBitDepth = 16,
  audioChannels = 2,
  audioBufferDuration = 5,
  extraMemory = 5
) {
  const bytesPerPixel = 3;
  const frameSize = videoWidth * videoHeight * bytesPerPixel;
  const numFramesInBuffer = 3;
  const decodeBufferSize = frameSize * numFramesInBuffer;
  const audioFrameSizePerSecond = audioSampleRate * (audioBitDepth / 8) * audioChannels;
  const audioBufferSize = audioFrameSizePerSecond * audioBufferDuration;
  const extraMemoryBytes = extraMemory * 1024 * 1024;
  const totalMemoryUsage = decodeBufferSize + audioBufferSize + extraMemoryBytes;
  return (totalMemoryUsage / (1024 * 1024)).toFixed(2);
}

window.calculate = calculate;
