package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func JSONMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Content-Type", "application/json")
		c.Next()
		if len(c.Errors) > 0 {
			return
		}
		if c.Writer.Status() == http.StatusOK {
			data, _ := c.Get("data")
			c.JSON(http.StatusOK, gin.H{
				"code": http.StatusOK,
				"msg":  "OK",
				"data": data,
			})
		} else {
			c.JSON(c.Writer.Status(), gin.H{
				"code": c.Writer.Status(),
				"msg":  http.StatusText(c.Writer.Status()),
			})
		}
	}
}

// func JSONMiddleware() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		c.Next()
// 		if len(c.Errors) > 0 {
// 			return
// 		}
// 		if c.Writer.Status() == http.StatusOK {
// 			data, exists := c.Get("data")
// 			if exists {
// 				c.JSON(http.StatusOK, gin.H{
// 					"code": http.StatusOK,
// 					"msg":  "OK",
// 					"data": data,
// 				})
// 			}
// 		} else {
// 			c.JSON(c.Writer.Status(), gin.H{
// 				"code": c.Writer.Status(),
// 				"msg":  http.StatusText(c.Writer.Status()),
// 			})
// 		}
// 	}
// }
