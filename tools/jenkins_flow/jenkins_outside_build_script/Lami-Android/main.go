package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

func main() {
	fmt.Println("generatehuleandroidprojectcache")
	androidProjectsDirPath := "/data/soft/jenkins_data/workspace"

	repositoryName := "Lami-Android"
	cmdDirPath := fmt.Sprintf("%[1]s%[2]s%[3]s", androidProjectsDirPath, string(filepath.Separator), repositoryName)

	// Add execute permission to gradlew
	chmodCmd := exec.Command("chmod", "+x", "./gradlew")
	chmodCmd.Dir = cmdDirPath
	err := chmodCmd.Run()
	if err != nil {
		log.Println("chmodCmd.Run() failed with", err)
	}
	buildType := "Release"
	cmd := exec.Command(
		"./gradlew",
		"clean",
		"assemblegoogle"+buildType,
		// "-DsystemProp.http.proxyHost=127.0.0.1",
		// "-DsystemProp.http.proxyPort=8118",
		// "-DsystemProp.https.proxyHost=127.0.0.1",
		// "-DsystemProp.https.proxyPort=8118",
		// "-Dhttp.proxyHost=127.0.0.1",
		// "-Dhttp.proxyPort=8118",
		// "-Dhttps.proxyHost=127.0.0.1",
		// "-Dhttps.proxyPort=8118",
		"-Dorg.gradle.workers.max=60",
		// `'-Dorg.gradle.jvmargs=-Xmx4g -Xms2g -Dfile.encoding=UTF-8 -XX:+UseParallelGC -XX:MaxMetaspaceSize=1g'`,
		"-Dorg.gradle.console=plain",
		"-Dorg.gradle.daemon=false",
		"-DDOKIT_METHOD_SWITCH=false",
		"--build-cache",
		fmt.Sprintf("--gradle-user-home=../%[1]s-%[2]sBuildCache", repositoryName, buildType),
		"--parallel",
		"--max-workers=60",
		// "--exclude-task uploadCrashlyticsMappingFileGoogleBeta",
		// "--exclude-task uploadCrashlyticsMappingFileGoogleRelease",
		// "--exclude-task uploadCrashlyticsMappingFileHuaweiBeta",
		// "--exclude-task uploadCrashlyticsMappingFileHuaweiRelease",
	)

	cmd.Dir = cmdDirPath //fmt.Sprintf("%[1]s%[2]s%[3]s", androidProjectsDirPath, string(filepath.Separator), task.Repository.Name)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	log.Println(cmd.Dir)
	log.Println(cmd.String())
	err = cmd.Run()
	if err != nil {
		log.Println(repositoryName, "cmd.Run() failed with", err)
	}

	// var lightChatCurlEntity = CurlEntity{
	// 	AppName:        "",
	// 	RobotAccessUrl: `https://oapi.dingtalk.com/robot/send?access_token=b9ee7acc687c7c0831de52e85d9f59a97ff7876b1892b4b26cea865c52c5717c`,
	// }
	// var textContent string = "**Cache Build Result**\n\n"
	// for _, taskResult := range taskResults {
	// 	if strings.Contains(taskResult.result, "鲜花") {
	// 		textContent += fmt.Sprintf("[<font color=common_blue1_color>%[1]s %[2]s</font>](%[3]s)\n\n",
	// 			taskResult.repositoryName,
	// 			taskResult.result,
	// 			taskResult.logUrl)
	// 	} else {
	// 		textContent += fmt.Sprintf("[<font color=common_red1_color>%[1]s %[2]s</font>](%[3]s)\n\n",
	// 			taskResult.repositoryName,
	// 			taskResult.result,
	// 			taskResult.logUrl)
	// 	}

	// }
	// var dingdingCmd = exec.Command(`curl`, lightChatCurlEntity.RobotAccessUrl,
	// 	`-H`, `Content-Type: application/json`,
	// 	`-d`, fmt.Sprintf(`{
	// 			"msgtype": "markdown",
	// 			"markdown": {
	// 				"title": "Android Build-Cache Notice",
	// 				"text": "%[1]s"
	// 			}
	// 		}`,
	// 		textContent))

	// log.Println(dingdingCmd.String())
	// var stdout bytes.Buffer
	// var stderr bytes.Buffer

	// dingdingCmd.Stdout = &stdout
	// dingdingCmd.Stderr = &stderr

	// if err := dingdingCmd.Run(); err != nil {
	// 	log.Println(err.Error())
	// }
}

/*
curl 'https://oapi.dingtalk.com/robot/send?access_token=b9ee7acc687c7c0831de52e85d9f59a97ff7876b1892b4b26cea865c52c5717c' -d '{
                                "msgtype": "markdown",
                                "markdown": {
                                        "title": "Android Build-Cache Notice",
                                        "text": "[Yolo-Android-TestBUILD SUCCESSFUL in 27s](logs/generatehuleandroidprojectcache)
"
                                }
                        }'
export http_proxy=http://127.0.0.1:8118 http_proxy=http://127.0.0.1:8118 \
export https_proxy=http://127.0.0.1:8118 https_proxy=http://127.0.0.1:8118 \
./gradlew -DsystemProp.http.proxyHost=127.0.0.1 -DsystemProp.http.proxyPort=8118 -DsystemProp.https.proxyHost=127.0.0.1 -DsystemProp.https.proxyPort=8118 -Dhttp.proxyHost=127.0.0.1 -Dhttp.proxyPort=8118 -Dhttps.proxyHost=127.0.0.1 -Dhttps.proxyPort=8118  -DPRODUCT_FLAVORS=google -Dandroid.enableR8.fullMode=false -DBUILD_TYPE=googleRelease -PPRODUCT_FLAVORS=google -PBUILD_TYPE=googleRelease -Dorg.gradle.workers.max=120 '-Dorg.gradle.jvmargs=-Xmx16g -Xms16g -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8 -XX:+UseParallelGC -XX:MaxMetaspaceSize=2g' -Dorg.gradle.console=plain -Dorg.gradle.daemon=false -DDOKIT_METHOD_SWITCH=false assemblegoogleRelease bundlegoogleRelease --build-cache --gradle-user-home=../Yolo-Android-Test-Android-ReleaseBuildCache --profile --parallel --max-workers=120
*/
