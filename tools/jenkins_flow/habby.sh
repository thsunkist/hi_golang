echo "————————————————————————————————————————————————————————————————————————————————————————————————————"
FLAVOR="google"
if [ -f "project.properties" ]; then
    FLAVOR=$(grep -oP 'flavor=\K[^ ]+' project.properties)
    # 在这里执行其他操作
else
    echo "project.properties 文件不存在"
fi
BUILD_TYPE=""
BUILD_ENV=""
if [[ $GIT_BRANCH == "origin/master" || $GIT_BRANCH == "origin/release" ]]; then
	BUILD_ENV="正式包"
	BUILD_TYPE="Release"
elif [[ $GIT_BRANCH == "origin/develop" || $GIT_BRANCH == "origin/dev" ]]; then
	BUILD_ENV="测试包"
	BUILD_TYPE="Debug"
elif [[ $GIT_BRANCH =~ ^origin/feature_.*$ ]]; then
	BUILD_ENV="测试包"
	BUILD_TYPE="Debug"
else
    echo "Unknown branch"
    exit 0
fi

echo "trigger common ndk build and upload--------------------------------------------------"
#!/bin/bash
while IFS='=' read -r key value || [[ -n "$key" ]]; do
  if [[ $key != "#"* && ! -z $key ]]; then
    if [[ $key == "signature_code" ]]; then
      signature_code=$value
    elif [[ $key == "signature_code_local" ]]; then
      signature_code_local=$value
    elif [[ $key == "des_encrypt_key" ]]; then
      des_encrypt_key=$value
    elif [[ $key == "aes_encrypt_key" ]]; then
      aes_encrypt_key=$value
    elif [[ $key == "aes_encrypt_iv" ]]; then
      aes_encrypt_iv=$value
    elif [[ $key == "ndk_package_name" ]]; then
      ndk_package_name=$value
    elif [[ $key == "group_id" ]]; then
      group_id=$value
    elif [[ $key == "des_key" ]]; then
      des_key=$value
    elif [[ $key == "des_key_halo_local" ]]; then
      des_key_halo_local=$value
    elif [[ $key == "app_version" ]]; then
      app_version=$value
    elif [[ $key == "flavor" ]]; then
      flavor=$value
    elif [[ $key == "bugly_app_id" ]]; then
      bugly_app_id=$value
    elif [[ $key == "bugly_app_key" ]]; then
      bugly_app_key=$value
    elif [[ $key == "bugly_bundleid" ]]; then
      bugly_bundleid=$value
    elif [[ $key == "netease_key" ]]; then
      netease_key=$value
    elif [[ $key == "agora_key" ]]; then
      agora_key=$value
    fi
  fi
done <"project.properties"

url="http://tools.shijianline.cn:9003/repository/juxiao_android_snapshot/"
name="taohui"
password="4hjBvKn8sV2D6g"

cd ../${JOB_BASE_NAME}-common-android-ndk
./gradlew clean
./gradlew ndkdevlib_common:actionBuildAndUploadLib \
-Purl="$url" \
-Pname="$name" \
-Ppassword="$password" \
-Psignature_code=$signature_code \
-Psignature_code_local=$signature_code_local \
-Pdes_encrypt_key=$des_encrypt_key \
-Paes_encrypt_key=$aes_encrypt_key \
-Paes_encrypt_iv=$aes_encrypt_iv \
-Pndk_package_name=$ndk_package_name \
-Pgroup_id=$group_id \
-Pdes_key=$des_key \
-Pdes_key_halo_local=$des_key_halo_local \
-Pnetease_key=$netease_key \
-Pagora_key=$agora_key \
-Papp_version=$app_version \
-Pflavor=$flavor

maven_group_id=$(echo "${group_id}" | awk -F'.' '{print $2}')
# meta_metadata=$(curl -u zzd:4hjBvKn8sV2D6g -s http://tools.shijianline.cn:9003/repository/juxiao_android_snapshot/com/${maven_group_id}/ndklib/maven-metadata.xml)
# 提取<latest>元素的值
latest_version=${app_version}
#$(echo "$meta_metadata" | grep -oP '<latest>\K[^<]*' | sed 's/^[ \t]*//')
echo "Latest version: $latest_version"

ndklib_metadata=$(curl -u zzd:4hjBvKn8sV2D6g -s http://tools.shijianline.cn:9003/repository/juxiao_android_snapshot/com/${maven_group_id}/ndklib/${latest_version}-SNAPSHOT/maven-metadata.xml)
echo "ndklib_metadata: $ndklib_metadata"

# 提取groupId
groupId=$(echo "$ndklib_metadata" | xmlstarlet sel -t -v "//groupId")

# 提取artifactId
artifactId=$(echo "$ndklib_metadata" | xmlstarlet sel -t -v "//artifactId")

# 提取version
version=$(echo "$ndklib_metadata" | xmlstarlet sel -t -v "//version")

# 提取timestamp
timestamp=$(echo "$ndklib_metadata" | xmlstarlet sel -t -v "//timestamp")

# 提取buildNumber
buildNumber=$(echo "$ndklib_metadata" | xmlstarlet sel -t -v "//buildNumber")

echo "groupId: $groupId"
echo "artifactId: $artifactId"
echo "version: $latest_version"
echo "timestamp: $timestamp"
echo "buildNumber: $buildNumber"
#implementation("com.yolo:ndklib:1.0.0.0-20240312.065936-32")
#groupId: com.yolo
#artifactId: ndklib
#version: 1.0.0.0-SNAPSHOT
#timestamp: 20240312.065936
#buildNumber: 32

NDKLIB_NAME="${groupId}:${artifactId}"
NDKLIB_BUILD=$(echo "${version}-${timestamp}-${buildNumber}" | sed 's/-SNAPSHOT//')
echo "————————————————————————————————————————————————————————————————————————————————————————————————————"
cd ../$JOB_BASE_NAME
./gradlew -DPRODUCT_FLAVORS=${FLAVOR} \
-DGRADLE_PROPERTIES_PATH=../jenkins_gradle.properties \
-Dhttp.proxyHost=127.0.0.1 -Dhttp.proxyPort=8118 -Dhttps.proxyHost=127.0.0.1 -Dhttps.proxyPort=8118 \
-DBUILD_TYPE=${FLAVOR}${BUILD_TYPE} \
-PIS_JENKINS=true \
-PJENKINS_APK_PATH=../../../../../../../buildFiles/${JOB_BASE_NAME}/${BUILD_NUMBER}/${JOB_BASE_NAME}_${BUILD_NUMBER}.apk \
-PPRODUCT_FLAVORS=${FLAVOR} \
-PGRADLE_PROPERTIES_PATH=../jenkins_gradle.properties \
-PBUILD_TYPE=${FLAVOR}${BUILD_TYPE} \
-Porg.gradle.workers.max=32 -Porg.gradle.jvmargs="-Xmx8g -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8" \
assemble${FLAVOR}${BUILD_TYPE} bundle${FLAVOR}${BUILD_TYPE} \
--profile \
--parallel
echo "————————————————————————————————————————————————————————————————————————————————————————————————————"
ACCESS_TOKEN="c0a310ff00325f49b9bedd170ea12ee49882d9ced411ba4107d6a7dbb9cc5902"
VERSION_NAME=$(grep 'versionName' ./version.gradle | grep -o '[0-9.]\+' | grep 1)

BUILD_REPORT_HTML=$(ls build/reports/profile -t | grep 'html' | head -n 1)
AAB_FOLDER_PATH="erban_client/build/outputs/bundle/${FLAVOR}${BUILD_TYPE}"
AAB_FILENAME=$(ls ${AAB_FOLDER_PATH} | grep '.aab' | head -n 1)
AAB_FILEPATH="${AAB_FOLDER_PATH}/${AAB_FILENAME}"

DOWNLOAD_URL="http://tools.shijianline.cn:9002/download/${JOB_BASE_NAME}/${BUILD_NUMBER}.apk"
DOWNLOAD_AAB_URL="http://tools.shijianline.cn:9002/download/${JOB_BASE_NAME}/${BUILD_NUMBER}.aab"
QRENCODE_URL="http://tools.shijianline.cn:9002/download/${JOB_BASE_NAME}/${BUILD_NUMBER}.png"
BUILD_REPORT_URL="http://tools.shijianline.cn:9002/download/${JOB_BASE_NAME}/${BUILD_NUMBER}/profile/$BUILD_REPORT_HTML"

echo 当前分支：$BRANCH_NAME
echo 当前用户：${GIT_BRANCH}
echo 当前tag：${GIT_LOCAL_BRANCH}
echo jenkins工作路径： $WORKSPACE
echo "$GIT_COMMIT"
echo  "$BUILD_TAG"
echo "$refs"
echo SCM_CHANGELOG $SCM_CHANGELOG

#%3$s 为 git commit message
mkdir -p /data/package/${JOB_BASE_NAME}/${BUILD_NUMBER}
qrencode -o /data/package/${JOB_BASE_NAME}/${BUILD_NUMBER}.png -s 2 "${DOWNLOAD_URL}"
cp --force /data/soft/jenkins_data/workspace/buildFiles/${JOB_BASE_NAME}/${BUILD_NUMBER}/${JOB_BASE_NAME}_${BUILD_NUMBER}.apk /data/package/${JOB_BASE_NAME}/${BUILD_NUMBER}.apk
cp -R --force /data/soft/jenkins_data/workspace/${JOB_BASE_NAME}/build/reports/profile /data/package/${JOB_BASE_NAME}/${BUILD_NUMBER}
cp --force $AAB_FILEPATH /data/package/${JOB_BASE_NAME}/${BUILD_NUMBER}.aab

if [ ! -n "$SCM_CHANGELOG" ] ;then
    echo "没有commit记录 请注意 发送邮件通知运维"
else
    echo "SCM_CHANGELOG  如下 $SCM_CHANGELOG"
fi


#--------------------------------------------------
curl "https://oapi.dingtalk.com/robot/send?access_token=${ACCESS_TOKEN}" \
-H 'Content-Type: application/json' \
-d "{
  \"msgtype\": \"markdown\",
  \"markdown\": {
    \"title\": \"Jenkins ${JOB_BASE_NAME} ${FLAVOR} ${BUILD_ENV} update 🎉\",
    \"text\": \"
    \n[${JOB_BASE_NAME}_${FLAVOR}_${BUILD_ENV}_${VERSION_NAME}](${DOWNLOAD_URL})
    \n[⏬点击下载APK]($DOWNLOAD_URL) [⏬点击下载AAB]($DOWNLOAD_AAB_URL) [📄构建报告]($BUILD_REPORT_URL)
    \nndklib name: ${NDKLIB_NAME}
    \nndklib build: ${NDKLIB_BUILD}
    \n![avatar](${QRENCODE_URL})
    \n${SCM_CHANGELOG}
    \"
  },
  \"at\": {
    \"atMobiles\": [
      \"*** **** ****\"
    ],
    \"atUserIds\": [
      \"user123\"
    ],
    \"isAtAll\": false
  }
}"
#--------------------------------------------------
# 如果是release包，直接上传或者公开bugly/Google的mapping文件
mapping_filepath="./erban_client/build/outputs/mapping/${FLAVOR}${BUILD_TYPE}/mapping.txt"
if [ -z "$bugly_app_id" ] || [ -z "$bugly_app_key" ] || [ -z "$bugly_bundleid" ] || [ $GIT_BRANCH != "origin/release" ] || [ ! -f ${mapping_filepath} ]; then
    echo "Bugly configuration is incomplete or it is not a release package, so the mapping file is not uploaded."
else
    echo "Bugly configuration is complete and it is a release package, so the mapping file is uploaded."
    JAVA_HOME=/usr/local/jdk1.8.0_161 java -jar /opt/buglyqq-upload-symbol.jar \
    -appid $bugly_app_id \
    -appkey $bugly_app_key \
    -bundleid $bugly_bundleid \
    -version $version \
    -platform Android \
    -inputSymbol ../${JOB_BASE_NAME}-common-android-ndk/ndkdevlib_common/build/intermediates/merged_jni_libs/release/out \
    -inputMapping ${mapping_filepath}
fi
#--------------------------------------------------
# TODO TIPS: trigger common ndk build and upload
#--------------------------------------------------
# TODO TIPS: app/build/reports/lint-results.html
#--------------------------------------------------

