package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

type Repository struct {
	Name            string `json:"name"`
	VisibilityLevel int    `json:"visibility_level"`
	GitHTTPURL      string `json:"git_http_url"`
	GitSSHURL       string `json:"git_ssh_url"`
	URL             string `json:"url"`
	Homepage        string `json:"homepage"`
}

type Author struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

type Commit struct {
	Author    Author    `json:"author"`
	ID        string    `json:"id"`
	Message   string    `json:"message"`
	URL       string    `json:"url"`
	Timestamp time.Time `json:"timestamp"`
}

type CurlEntity struct {
	Rate           string
	AppName        string
	AppIcon        string
	RobotAccessUrl string
}

type RecordLatestCommitModel struct {
	TotalCommitsCount int        `json:"total_commits_count"`
	UserEmail         string     `json:"user_email"`
	Before            string     `json:"before"`
	UserExternUID     string     `json:"user_extern_uid"`
	UserName          string     `json:"user_name"`
	CheckoutSHA       string     `json:"checkout_sha"`
	Repository        Repository `json:"repository"`
	ObjectKind        string     `json:"object_kind"`
	Ref               string     `json:"ref"`
	ProjectID         int        `json:"project_id"`
	UserID            int        `json:"user_id"`
	Commits           []Commit   `json:"commits"`
	After             string     `json:"after"`
	AliyunPK          string     `json:"aliyun_pk"`
}
type Payload struct {
	Ref string `json:"ref"`
}
type TaskResult struct {
	repositoryName string
	result         string
	logUrl         string
}

func main() {
	// jenkinsWorkspace
	var tag = "generatehuleandroidprojectcache"
	fmt.Println(tag)
	// androidProjectsDirPath := "/Users/mac/DevStation/hi_android"
	androidProjectsDirPath := "/data/soft/jenkins_data/workspace"
	// hiGolangProjectsDirPath := "/Users/mac/DevStation/hi_golang/hi_golang"
	hiGolangProjectsDirPath := "/srv/hi_golang"
	compileLogDirPath := fmt.Sprintf("%[1]s%[2]s%[3]s",
		"logs",
		string(filepath.Separator),
		"generatehuleandroidprojectcache")

	compileLogFullDirPath := fmt.Sprintf("%[1]s%[2]s%[3]s",
		hiGolangProjectsDirPath,
		string(filepath.Separator),
		compileLogDirPath,
	)

	if err := os.MkdirAll(compileLogFullDirPath, os.ModePerm); err != nil {
		fmt.Println(tag, err.Error())
	}

	dirPath := fmt.Sprintf("%[1]s/%[2]s/%[3]s", hiGolangProjectsDirPath, "logs", "record_latest_commit")
	fmt.Println(tag, dirPath)
	var tasks []RecordLatestCommitModel
	err := filepath.Walk(dirPath, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Println(tag, "Error retrieving info for file %s: %v", path, err)
			return err
		}
		if !info.IsDir() {
			data, err := os.ReadFile(path)
			if err != nil {
				fmt.Println(tag, "Unable to read file: %v", err)
				return err
			}
			// fmt.Printf("Content of %s:\n%s\n", path, string(data))
			var recordLatestCommitModel RecordLatestCommitModel

			if err := json.Unmarshal(data, &recordLatestCommitModel); err != nil {
				fmt.Println(tag, err.Error())
			}
			tasks = append(tasks, recordLatestCommitModel)
		}
		return nil
	})

	if err != nil {
		fmt.Println(tag, "Error walking the directory: %v", err)
		return
	}
	var wg sync.WaitGroup
	var taskResults []TaskResult

	for _, v := range tasks {

		task := v
		wg.Add(1)

		go func(task RecordLatestCommitModel) {

			_, err := json.Marshal(task)
			buildType := "Debug" //Debug or Release
			if err != nil {
				fmt.Println(tag, err)
			}
			// lop.Println(string(jsonData))
			if err != nil {
				fmt.Println(tag, err)
			}

			logFilename := task.Repository.Name + "-" + buildType + ".log"
			cmdLogFullFilepath := fmt.Sprintf("%s%s%s", compileLogFullDirPath, string(filepath.Separator), logFilename)
			cmdDirPath := fmt.Sprintf("%[1]s%[2]s%[3]s", androidProjectsDirPath, string(filepath.Separator), task.Repository.Name)

			os.Remove(cmdLogFullFilepath)
			cmdLogFile, err := os.Create(cmdLogFullFilepath)
			if err != nil {
				fmt.Println(tag, "Failed to create file: ", err)
			} else {
				defer cmdLogFile.Close()
			}
			// Add execute permission to gradlew
			chmodCmd := exec.Command("chmod", "+x", "./gradlew")
			chmodCmd.Dir = cmdDirPath //fmt.Sprintf("%[1]s%[2]s%[3]s", androidProjectsDirPath, string(filepath.Separator), task.Repository.Name)
			err = chmodCmd.Run()
			if err != nil {
				fmt.Println(tag, "chmodCmd.Run() failed with", err)
			}
			cmd := exec.Command(
				"./gradlew",
				"clean",
				"assemblegoogle"+buildType,
				// "-DsystemProp.http.proxyHost=127.0.0.1",
				// "-DsystemProp.http.proxyPort=8118",
				// "-DsystemProp.https.proxyHost=127.0.0.1",
				// "-DsystemProp.https.proxyPort=8118",
				// "-Dhttp.proxyHost=127.0.0.1",
				// "-Dhttp.proxyPort=8118",
				// "-Dhttps.proxyHost=127.0.0.1",
				// "-Dhttps.proxyPort=8118",
				"-Dorg.gradle.workers.max=30",
				"-Dorg.gradle.jvmargs=-Xmx8g -Xms6g -Dfile.encoding=UTF-8 -XX:+UseParallelGC -XX:MaxMetaspaceSize=2g",
				"-Dorg.gradle.console=plain",
				"-Dorg.gradle.daemon.idletimeout=43200000",
				"-Dorg.gradle.daemon=false",
				"-DDOKIT_METHOD_SWITCH=false",
				"--build-cache",
				fmt.Sprintf("--gradle-user-home=../%[1]s-%[2]sBuildCache", task.Repository.Name, buildType),
				"--parallel",
				"--no-daemon",
				"--max-workers=30",
				// "--exclude-task uploadCrashlyticsMappingFileGoogleBeta",
				// "--exclude-task uploadCrashlyticsMappingFileGoogleRelease",
				// "--exclude-task uploadCrashlyticsMappingFileHuaweiBeta",
				// "--exclude-task uploadCrashlyticsMappingFileHuaweiRelease",
			)

			cmd.Dir = cmdDirPath //fmt.Sprintf("%[1]s%[2]s%[3]s", androidProjectsDirPath, string(filepath.Separator), task.Repository.Name)
			cmd.Stderr = cmdLogFile
			cmd.Stdout = cmdLogFile
			fmt.Println(tag, cmd.Dir)
			fmt.Println(tag, cmd.String())
			err = cmd.Run()
			if err != nil {
				fmt.Println(tag, task.Repository.Name, "cmd.Run() failed with", err)
			}

			cmdLogFileContentBytes, err := os.ReadFile(cmdLogFullFilepath)
			if err != nil {
				fmt.Println(tag, task.Repository.Name, err.Error())
			}
			cmdLogFileContent := string(cmdLogFileContentBytes)

			// if err == nil && strings.Contains(cmdLogFileContent, "BUILD SUCCESSFUL in") {
			// lop.Println("cmdLogFileContent", cmdLogFileContent)
			// lop.Println(task.Repository.Name, "BUILD SUCCESSFUL!!!")
			// } else {
			// lop.Println(task.Repository.Name, "BUILD FAILED!!!", cmdLogFileContent, err.Error())
			// }

			// cmdLogFile.Seek(0, 0)
			if strings.Contains(cmdLogFileContent, "BUILD SUCCESSFUL") {
				taskResults = append(taskResults, TaskResult{
					repositoryName: task.Repository.Name,
					result:         buildType + " [鲜花]",
					logUrl: fmt.Sprintf("https://tools.shijianline.cn:9004/%[1]s/%[2]s",
						compileLogDirPath,
						logFilename)},
				)
			} else {
				taskResults = append(taskResults, TaskResult{
					repositoryName: task.Repository.Name,
					result:         buildType + " [残花]",
					logUrl: fmt.Sprintf("https://tools.shijianline.cn:9004/%[1]s/%[2]s",
						compileLogDirPath,
						logFilename)},
				)
			}
			wg.Done()
		}(task)
	}

	wg.Wait()

	var lightChatCurlEntity = CurlEntity{
		AppName:        "",
		RobotAccessUrl: `https://oapi.dingtalk.com/robot/send?access_token=b9ee7acc687c7c0831de52e85d9f59a97ff7876b1892b4b26cea865c52c5717c`,
	}
	var textContent string = "**Cache Build Result**\n\n"
	for _, taskResult := range taskResults {
		if strings.Contains(taskResult.result, "鲜花") {
			textContent += fmt.Sprintf("[<font color=common_blue1_color>%[1]s %[2]s</font>](%[3]s)\n\n",
				taskResult.repositoryName,
				taskResult.result,
				taskResult.logUrl)
		} else {
			textContent += fmt.Sprintf("[<font color=common_red1_color>%[1]s %[2]s</font>](%[3]s)\n\n",
				taskResult.repositoryName,
				taskResult.result,
				taskResult.logUrl)
		}

	}
	var dingdingCmd = exec.Command(`curl`, lightChatCurlEntity.RobotAccessUrl,
		`-H`, `Content-Type: application/json`,
		`-d`, fmt.Sprintf(`{
				"msgtype": "markdown",
				"markdown": {
					"title": "Android Build-Cache Notice",
					"text": "%[1]s"
				}
			}`,
			textContent))

	fmt.Println(tag, dingdingCmd.String())
	var stdout bytes.Buffer
	var stderr bytes.Buffer

	dingdingCmd.Stdout = &stdout
	dingdingCmd.Stderr = &stderr

	if err := dingdingCmd.Run(); err != nil {
		fmt.Println(tag, err.Error())
	}
}

/*
curl 'https://oapi.dingtalk.com/robot/send?access_token=b9ee7acc687c7c0831de52e85d9f59a97ff7876b1892b4b26cea865c52c5717c' -d '{
                                "msgtype": "markdown",
                                "markdown": {
                                        "title": "Android Build-Cache Notice",
                                        "text": "[Yolo-Android-TestBUILD SUCCESSFUL in 27s](logs/generatehuleandroidprojectcache)
"
                                }
                        }'
export http_proxy=http://127.0.0.1:8118 http_proxy=http://127.0.0.1:8118 \
export https_proxy=http://127.0.0.1:8118 https_proxy=http://127.0.0.1:8118 \
./gradlew -DsystemProp.http.proxyHost=127.0.0.1 -DsystemProp.http.proxyPort=8118 -DsystemProp.https.proxyHost=127.0.0.1 -DsystemProp.https.proxyPort=8118 -Dhttp.proxyHost=127.0.0.1 -Dhttp.proxyPort=8118 -Dhttps.proxyHost=127.0.0.1 -Dhttps.proxyPort=8118  -DPRODUCT_FLAVORS=google -Dandroid.enableR8.fullMode=false -DBUILD_TYPE=googleRelease -PPRODUCT_FLAVORS=google -PBUILD_TYPE=googleRelease -Dorg.gradle.workers.max=120 '-Dorg.gradle.jvmargs=-Xmx16g -Xms16g -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8 -XX:+UseParallelGC -XX:MaxMetaspaceSize=2g' -Dorg.gradle.console=plain -Dorg.gradle.daemon=false -DDOKIT_METHOD_SWITCH=false assemblegoogleRelease bundlegoogleRelease --build-cache --gradle-user-home=../Yolo-Android-Test-Android-ReleaseBuildCache --profile --parallel --max-workers=120
*/
