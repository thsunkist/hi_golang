package tools

import (
	"fmt"
	"path/filepath"
	"runtime"
	"time"
)

func GetGenPath(directorFolder string) string {
	_, b, _, _ := runtime.Caller(0)
	return filepath.Join(filepath.Dir(b), "../../gen/"+directorFolder)
}

func GenFileName(filename string) string {
	return fmt.Sprintf("%s.%d", filename, time.Now().UnixNano()/int64(time.Microsecond))
}
func GenTimeNowInUnixNano(filename string) string {
	return fmt.Sprintf("%s.%d", filename, time.Now().UnixNano()/int64(time.Microsecond))
}
