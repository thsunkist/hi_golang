package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
)

func main() {
	targetBranch := "dev"
	sourceBranch := "develop"

	currentExecutablePath, err := os.Executable()
	if err != nil {
		panic(err)
	}
	//git config --global url."git@github.com:".insteadOf "https://github.com/"
	currentExecutablePathDir := filepath.Dir(currentExecutablePath)
	fmt.Println("当前运行程序所在的文件夹路径：", currentExecutablePathDir)

	// 检查本地是否存在目标分支
	if !isBranchExist(targetBranch, currentExecutablePathDir) {
		checkoutBranch(targetBranch, currentExecutablePathDir)
	} else {
		switchBranch(targetBranch, currentExecutablePathDir)
	}

	// 合并源分支到目标分支
	mergeAndPush(sourceBranch, targetBranch, currentExecutablePathDir)
	// 切换回原来的分支
	switchBranch(sourceBranch, currentExecutablePathDir)
}

// 检查本地是否存在指定分支
func isBranchExist(branchName, currentExecutablePathDir string) bool {
	cmd := exec.Command("git", "show-ref", "--verify", "--quiet", "refs/heads/"+branchName)
	cmd.Dir = currentExecutablePathDir
	err := cmd.Run()
	return err == nil
}

// 检出指定分支
func checkoutBranch(branchName, currentExecutablePathDir string) {
	cmd := exec.Command("git", "checkout", "-b", branchName)
	cmd.Dir = currentExecutablePathDir
	err := cmd.Run()
	if err != nil {
		fmt.Printf("检出分支 %s 失败: %v\n", branchName, err)
	} else {
		fmt.Printf("成功检出分支 %s\n", branchName)
	}
}

// 切换到指定分支
func switchBranch(branchName, currentExecutablePathDir string) {
	cmd := exec.Command("git", "checkout", branchName)
	cmd.Dir = currentExecutablePathDir
	err := cmd.Run()
	if err != nil {
		fmt.Printf("切换到分支 %s 失败: %v\n", branchName, err)
	} else {
		fmt.Printf("成功切换到分支 %s\n", branchName)
	}
}

// 合并源分支到目标分支并推送
func mergeAndPush(sourceBranch, targetBranch, currentExecutablePathDir string) {
	// 切换到目标分支
	cmd := exec.Command("git", "checkout", targetBranch)
	cmd.Dir = currentExecutablePathDir
	err := cmd.Run()
	if err != nil {
		fmt.Printf("切换到分支 %s 失败: %v\n", targetBranch, err)
		return
	}

	// 合并源分支到目标分支
	cmd = exec.Command("git", "merge", sourceBranch)
	cmd.Dir = currentExecutablePathDir
	err = cmd.Run()
	if err != nil {
		fmt.Printf("合并分支 %s 到 %s 失败: %v\n", sourceBranch, targetBranch, err)
		return
	}

	// 推送到远程仓库
	cmd = exec.Command("git", "push", "origin", targetBranch, "--force")
	cmd.Dir = currentExecutablePathDir
	err = cmd.Run()
	if err != nil {
		fmt.Printf("推送到远程仓库失败: %v\n", err)
		return
	}

	fmt.Printf("成功合并并推送 %s 分支到 %s 分支\n", sourceBranch, targetBranch)
}

// 获取当前分支名称
func getCurrentBranch() string {
	cmd := exec.Command("git", "rev-parse", "--abbrev-ref", "HEAD")
	out, err := cmd.Output()
	if err != nil {
		fmt.Println("获取当前分支失败:", err)
		return ""
	}
	return string(out)
}
