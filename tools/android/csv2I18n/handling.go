package csv2I18n

import (
	"bufio"
	"encoding/csv"
	"errors"
	"fmt"
	"hi_golang/tools/lop"
	"io"
	"log"
	"os"
	"regexp"
	"strings"
)

func Csv2AndroidI18n(outputFolder, csvFilePath, version string, countryAbbrArray []string) error {
	// if err := ReadCSVFile(csvFilePath); err != nil {
	// 	return err
	// }
	csvFile, _ := os.Open(csvFilePath)
	reader := csv.NewReader(bufio.NewReader(csvFile))
	reader.LazyQuotes = true
	reader.TrimLeadingSpace = true
	reader.FieldsPerRecord = -1
	androidI18nFiles := make(map[string][]string, len(countryAbbrArray))
	for _, v := range countryAbbrArray {
		androidI18nFiles[v] = []string{}
	}

	firstBool := true

	for {
		line, error := reader.Read()

		// reader.FieldPos(0)
		// lop.E(line)
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
			continue
		}
		if firstBool == true {
			firstBool = false
			continue
		}
		// len(lop.I(line)) [I]  2023-10-18 17:00:01 999 handling.go:44 2 3ar63 3
		//[I]  2023-10-18 17:05:49 999 handling.go:46 1line len: 3, v: enlen(androidI18nFiles[v]): 63 3Expiry date: %s days left

		for index, v := range countryAbbrArray {
			// lop.D(len(line), line, error)

			// [I]  2023-10-18 17:40:18 999 handling.go:54 3line len: 3, v: pt, len(androidI18nFiles[v]): 16 7
			lop.I(index,
				"line len: ", len(line),
				", v: ", v,
				", len(androidI18nFiles[v]): ", len(androidI18nFiles[v]),
				len(countryAbbrArray),
			)
			if index >= len(line) {
				androidI18nFiles[v] = append(androidI18nFiles[v], "")
			} else {
				androidI18nFiles[v] = append(androidI18nFiles[v], line[index])
			}
		}
	}

	fmt.Println(androidI18nFiles)
	re := regexp.MustCompile("\\w{2,}")
	lop.E("running")

	/* initialize keys */
	var keys []string
	var keyDuplicateMap = make(map[string]uint32)
	lop.E("running")

	for _, v := range androidI18nFiles["en"] {

		upperString := strings.ToLower(v)
		upperString = strings.ReplaceAll(upperString, "’", "")
		upperString = strings.ReplaceAll(upperString, "'", "")
		upperString = strings.ReplaceAll(upperString, " ", "_")
		upperString = re.FindString(upperString)
		upperString = "s_" + version + "_" + upperString
		/* 如果是纯数字 */

		if duplicateID, isExist := keyDuplicateMap[upperString]; isExist {
			keyDuplicateMap[upperString] = duplicateID + 1
			keys = append(keys, "empty_"+fmt.Sprint(upperString, keyDuplicateMap[upperString]))
		} else {
			keyDuplicateMap[upperString] = 0
			keys = append(keys, upperString)
		}
	}
	lop.E("running")

	/** 国家的简写 to 文件名 */
	for countryShortName, androidI18nFileValues := range androidI18nFiles {
		mkDirAllResultErr := os.MkdirAll(outputFolder+"/values-"+countryShortName, 0777)
		if mkDirAllResultErr != nil {
			log.Fatal(mkDirAllResultErr)
		}
		stringsPwd := outputFolder + "/values-" + countryShortName + "/strings" + version + ".xml"
		androidI18nFile, err := os.OpenFile(stringsPwd, os.O_CREATE|os.O_RDWR, 0777)
		if err != nil {
			log.Fatal(err)
		}

		/** 生成xml文件头部声明 */
		if _, err := androidI18nFile.WriteString("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<resources>\n"); err != nil {
			log.Fatal(err)
		}

		for k, content := range keys {
			if len(content) == 0 {
				continue
			}

			var androidI18nFileValue = androidI18nFileValues[k]
			androidI18nFileValue = strings.ReplaceAll(androidI18nFileValue, "\n", `\n`)
			// androidI18nFileValue = strings.ReplaceAll(androidI18nFileValue, `"`, `\"`)
			androidI18nFileValue = strings.ReplaceAll(androidI18nFileValue, `>`, `&gt;`)
			// < (less-than) &lt;
			androidI18nFileValue = strings.ReplaceAll(androidI18nFileValue, `<`, `&lt;`)
			androidI18nFileValue = strings.ReplaceAll(androidI18nFileValue, `&`, `&amp;`)
			androidI18nFileValue = strings.ReplaceAll(androidI18nFileValue, `'`, `\'`)
			androidI18nFileValue = strings.ReplaceAll(androidI18nFileValue, `"`, `&quot;`)

			itemString := fmt.Sprint(`<string name="`, content, `">`, androidI18nFileValue, `</string>`, "\n")
			//itemString = strings.ReplaceAll(itemString, `'`, `\'`)

			/** 为了处理一些特殊语言在输入的时候会自动顺序颠倒的问题 */
			if (strings.Compare("ar", countryShortName) == 0 || strings.Compare("ur", countryShortName) == 0) && strings.Contains(itemString, "s%") {
				itemString = strings.ReplaceAll(itemString, "s%", "%s")
			}
			itemString = strings.ReplaceAll(itemString, "%S", "%s")

			var count = strings.Count(itemString, "%s")
			for i := 0; i < count; i++ {
				itemString = strings.Replace(itemString, "%s", fmt.Sprintf("%%%d$s", i+1), 1)
			}

			/** 处理百分号，%符号影响格式化的操作 */
			var percentRegex = regexp.MustCompile("\\d+(?:\\.\\d+)?%")
			var percentRegexResults = percentRegex.FindAllString(itemString, len(itemString))

			if len(percentRegexResults) > 0 {
				for i := 0; i < len(percentRegexResults); i++ {
					var percentRegexResult = percentRegexResults[i]
					var count = strings.Count(itemString, percentRegexResults[i])
					/* TODO tips: 此处需检查各种语言间占位符是否一致 */
					for i := 0; i < count; i++ {
						itemString = strings.Replace(itemString, percentRegexResult, percentRegexResult+"%", 1)
					}
				}
			}
			androidI18nFile.WriteString(itemString)
		}

		androidI18nFile.WriteString("\n</resources>")
		androidI18nFile.Close()
	}
	fmt.Println("Woo~~~ Congratulations for you!!!")
	return nil
}

func ReadCSVFile(filePath string) error {
	lop.E(filePath)
	file, err := os.Open(filePath)
	if err != nil {
		lop.E(err.Error())
		return err
	}
	defer file.Close()

	reader := csv.NewReader(file)
	reader.FieldsPerRecord = -1 // Disable record length test

	// Read header
	header, err := reader.Read()
	if err != nil {
		lop.E(err.Error())
		return err
	}

	// Read rows
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			lop.E(err.Error())
			return err
		}

		// Check number of fields
		if len(record) != len(header) {
			// Handle wrong number of fields
			lop.E("Wrong number of fields: %v\n", record)

			return errors.New(fmt.Sprint("Wrong number of fields: %v\n", record))
		}

		// Process record
		// ...
	}

	return nil
}
