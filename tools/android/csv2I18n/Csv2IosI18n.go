package csv2I18n

import (
	"flag"
	"hi_golang/tools/lop"
	"strings"
)

/*
	only supports *.csv file
	release v2 cmd:
	cmd:
		go run Csv2IosI18n.go -v <version_code> -abbrs <country_short_name> -path <file_path>
	eg:
		go run Csv2IosI18n.go -v 1260 -abbrs zh,en,id -path "/Users/taohui/DevStation/hi_golang/hi_golang/tools/csv2i18n/csv/LamiV1.2.6.csv"
*/
func main2() {

	csvFilePath := "/Users/taohui/DevStation/hi_golang/hi_golang/tools/csv2i18n/csv/LamiV1.2.6.csv"
	fileSuffix := flag.String("v", "", "# of iterations")
	abbrs := flag.String("abbrs", "", "# of iterations")
	path := flag.String("path", "", "# of iterations")
	flag.Parse()
	lop.I("Csv2IosI18n version: 2.0.0")
	lop.I("-v ", (*fileSuffix))
	lop.I("-abbrs ", *abbrs)
	lop.I("-path", *path)
	lop.I("")

	countryAbbrArray := strings.Split(*abbrs, ",")
	csvFilePath = *path

	for _, v := range countryAbbrArray {
		lop.I("country abbr: " + v)
	}

	Csv2AndroidI18n("", csvFilePath, *fileSuffix, countryAbbrArray)

}

// func Csv2AndroidI18n(csvFilePath, version string, countryAbbrArray []string) {
// 	csvFile, _ := os.Open(csvFilePath)
// 	reader := csv.NewReader(bufio.NewReader(csvFile))
// 	reader.LazyQuotes = true
// 	androidI18nFiles := make(map[string][]string, len(countryAbbrArray))
// 	for _, v := range countryAbbrArray {
// 		androidI18nFiles[v] = []string{}
// 	}

// 	firstBool := true

// 	for {
// 		line, error := reader.Read()
// 		if error == io.EOF {
// 			break
// 		} else if error != nil {
// 			log.Fatal(error)
// 		}
// 		if firstBool == true {
// 			firstBool = false
// 			continue
// 		}
// 		for index, v := range countryAbbrArray {
// 			androidI18nFiles[v] = append(androidI18nFiles[v], line[index])

// 		}
// 	}
// 	fmt.Println(androidI18nFiles)
// 	re := regexp.MustCompile("\\w{2,}")

// 	/* initialize keys */
// 	var keys []string
// 	var keyDuplicateMap = make(map[string]uint32)
// 	for _, v := range androidI18nFiles["en"] {

// 		upperString := strings.ToLower(v)
// 		upperString = strings.ReplaceAll(upperString, "’", "")
// 		upperString = strings.ReplaceAll(upperString, "'", "")
// 		upperString = strings.ReplaceAll(upperString, " ", "_")
// 		upperString = re.FindString(upperString)
// 		upperString = "s_" + version + "_" + upperString
// 		/* 如果是纯数字 */

// 		if duplicateID, isExist := keyDuplicateMap[upperString]; isExist {
// 			keyDuplicateMap[upperString] = duplicateID + 1
// 			keys = append(keys, "empty_"+fmt.Sprint(upperString, keyDuplicateMap[upperString]))
// 		} else {
// 			keyDuplicateMap[upperString] = 0
// 			keys = append(keys, upperString)
// 		}
// 	}

// 	/** 国家的简写 to 文件名 */
// 	for countryShortName, androidI18nFileValues := range androidI18nFiles {
// 		mkDirAllResultErr := os.MkdirAll("outputs/values-"+countryShortName, 0777)
// 		if mkDirAllResultErr != nil {
// 			log.Fatal(mkDirAllResultErr)
// 		}
// 		stringsPwd := "outputs/values-" + countryShortName + "/strings" + version + ".xml"
// 		androidI18nFile, err := os.OpenFile(stringsPwd, os.O_CREATE|os.O_RDWR, 0777)
// 		if err != nil {
// 			log.Fatal(err)
// 		}

// 		/** 生成xml文件头部声明 */
// 		if _, err := androidI18nFile.WriteString("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<resources>\n"); err != nil {
// 			log.Fatal(err)
// 		}

// 		for k, content := range keys {
// 			if len(content) == 0 {
// 				continue
// 			}

// 			var androidI18nFileValue = androidI18nFileValues[k]
// 			androidI18nFileValue = strings.ReplaceAll(androidI18nFileValue, "\n", `\n`)
// 			itemString := fmt.Sprint(`<string name="`, content, `">`, androidI18nFileValue, `</string>`, "\n")
// 			itemString = strings.ReplaceAll(itemString, `'`, `\'`)

// 			/** 为了处理一些特殊语言在输入的时候会自动顺序颠倒的问题 */
// 			if (strings.Compare("ar", countryShortName) == 0 || strings.Compare("ur", countryShortName) == 0) && strings.Contains(itemString, "s%") {
// 				itemString = strings.ReplaceAll(itemString, "s%", "%s")
// 			}
// 			itemString = strings.ReplaceAll(itemString, "%S", "%s")

// 			var count = strings.Count(itemString, "%s")
// 			for i := 0; i < count; i++ {
// 				itemString = strings.Replace(itemString, "%s", fmt.Sprintf("%%%d$s", i+1), 1)
// 			}

// 			/** 处理百分号，%符号影响格式化的操作 */
// 			var percentRegex = regexp.MustCompile("\\d+(?:\\.\\d+)?%")
// 			var percentRegexResults = percentRegex.FindAllString(itemString, len(itemString))

// 			if len(percentRegexResults) > 0 {
// 				for i := 0; i < len(percentRegexResults); i++ {
// 					var percentRegexResult = percentRegexResults[i]
// 					var count = strings.Count(itemString, percentRegexResults[i])
// 					/* TODO tips: 此处需检查各种语言间占位符是否一致 */
// 					for i := 0; i < count; i++ {
// 						itemString = strings.Replace(itemString, percentRegexResult, percentRegexResult+"%", 1)
// 					}
// 				}
// 			}
// 			androidI18nFile.WriteString(itemString)
// 		}

// 		androidI18nFile.WriteString("\n</resources>")
// 		androidI18nFile.Close()
// 	}
// 	fmt.Println("Woo~~~ Congratulations for you!!!")
// }
