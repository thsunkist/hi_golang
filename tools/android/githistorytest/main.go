package main

import (
	"fmt"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

// 获取文件的 git 最后修改时间
func getGitLastModifiedTime(filePath string) (time.Time, error) {
	// Git 仓库的根目录
	repoRoot := "/Users/mac/DevStation/hi_android/oe-android-bak/oe-android"

	// 转换为相对路径
	relativePath, err := filepath.Rel(repoRoot, filePath)
	if err != nil {
		return time.Time{}, fmt.Errorf("获取相对路径时出错: %v", err)
	}

	// 构造 git log 命令
	cmd := exec.Command("git", "log", "-1", "--format=%ct", relativePath)
	cmd.Dir = repoRoot // 设置工作目录为 Git 仓库根目录

	// 执行命令
	output, err := cmd.Output()
	if err != nil {
		return time.Time{}, fmt.Errorf("执行 git log 时出错: %v", err)
	}

	// 转换输出为时间戳
	timestampStr := strings.TrimSpace(string(output))
	timestamp, err := strconv.ParseInt(timestampStr, 10, 64)
	if err != nil {
		return time.Time{}, fmt.Errorf("解析时间戳时出错: %v", err)
	}

	// 转换为 time.Time
	return time.Unix(timestamp, 0), nil
}

func main() {
	// 测试文件路径
	filePath := "/Users/mac/DevStation/hi_android/oe-android-bak/oe-android/erban_client/src/main/java/com/halo/mobile/XChatApplication.java"

	// 获取最后修改时间
	modTime, err := getGitLastModifiedTime(filePath)
	if err != nil {
		fmt.Printf("获取文件 %s 的 git 最后修改时间时出错: %v\n", filePath, err)
		return
	}

	fmt.Printf("文件 %s 的最后修改时间是: %s\n", filePath, modTime.Format(time.RFC3339))
}
