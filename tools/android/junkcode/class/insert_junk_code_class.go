package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

/* 需要清理所有的文档注释 \/\*\*(?:.|\n)*?\*\/ */
/* ^ \*.*\{*\} 这样靠谱点 */
const projectRoot = "/Users/mac/DevStation/hi_android/Yolo-Android"

var (
	filePattern  = regexp.MustCompile(`.*\.(java)$`)
	classPattern = regexp.MustCompile(`(?m)^\s*(public\s+)?(abstract\s+)?class\s+(\w+)`)

	methodTemplateClass = `
	static { %s(); }
private static java.lang.String %s() {
    return "%s000001"%s;
}
`
	fieldTemplateClass = "static int %s = 0x%06X;\n"
)

func generateRandomFields(className string) (string, string) {
	var fields strings.Builder
	var appendFiledName2String = ""
	for i := 1; i <= 10; i++ {
		fieldName := fmt.Sprintf("%s%06X", strings.ToLower(className[:1])+className[1:], i) + generate64BitRandomFields()
		appendFiledName2String += ("+" + fieldName)
		fields.WriteString(fmt.Sprintf(fieldTemplateClass, fieldName, i))
	}
	return fields.String(), appendFiledName2String
}

func insertFieldsAndMethodInClassFile(filePath string) {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return
	}

	contentStr := string(content)
	classMatch := classPattern.FindStringSubmatch(contentStr)

	if classMatch != nil {
		className := classMatch[3]
		methodName := strings.ToLower(className[:1]) + className[1:] + "LeekaLive" + generate64BitRandomFields()
		insertPosition := strings.LastIndex(contentStr, "}")
		if insertPosition != -1 {
			fieldsStr, appendFiledName2String := generateRandomFields(className)
			methodStr := fmt.Sprintf(methodTemplateClass, methodName, methodName, methodName, appendFiledName2String)
			newContent := contentStr[:insertPosition] + methodStr + contentStr[insertPosition:]
			newContent = strings.Replace(newContent, "{", "{\n"+fieldsStr, 1)
			if err := ioutil.WriteFile(filePath, []byte(newContent), 0644); err != nil {
				fmt.Println("Error writing file:", err)
				return
			}
			fmt.Printf("Inserted fields and method in class %s\n", filePath)
		}
	}
}

func processProjectForClasses(rootDir string) {
	err := filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && filePattern.MatchString(info.Name()) {
			insertFieldsAndMethodInClassFile(path)
		}
		return nil
	})
	if err != nil {
		fmt.Println("Error walking the path:", err)
	}
}

func main() {
	processProjectForClasses(projectRoot)
}

func generate64BitRandomFields() string {
	rand.Seed(time.Now().UnixNano())

	const charset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	result := make([]byte, 6)
	for i := range result {
		result[i] = charset[rand.Intn(len(charset))]
	}

	log.Println(string(result))
	return string(result)
}
