import os
import hashlib

def modify_file_md5(file_path):
    with open(file_path, 'ab') as f:
        # 添加无关紧要的数据
        f.write(b'\2')  # habby \0 larki \1 yolo \2

def get_md5(file_path):
    hasher = hashlib.md5()
    with open(file_path, 'rb') as f:
        buf = f.read()
        hasher.update(buf)
    return hasher.hexdigest()

def process_images(root_dir):
    for dirpath, _, filenames in os.walk(root_dir):
        for filename in filenames:
            if filename.lower().endswith(('.webp', '.png')):
                file_path = os.path.join(dirpath, filename)
                old_md5 = get_md5(file_path)
                modify_file_md5(file_path)
                new_md5 = get_md5(file_path)
                print(f'File: {file_path}')
                print(f'Old MD5: {old_md5}')
                print(f'New MD5: {new_md5}')

if __name__ == "__main__":
    # 替换为你的项目路径
    project_root = '/Users/mac/DevStation/hi_android/Yolo-Android/'
    process_images(project_root)
