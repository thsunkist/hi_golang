package main

import (
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func modifyFileMD5(filePath string) error {
	f, err := os.OpenFile(filePath, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer f.Close()

	for i := 0; i < 10; i++ {
		byteSlice := make([]byte, 1)
		_, err = rand.Read(byteSlice)
		if err != nil {
			return err
		}

		if _, err = f.Write(byteSlice); err != nil {
			return err
		}
	}

	return nil
}

func getMD5(filePath string) (string, error) {
	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		return "", err
	}

	hasher := md5.New()
	hasher.Write(bytes)
	return hex.EncodeToString(hasher.Sum(nil)), nil
}

func processImages(rootDir string) {
	filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
			return err
		}

		if !info.IsDir() && strings.HasSuffix(strings.ToLower(info.Name()), ".webp") || strings.HasSuffix(strings.ToLower(info.Name()), ".png") {
			oldMD5, _ := getMD5(path)
			modifyFileMD5(path)
			newMD5, _ := getMD5(path)
			fmt.Printf("File: %s\n", path)
			fmt.Printf("Old MD5: %s\n", oldMD5)
			fmt.Printf("New MD5: %s\n", newMD5)
		}

		return nil
	})
}

func main() {
	projectRoot := "/Users/mac/DevStation/hi_android/Nyko-Android/"
	processImages(projectRoot)
}
