package main

import (
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"
)

func main() {
	// Read the Java source file.
    data, err := ioutil.ReadFile("/Users/mac/DevStation/hi_android/library-project-androidX/lib_uikit/src/main/java/com/juxiao/library_ui/widget/AppToolBar.java")
	if err != nil {
		log.Fatal(err)
	}

	// Define a regular expression to match Java method definitions.
	re := regexp.MustCompile(`(public|protected|private|static|\s) +[\w\<\>\[\]]+\s+(\w+) *\([^\)]*\) *(\{?|[^;])`)

	// Replace method definitions with method definitions wrapped with log statements.
	modifiedData := re.ReplaceAllStringFunc(string(data), func(str string) string {
		if strings.HasSuffix(str, "{") && !strings.Contains(str, "this(") && !strings.Contains(str, "super(") {
			return str + `Log.d("Start method", "` + strings.Split(str, " ")[len(strings.Split(str, " "))-1] + `");`
		}
		return str
	})

	// Write the modified data back to the file.
    err = ioutil.WriteFile("/Users/mac/DevStation/hi_android/library-project-androidX/lib_uikit/src/main/java/com/juxiao/library_ui/widget/AppToolBar.java", []byte(modifiedData), os.ModePerm)
	if err != nil {
		log.Fatal(err)
	}
}
