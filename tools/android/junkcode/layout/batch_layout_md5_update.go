package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/beevik/etree"
)

func modifyFileMD5(filePath string) error {
	doc := etree.NewDocument()
	if err := doc.ReadFromFile(filePath); err != nil {
		log.Printf("failed to read file %q: %v", filePath, err)
		return err
	}

	root := doc.SelectElement("*")
	if root == nil {
		root = doc.CreateElement("root") // replace "root" with the name of the root element you want to create
	}

	root.CreateAttr("xmlns:custom", "http://schemas.android.com/apk/res-auto")
	root.CreateAttr("custom:timestamp", time.Now().Format(time.RFC3339))

	err := doc.WriteToFile(filePath)
	if err != nil {
		log.Printf("failed to write file %q: %v", filePath, err)
	}
	return err
}

func main() {
	projectRoot := "/Users/mac/DevStation/hi_android/Yolo-Android" // replace with your project root directory

	filepath.Walk(projectRoot, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
			return err
		}

		if !info.IsDir() && strings.HasSuffix(strings.ToLower(info.Name()), ".xml") && strings.Contains(path, "res/layout") {
			if err := modifyFileMD5(path); err != nil {
				fmt.Printf("failed to modify file %q: %v\n", path, err)
				return err
			}
		}

		return nil
	})
}
