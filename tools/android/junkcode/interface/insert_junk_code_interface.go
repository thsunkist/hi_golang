package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

/* 需要清理所有的文档注释 \/\*\*(?:.|\n)*?\*\/ */
const projectRoot = "/Users/mac/DevStation/hi_android/Yolo-Android"

var (
	filePattern             = regexp.MustCompile(`.*\.(java)$`)
	publicInterfacePattern  = regexp.MustCompile(`(?m)^\s*public\s+interface\s+(\w+)`)
	methodTemplateInterface = `
default void %s() {
}
`
	fieldTemplateInterface = "static final int %s = 0x%06X;\n"
)

func generateRandomFields(className string) string {
	var fields strings.Builder
	for i := 1; i <= 10; i++ {
		fieldName := fmt.Sprintf("%s%06X", strings.ToLower(className[:1])+className[1:], i)
		fields.WriteString(fmt.Sprintf(fieldTemplateInterface, fieldName, i))
	}
	return fields.String()
}

func insertFieldsAndMethodInPublicInterfaceFile(filePath string) {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return
	}

	contentStr := string(content)
	publicInterfaceMatch := publicInterfacePattern.FindStringSubmatch(contentStr)

	if publicInterfaceMatch != nil {
		interfaceName := publicInterfaceMatch[1]
		methodName := strings.ToLower(interfaceName[:1]) + interfaceName[1:] + "Method"
		insertPosition := strings.LastIndex(contentStr, "}")
		if insertPosition != -1 {
			methodStr := fmt.Sprintf(methodTemplateInterface, methodName)
			fieldsStr := generateRandomFields(interfaceName)
			newContent := contentStr[:insertPosition] + methodStr + contentStr[insertPosition:]
			newContent = strings.Replace(newContent, "{", "{\n"+fieldsStr, 1)
			if err := ioutil.WriteFile(filePath, []byte(newContent), 0644); err != nil {
				fmt.Println("Error writing file:", err)
				return
			}
			fmt.Printf("Inserted fields and method in public interface %s\n", filePath)
		}
	}
}

func processProjectForPublicInterface(rootDir string) {
	err := filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() && filePattern.MatchString(info.Name()) {
			insertFieldsAndMethodInPublicInterfaceFile(path)
		}
		return nil
	})
	if err != nil {
		fmt.Println("Error walking the path:", err)
	}
}

func main() {
	processProjectForPublicInterface(projectRoot)
}
