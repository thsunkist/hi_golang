package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

func main() {
	// Seed the random number generator
	rand.Seed(time.Now().UnixNano())

	// Create a new file
	file, err := os.Create("bt-proguard.txt")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// Create a map to store the generated strings
	generatedStrings := make(map[string]bool)

	// Define the characters to use
	characters := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

	// Generate 5000 unique lines
	for len(generatedStrings) < 5000 {
		// Generate a line of random length (between 1 and 12 characters)
		lineLength := rand.Intn(12) + 1
		line := ""
		for j := 0; j < lineLength; j++ {
			// Generate a random character
			char := characters[rand.Intn(len(characters))]
			line += string(char)
		}

		// Check if the line is unique
		if _, exists := generatedStrings[line]; !exists {
			// If the line is unique, write it to the file and add it to the map
			fmt.Fprintln(file, line)
			generatedStrings[line] = true
		}
	}
}