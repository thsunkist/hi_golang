package main

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"
)

// 定义文件信息结构
type FileInfo struct {
	ModTime       time.Time
	LastAuthor    string
	ContentLength int // 文件内容的字符长度
	RelativePath  string
	FileName      string
}

func main() {
	// 检查是否提供了目录参数
	if len(os.Args) < 2 {
		fmt.Println("用法: go run main.go <Android项目绝对目录>")
		return
	}

	// 获取命令行传入的目录
	rootDir := os.Args[1]

	// 检查目录是否存在
	if _, err := os.Stat(rootDir); os.IsNotExist(err) {
		fmt.Printf("指定的目录不存在: %s\n", rootDir)
		return
	}

	// 删除所有子目录中的 build 文件夹
	if err := removeBuildDirectories(rootDir); err != nil {
		fmt.Printf("删除 build 文件夹时出错: %v\n", err)
		return
	}

	// 调用 gradlew clean 清理缓存
	if err := cleanGradleCache(rootDir); err != nil {
		fmt.Printf("清理 Gradle 缓存时出错: %v\n", err)
		return
	}

	// 定义用于存储文件信息的切片
	var files []FileInfo

	// 遍历目录
	err := filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// 检查是否是文件并且扩展名为 .java 或 .kotlin
		if !info.IsDir() && (filepath.Ext(path) == ".java" || filepath.Ext(path) == ".kt") {
			// 获取文件的相对路径
			relativePath, err := filepath.Rel(rootDir, path)
			if err != nil {
				return err
			}

			// 获取文件的最后修改时间和作者（从 git 获取）
			modTime, author, err := getGitLastModifiedInfo(path, rootDir)
			if err != nil {
				fmt.Printf("获取文件 %s 的 git 最后修改信息时出错: %v\n", path, err)
				return nil // 忽略该文件的错误，继续处理其他文件
			}

			// 计算文件内容的字符长度
			contentLength, err := getFileContentLength(path)
			if err != nil {
				fmt.Printf("计算文件 %s 的内容长度时出错: %v\n", path, err)
				return nil // 忽略该文件的错误，继续处理其他文件
			}

			// 将文件信息添加到切片中
			files = append(files, FileInfo{
				ModTime:       modTime,
				LastAuthor:    author,
				ContentLength: contentLength,
				RelativePath:  relativePath,
				FileName:      info.Name(),
			})

			// 更新文件的修改时间为 git 的最后修改时间
			if err := os.Chtimes(path, modTime, modTime); err != nil {
				fmt.Printf("更新文件 %s 的修改时间时出错: %v\n", path, err)
				return nil // 忽略该文件的错误，继续处理其他文件
			}
		}
		return nil
	})

	if err != nil {
		fmt.Printf("遍历目录时出错: %v\n", err)
		return
	}

	// 按时间升序排序
	sort.Slice(files, func(i, j int) bool {
		return files[i].ModTime.Before(files[j].ModTime)
	})

	// 动态生成输出文件名
	outputFileName := generateOutputFileName(rootDir)

	// 写入 CSV 文件
	if err := writeCSV(files, outputFileName); err != nil {
		fmt.Printf("写入 CSV 文件时出错: %v\n", err)
		return
	}

	fmt.Printf("文件信息已成功写入 %s\n", outputFileName)
}

// 删除所有子目录中的 build 文件夹
func removeBuildDirectories(rootDir string) error {
	fmt.Println("正在删除所有子目录中的 build 文件夹...")
	cmd := exec.Command("sh", "-c", "rm -rf */build")
	cmd.Dir = rootDir // 设置工作目录为传入的项目目录
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return fmt.Errorf("删除 build 文件夹时出错: %v", err)
	}
	fmt.Println("所有 build 文件夹已删除！")
	return nil
}

// 清理 Gradle 缓存
func cleanGradleCache(rootDir string) error {
	gradlewPath := filepath.Join(rootDir, "gradlew")
	if _, err := os.Stat(gradlewPath); os.IsNotExist(err) {
		return fmt.Errorf("未找到 gradlew 脚本: %s", gradlewPath)
	}

	fmt.Println("正在清理 Gradle 缓存...")
	cmd := exec.Command(gradlewPath, "clean")
	cmd.Dir = rootDir // 设置工作目录为传入的项目目录
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return fmt.Errorf("执行 gradlew clean 时出错: %v", err)
	}
	fmt.Println("Gradle 缓存清理完成！")
	return nil
}

// 获取文件的 git 最后修改时间和作者
func getGitLastModifiedInfo(filePath, repoRoot string) (time.Time, string, error) {
	// 转换为相对路径
	relativePath, err := filepath.Rel(repoRoot, filePath)
	if err != nil {
		return time.Time{}, "", fmt.Errorf("获取相对路径时出错: %v", err)
	}

	// 执行 git log 命令获取最后修改时间和作者
	cmd := exec.Command("git", "log", "-1", "--format=%ct|%an", relativePath)
	cmd.Dir = repoRoot // 设置工作目录为 Git 仓库根目录
	output, err := cmd.Output()
	if err != nil {
		return time.Time{}, "", fmt.Errorf("执行 git log 时出错: %v", err)
	}

	// 解析输出内容
	outputStr := strings.TrimSpace(string(output))
	parts := strings.Split(outputStr, "|")
	if len(parts) != 2 {
		return time.Time{}, "", fmt.Errorf("解析 git log 输出时出错: %s", outputStr)
	}

	// 转换时间戳
	timestamp, err := strconv.ParseInt(parts[0], 10, 64)
	if err != nil {
		return time.Time{}, "", fmt.Errorf("解析时间戳时出错: %v", err)
	}

	// 返回时间和作者
	return time.Unix(timestamp, 0), parts[1], nil
}

// 获取文件内容的字符长度
func getFileContentLength(filePath string) (int, error) {
	// 读取文件内容
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return 0, fmt.Errorf("读取文件内容时出错: %v", err)
	}

	// 返回字符长度
	return len(content), nil
}

// 写入 CSV 文件
func writeCSV(files []FileInfo, outputFilePath string) error {
	// 打开 CSV 文件以写入
	outputFile, err := os.Create(outputFilePath)
	if err != nil {
		return fmt.Errorf("无法创建 CSV 文件: %v", err)
	}
	defer outputFile.Close()

	// 创建 CSV 写入器
	writer := csv.NewWriter(outputFile)
	defer writer.Flush()

	// 写入 CSV 表头
	header := []string{"Last Modified (YYYY-MM-DD)", "Last Author", "Content Length", "Relative Path", "File Name"}
	if err := writer.Write(header); err != nil {
		return fmt.Errorf("写入 CSV 表头时出错: %v", err)
	}

	// 写入排序后的文件信息
	for _, file := range files {
		record := []string{
			file.ModTime.Format("2006-01-02"),
			file.LastAuthor,
			strconv.Itoa(file.ContentLength),
			file.RelativePath,
			file.FileName,
		}
		if err := writer.Write(record); err != nil {
			return fmt.Errorf("写入 CSV 行时出错: %v", err)
		}
	}

	return nil
}

// 生成输出文件名
func generateOutputFileName(rootDir string) string {
	// 获取目录的最后一个部分作为文件名
	baseName := filepath.Base(rootDir)
	// 拼接结果文件名
	return fmt.Sprintf("%s-result.csv", baseName)
}
