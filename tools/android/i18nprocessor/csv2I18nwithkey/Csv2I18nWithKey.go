package csv2i18nwithkey

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"hi_golang/tools"
	"hi_golang/tools/lop"
	"io"
	"log"
	"os"
	"path/filepath"
)

func main() {

}

func Csv2I18nWithKey(path string) string {
	var csvFilePath = path //"./lc_vi.csv"
	csvFile, _ := os.Open(csvFilePath)
	defer csvFile.Close()
	reader := csv.NewReader(bufio.NewReader(csvFile))
	reader.LazyQuotes = true
	line1, _ := reader.Read()
	var rows = make([][]string, len(line1))
	lop.I(len(line1))

	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			panic(error)
		}
		for i := 0; i < len(line1); i++ {
			rows[i] = append(rows[i], line[i])
			// rows[1] = append(rows[1], line[1])
			// rows[2] = append(rows[2], line[2])
			// rows[3] = append(rows[3], line[3])
		}
	}

	lop.D(len(rows), len(rows[0]), len(rows[1]), len(rows[2]), len(rows[3]))
	// lop.D(line1[0], line1[1], line1[2], line1[3])

	outputDirPath := filepath.Join("tmp", "androidi18n", "add_language", "generate", tools.GenFileName("outputs"))
	lop.D(outputDirPath)
	arr := []string{}
	for i, columns := range rows {

		lop.D(len(rows[i]))
		// log.Println(columns)
		countryShortName := line1[i] //columns[0]
		version := countryShortName  //"1270_patch1"
		// mkDirAllResultErr := os.MkdirAll("outputs/values-"+countryShortName, 0777)
		// if mkDirAllResultErr != nil {
		// 	panic(mkDirAllResultErr)
		// }

		os.MkdirAll(outputDirPath, 0777)
		stringsPwd := filepath.Join(outputDirPath, "strings"+version+".xml") // "outputs/values-" + countryShortName + "/strings" + version + ".xml"
		arr = append(arr, stringsPwd)
		lop.I(stringsPwd)
		androidI18nFile, err := os.OpenFile(stringsPwd, os.O_CREATE|os.O_RDWR, 0777)
		defer androidI18nFile.Close()
		if err != nil {
			panic(err)
		}

		/** 生成xml文件头部声明 */
		if _, err := androidI18nFile.WriteString("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<resources>\n"); err != nil {
			log.Fatal(err)
		}
		var isFirstRow = true
		for index, column := range columns {
			if isFirstRow {
				isFirstRow = false
			}
			var itemString = ""
			if len(column) == 0 {
				itemString = fmt.Sprint(`<string name="`, rows[0][index], `">`, rows[1][index], `</string>`, "\n")
			} else {
				itemString = fmt.Sprint(`<string name="`, rows[0][index], `">`, column, `</string>`, "\n")
			}
			androidI18nFile.WriteString(itemString)
		}
		androidI18nFile.WriteString("\n</resources>")
		androidI18nFile.Close()
	}

	return outputDirPath
}
