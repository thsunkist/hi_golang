package i18nxml2Csv

import (
	"encoding/csv"
	"encoding/xml"
	"hi_golang/tools/lop"
	"io/ioutil"
	"os"
)

type HostProperties struct {
	XMLName xml.Name `xml:"resources"`
	Tags    []Tag    `xml:"string"`
}

type Tag struct {
	Key string `xml:"name,attr"`
	/* <tag>value</tag> */
	Value string `xml:",innerxml"`
}

/*
根据共同的 string-name 合并两个资源文件的string-value, 生成含key的csv对照翻译表
格式:

	key,zh,cn
*/
func main() {
	lop.D("i18n2Csv running")

	cnI18nCsvMap := getZhI18nMap("resources/values-zh")
	enI18nCsvMap := getZhI18nMap("resources/values-en")

	allCsvDataArray := [][3]string{}
	titles := [3]string{}
	titles[0] = "key"
	titles[1] = "en"
	titles[2] = "cn"
	allCsvDataArray = append(allCsvDataArray, titles)

	for k, enValue := range enI18nCsvMap {
		enCnArray := [3]string{}

		cnValue, exist := cnI18nCsvMap[k]
		if exist {
			enCnArray[0] = k
			enCnArray[1] = enValue[0]
			enCnArray[2] = cnValue[0]
		} else {
			enCnArray[0] = k
			enCnArray[1] = enValue[0]
			enCnArray[2] = ""
		}

		allCsvDataArray = append(allCsvDataArray, enCnArray)
	}
	os.MkdirAll("outputs", 0777)

	csvFile, _ := os.Create("outputs/result.csv")

	csvwriter := csv.NewWriter(csvFile)
	for _, empRow := range allCsvDataArray {
		// lop.I(empRow[0], empRow[1], empRow[2])
		_ = csvwriter.Write([]string{empRow[0], empRow[1], empRow[2]})
	}
	csvwriter.Flush()
	csvFile.Close()
}

func getZhI18nMap(zhDirPath string) map[string][]string {
	cnI18nCsvMap := make(map[string][]string, 0)

	zhDir, _ := ioutil.ReadDir(zhDirPath)
	zhHostProperties := HostProperties{}

	cnHostPropertiesArray := make([]interface{}, 0)
	for _, fi := range zhDir {
		filePtr, _ := os.OpenFile(zhDirPath+"/"+fi.Name(), os.O_RDONLY, 0777)
		contentBytes, _ := ioutil.ReadAll(filePtr)
		xml.Unmarshal(contentBytes, &zhHostProperties)
		cnHostPropertiesArray = append(cnHostPropertiesArray, zhHostProperties)
	}
	for _, hostProperties := range cnHostPropertiesArray {
		var cn = hostProperties.(HostProperties)
		for _, t := range cn.Tags {
			cnI18nCsvMap[t.Key] = append(cnI18nCsvMap[t.Key], t.Value)
		}
	}
	return cnI18nCsvMap
}
