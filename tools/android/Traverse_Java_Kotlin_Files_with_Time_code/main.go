package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"time"
)

// 定义文件信息结构
type FileInfo struct {
	ModTime      time.Time
	RelativePath string
	FileName     string
}

func main() {
	// 检查是否提供了目录参数
	if len(os.Args) < 2 {
		fmt.Println("用法: go run main.go <Android项目绝对目录>")
		return
	}

	// 获取命令行传入的目录
	rootDir := os.Args[1]

	// 检查目录是否存在
	if _, err := os.Stat(rootDir); os.IsNotExist(err) {
		fmt.Printf("指定的目录不存在: %s\n", rootDir)
		return
	}

	// 调用 gradlew clean 清理缓存
	gradlewPath := filepath.Join(rootDir, "gradlew")
	if _, err := os.Stat(gradlewPath); os.IsNotExist(err) {
		fmt.Printf("未找到 gradlew 脚本: %s\n", gradlewPath)
		return
	}

	fmt.Println("正在清理 Gradle 缓存...")
	cmd := exec.Command(gradlewPath, "clean")
	cmd.Dir = rootDir // 设置工作目录为传入的项目目录
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		fmt.Printf("执行 gradlew clean 时出错: %v\n", err)
		return
	}
	fmt.Println("Gradle 缓存清理完成！")

	// 定义用于存储文件信息的切片
	var files []FileInfo

	// 遍历目录
	err := filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// 检查是否是文件并且扩展名为 .java 或 .kotlin
		if !info.IsDir() && (filepath.Ext(path) == ".java" || filepath.Ext(path) == ".kotlin") {
			// 获取文件的相对路径
			relativePath, err := filepath.Rel(rootDir, path)
			if err != nil {
				return err
			}

			// 将文件信息添加到切片中
			files = append(files, FileInfo{
				ModTime:      info.ModTime(),
				RelativePath: relativePath,
				FileName:     info.Name(),
			})
		}
		return nil
	})

	if err != nil {
		fmt.Printf("遍历目录时出错: %v\n", err)
		return
	}

	// 按时间升序排序
	sort.Slice(files, func(i, j int) bool {
		return files[i].ModTime.Before(files[j].ModTime)
	})

	// 打开 CSV 文件以写入
	outputFile, err := os.Create("output.csv")
	if err != nil {
		fmt.Printf("无法创建 CSV 文件: %v\n", err)
		return
	}
	defer outputFile.Close()

	// 创建 CSV 写入器
	writer := csv.NewWriter(outputFile)
	defer writer.Flush()

	// 写入 CSV 表头
	header := []string{"Last Modified (YYYY-MM-DD)", "Relative Path", "File Name"}
	if err := writer.Write(header); err != nil {
		fmt.Printf("写入 CSV 表头时出错: %v\n", err)
		return
	}

	// 写入排序后的文件信息
	for _, file := range files {
		record := []string{
			file.ModTime.Format("2006-01-02"),
			file.RelativePath,
			file.FileName,
		}
		if err := writer.Write(record); err != nil {
			fmt.Printf("写入 CSV 行时出错: %v\n", err)
			return
		}
	}

	fmt.Println("文件信息已成功写入 output.csv")
}
