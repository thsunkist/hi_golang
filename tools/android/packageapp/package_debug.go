package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

const sshKeyPassword = "tttt1111"

func main() {
	// 获取当前工作目录
	dir, err := os.Getwd()
	log.Println("dir: " + dir)
	if err != nil {
		log.Fatalf("获取当前工作目录失败: %v", err)
	}
	executablePath, err := os.Executable()
	executableDir := filepath.Dir(executablePath)
	log.Println("dir: " + executableDir)

	// 构建SSH密钥文件的绝对路径
	sshKeyPath := filepath.Join(executableDir, "codeup_access_ssh")
	if err != nil {
		log.Fatalf("获取SSH密钥文件的绝对路径失败: %v", err)
	}

	// 检查SSH密钥文件是否存在
	if _, err := os.Stat(sshKeyPath); os.IsNotExist(err) {
		log.Fatalf("SSH密钥文件不存在: %v", sshKeyPath)
	}

	// 设置SSH密钥文件权限
	err = os.Chmod(sshKeyPath, 0600)
	if err != nil {
		log.Fatalf("设置SSH密钥文件权限失败: %v", err)
	}

	// 创建临时的 ssh-askpass 脚本
	askpassScript := fmt.Sprintf("#!/bin/sh\necho %s\n", sshKeyPassword)
	tmpFile, err := ioutil.TempFile("", "ssh-askpass")
	if err != nil {
		log.Fatalf("创建临时文件失败: %v", err)
	}
	defer os.Remove(tmpFile.Name())

	if _, err := tmpFile.Write([]byte(askpassScript)); err != nil {
		log.Fatalf("写入临时文件失败: %v", err)
	}
	if err := tmpFile.Close(); err != nil {
		log.Fatalf("关闭临时文件失败: %v", err)
	}

	if err := os.Chmod(tmpFile.Name(), 0700); err != nil {
		log.Fatalf("设置临时文件权限失败: %v", err)
	}

	// 启动 ssh-agent 并解析输出的环境变量
	cmd := exec.Command("ssh-agent", "-s")
	output, err := cmd.Output()
	if err != nil {
		log.Fatalf("启动ssh-agent失败: %v", err)
	}

	// 解析 ssh-agent 输出的环境变量
	lines := strings.Split(string(output), "\n")
	for _, line := range lines {
		if strings.HasPrefix(line, "SSH_AUTH_SOCK=") || strings.HasPrefix(line, "SSH_AGENT_PID=") {
			envVar := strings.Split(line, ";")[0]
			keyValue := strings.SplitN(envVar, "=", 2)
			os.Setenv(keyValue[0], keyValue[1])
		}
	}

	// 设置 SSH_ASKPASS 环境变量
	os.Setenv("SSH_ASKPASS", tmpFile.Name())
	os.Setenv("DISPLAY", ":0") // SSH_ASKPASS 通常需要 DISPLAY 环境变量

	// 添加 SSH 密钥到 ssh-agent
	cmd = exec.Command("ssh-add", sshKeyPath)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		log.Fatalf("添加SSH密钥到ssh-agent失败: %v", err)
	}
	cdExecutableDirCmd := exec.Command("cd", executableDir)
	if err := cdExecutableDirCmd.Run(); err != nil {
		log.Fatalf("cd executable dir cmd: %v", err)
	}

	// 自动提交当前分支的改动
	cmd = exec.Command("git", "add", ".")
	cmd.Dir = executableDir
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		log.Fatalf("添加改动失败: %v", err)
	}

	cmd = exec.Command("git", "commit", "-m", "Auto commit")
	cmd.Dir = executableDir

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		log.Fatalf("提交改动失败: %v", err)
	}

	// 强制推送到远程 dev 分支
	cmd = exec.Command("git", "push", "origin", "HEAD:dev", "--force")
	cmd.Dir = executableDir

	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		log.Fatalf("推送到远程 dev 分支失败: %v", err)
	}

	fmt.Println("代码已成功推送到远程 dev 分支")
}
