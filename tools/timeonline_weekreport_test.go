package tools

import (
	"bytes"
	"fmt"
	"os/exec"
	"testing"
)

func TestTimeOnlineWeekreport(_ *testing.T) {
	/*
		curl 'https://oapi.dingtalk.com/robot/send?access_token=93a20b674b3bb9f881d2f68cd112d1034d94027ab0dd08ebf3343376b23e8144&pc_slide=true' \
		 -H 'Content-Type: application/json' \
		 -d '{
		    "msgtype": "link",
		    "link": {
		        "text": "广告位招投",
		        "title": "⚠️周报提醒⚠️️️",
		        "picUrl": "http://i1.img.969g.com/down/imgx2015/02/27/289_095120_f0936.jpg",
		        "messageUrl": "https://alidocs.dingtalk.com/i/nodes/93NwLYZXWygM3L9lTy506OybJkyEqBQm?corpId=ding70975022872d7b7635c2f4657eb6378f&utm_medium=im_card&iframeQuery=sheet_range%3Dst-7488faa2-54271_0_0_1_5%26utm_medium%3Dim_card%26utm_source%3Dim&utm_scene=person_space&utm_source=im"
		    }
		}'
	*/

	/* test: 93a20b674b3bb9f881d2f68cd112d1034d94027ab0dd08ebf3343376b23e8144 */
	/* prod: 319568134cc83c90ff1c5cacc27d6dec1a027060e87f46ad544a74d9e2d0b2be
	https://tools.shijianline.cn:9004/assets/images/report_card.png
	*/
	var cmd = exec.Command(`curl`, `https://oapi.dingtalk.com/robot/send?access_token=319568134cc83c90ff1c5cacc27d6dec1a027060e87f46ad544a74d9e2d0b2be&pc_slide=true`,
		`-H`, `Content-Type: application/json`,
		`-d`, `{
	"msgtype": "link",
	"link": {
		"text": "点击填写安卓组周报",
		"title": "⚠️周报提醒⚠️️️",
		"picUrl": "https://tools.shijianline.cn:9004/assets/images/report_card.png",
		"messageUrl": "https://alidocs.dingtalk.com/i/nodes/YQBnd5ExVEwNpX6muja7Ee0d8yeZqMmz"
	}
}`)
/* https://alidocs.dingtalk.com/i/nodes/YQBnd5ExVEwNpX6muja7Ee0d8yeZqMmz?iframeQuery=sheet_range%3Dst-2b520672-80072_0_0_1_8 */
	/*https://alidocs.dingtalk.com/i/nodes/2X3LRMZdxkAJpx9y366qJGgrBYeOq5Ew */
	/*https://alidocs.dingtalk.com/i/nodes/Qnp9zOoBVBZgNj6zuOa74bgQV1DK0g6l*/
	/*https://alidocs.dingtalk.com/i/nodes/a9E05BDRVQ603Z5Lc1wYddyKJ63zgkYA*/
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err := cmd.Run()

	if err != nil {
		fmt.Printf("Stdout: %s\n", stdout.String())
		fmt.Printf("Stderr: %s\n", stderr.String())
	} else {
		fmt.Printf("Stdout: %s\n", stdout.String())
	}
	fmt.Println(err)

	var qaGroupCmd = exec.Command(`curl`, `https://oapi.dingtalk.com/robot/send?access_token=fa391f9f141a22224193c84bd168c956e19cb8bf725089a30f3f77a3cbcb3a13&pc_slide=true`,
		`-H`, `Content-Type: application/json`,
		`-d`, `{
		"msgtype": "link",
		"link": {
			"text": "点击填写测试组周报",
			"title": "⚠️周报提醒⚠️️️",
			"picUrl": "https://tools.shijianline.cn:9004/assets/images/report_card.png",
			"messageUrl": "https://alidocs.dingtalk.com/i/nodes/R1zknDm0WR3byQ5ehe91d4R9VBQEx5rG?utm_scene=person_space"
		}
	}`)
	var qaGroupCmdStdout bytes.Buffer
	var qaGroupCmdStderr bytes.Buffer
	qaGroupCmd.Stdout = &qaGroupCmdStdout
	qaGroupCmd.Stderr = &qaGroupCmdStderr

	qaGroupCmdRunErr := qaGroupCmd.Run()

	if qaGroupCmdRunErr != nil {
		fmt.Printf("Stdout: %s\n", qaGroupCmdStdout.String())
		fmt.Printf("Stderr: %s\n", qaGroupCmdStderr.String())
	} else {

		fmt.Printf("Stdout: %s\n", qaGroupCmdStdout.String())
	}

	fmt.Println(qaGroupCmdRunErr)
}
