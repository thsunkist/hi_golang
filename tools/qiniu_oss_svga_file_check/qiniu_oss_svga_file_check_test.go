package qiniuosssvgafilecheck

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

func TestQiniuOssSvgaFileCheck(_ *testing.T) {
	fmt.Println("hi TestQiniuOssSvgaFileCheck")

	// 打开文件
	file, err := os.Open("./habby-failed-svga-filelist.csv")
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()

	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer file.Close()

	// 使用 bufio.Scanner 逐行读取文件
	scanner := bufio.NewScanner(file)
	bucketname := "habby"

	for scanner.Scan() {
		line := scanner.Text()
		key := strings.Split(line, ",")[0]
		ossline := url.QueryEscape(line)
		fmt.Println(ossline)

		// 请求 GetOssFilesGet 接口
		url := fmt.Sprintf("http://127.0.0.1:8080/oss/file_get?ossline=%s&bucketname=%s", ossline, bucketname)
		resp, err := http.Get(url)
		if err != nil {
			fmt.Println("Error making request:", err)
			continue
		}
		defer resp.Body.Close()

		if resp.StatusCode != http.StatusOK {
			fmt.Println("Error response from server:", resp.Status)
			continue
		}

		// 创建 bucketfiles 文件夹
		err = os.MkdirAll("bucketfiles", os.ModePerm)
		if err != nil {
			fmt.Println("Error creating directory:", err)
			continue
		}

		// 创建文件
		filepath := filepath.Join("bucketfiles", key)
		outFile, err := os.Create(filepath)
		if err != nil {
			fmt.Println("Error creating file:", err)
			continue
		}

		// 将响应内容写入文件
		_, err = io.Copy(outFile, resp.Body)
		if err != nil {
			fmt.Println("Error writing to file:", err)
		}
		outFile.Close()

		uploadUrl := "http://localhost:8080/upload"
		fmt.Println("prepare upload file: ",filepath)
		response, err := uploadFile(uploadUrl, filepath)
		if err != nil {
			fmt.Println("Error uploading file:", err)
			return
		}
		fmt.Println("Response from server:", response)
	}

	// 检查扫描错误
	if err := scanner.Err(); err != nil {
		fmt.Println("Error reading file:", err)
	}
}

func uploadFile(url, filePath string) (string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filePath)
	if err != nil {
		return "", err
	}
	_, err = io.Copy(part, file)

	err = writer.Close()
	if err != nil {
		return "", err
	}

	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(respBody), nil
}
