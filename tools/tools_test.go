package tools

import (
	"encoding/json"
	"fmt"
	"hi_golang/controller/bugly"
	"strconv"
	"strings"
	"testing"
)

func TestTools(_ *testing.T) {
	var rawJson = `{
		"eventType": "bugly_crash_trend",
		"timestamp": 1462780713515,
		"isEncrypt": 0,
		"eventContent": {
		  "datas": [
			{
			  "accessUser": 12972,
			  "crashCount": 21,
			  "crashUser": 20,
			  "version": "1.2.3",
			  "url": "http://bugly.qq.com/realtime?app=1104512706&pid=1&ptag=1005-10003&vers=0.0.0.12.12&time=last_7_day&tab=crash"
			},
			{
			  "accessUser": 15019,
			  "crashCount": 66,
			  "crashUser": 64,
			  "version": "1.2.4",
			  "url": "http://bugly.qq.com/realtime?app=1104512706&pid=1&ptag=1005-10003&vers=0.0.0.12.12&time=last_7_day&tab=crash"
			},
			{
			  "accessUser": 15120,
			  "crashCount": 1430,
			  "crashUser": 1423,
			  "version": "1.2.4",
			  "url": "http://bugly.qq.com/realtime?app=1104512706&pid=1&ptag=1005-10003&vers=0.0.0.12.12&time=last_7_day&tab=crash"
			}
		  ],
		  "appId": "1104512706",
		  "platformId": 1,
		  "date": "20160508",
	  "appUrl":"http://bugly.qq.com/issueIndex?app=1104512706&pid=1&ptag=1005-10000"
		},
		"signature": "ACE346A4AE13A23A52A0D0D19350B466AF51728A"
	  }`
	// var a = new(bugly.CurlEntity)
	//m, ok := gjson.Parse(rawJson).Value().(map[string]interface{})

	var entity = new(bugly.BuglyWebhookRequestEntity)
	if err := json.Unmarshal([]byte(rawJson), &entity); err != nil {
		fmt.Println("err: " + err.Error())
	}
	var sb strings.Builder
	for _, data := range entity.EventContent.Datas {
		rate := float32(data.CrashUser) / float32(data.AccessUser)

		sb.WriteString(fmt.Sprintf(
			"版本号[%[1]s] 崩溃率: %[2]v%% \n", data.Version, strconv.FormatFloat(float64(rate*100), 'f', 2, 32)))
	}
	fmt.Println(sb.String())
}
