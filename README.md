# hi_golang #

一些简单的工具脚本

## csv2i18n ##

    generate i18n strings.xml via *.csv file
        cmd: go run Csv2IosI18n.go -v <version_code> -abbrs <country_short_name> -path <file_path>
    usage:
        go run Csv2IosI18n.go -v 1260 -abbrs zh,en,id -path "/Users/taohui/DevStation/hi_golang/hi_golang/tools/csv2i18n/csv/LamiV1.2.6.csv"

## csv file standard ##
     _______________________________
    | cn      | en      | in        |
    | ------- | ------- | --------- |
    | chinese | English | Indonesia |
     -------------------------------

## xml2binding ##

    insert <layout></layout> tag to target *.xml# hi_golang

## i18n2Csv ##

    merge diff strings.xml to *.csv

    https://tooltt.com/crontab/c/34.html


    # Light-Chat Project Structure #

adb shell ps -T | grep '16703' | grep 'unity'
adb shell ps -T -p 28145 | grep Log

Bugly符号表上传 MacOS 

```bash
/usr/local/opt/openjdk@8/bin/java -jar ~/DevStation/hi_android/Tools/buglyqq-upload-symbol/buglyqq-upload-symbol.jar \
-appid c229ac8bc0 -appkey a837b893-71e6-43e0-90be-9243e761f1f5 \
-bundleid com.tongdaxing.erban \
-version 1.9.4.2 \
-platform Android \
-inputSymbol ~/DevStation/hi_android/oe-android-ndk/ndkdevlib_halo/build/intermediates/jniLibs/release \
-inputMapping ~/DevStation/hi_android/oe-android/erban_client/build/outputs/mapping/googleRelease/mapping.txt
```

```bash
/usr/local/opt/openjdk@8/bin/java -jar ~/DevStation/hi_android/Tools/buglyqq-upload-symbol/buglyqq-upload-symbol.jar \
-appid c229ac8bc0  \
-appkey a837b893-71e6-43e0-90be-9243e761f1f5 \
-bundleid com.tongdaxing.erban \
-version 1.9.2.2 \
-platform Android \
-inputSymbol ~/DevStation/hi_android/oe-android-ndk/ndkdevlib_halo/build/intermediates/jniLibs/release \
-inputMapping ~/DevStation/hi_android/oe-android-bak/oe-android/erban_client/build/outputs/mapping/googleRelease/mapping.txt
```

```bash
/usr/local/opt/openjdk@8/bin/java -jar ~/Downloads/pepk.jar --keystore ~/Downloads/yinyuan.jks --alias yinyuan --output=sign.zip --encryptionkey=034200041E224EE22B45D19B23DB91BA9F52DE0A06513E03A5821409B34976FDEED6E0A47DBA48CC249DD93734A6C5D9A0F43461F9E140F278A5D2860846C2CF5D2C3C02 --include-cert
/usr/local/opt/openjdk@8/bin/java -jar ~/Downloads/pepk.jar --keystore ~/Downloads/yinyuan.jks --alias yinyuan --output=./ --encryptionkey=034200041E224EE22B45D19B23DB91BA9F52DE0A06513E03A5821409B34976FDEED6E0A47DBA48CC249DD93734A6C5D9A0F43461F9E140F278A5D2860846C2CF5D2C3C02 --include-cert
```

## core-third-sdk ##

* bigo-sdk
  https://github.com/AestronCloud/bigosdk-example-android
* trtc-sdk
  https://liteav.sdk.qcloud.com/doc/api/zh-cn/index.html
* agora-sdk
  https://docs.agora.io/cn/Voice/API%20Reference/java/index.html
* login-sdk facebook google phone

## network-flow ##

项目基于OK HTTP/Rx Kotlin/Retrofit

* server-api: 请自行查看RxNet/OkhttpManager
    * 现有的okhttp刨除picasso的okhttp-client, download-flow/api-flow都统一使用一个实例, 因为拦截器会影响到download-flow,
      所以download-flow的出口请求时header中会添加HEADER_DOWNLOAD_OSS_FILE用来强制忽略所有的拦截器
* glide-module
    *
  glide的配置用的是nim-kit中的 [GlideModule.java](erban_core/src/main/java/com/tongdaxing/xchat_core/GlideModule.java)
    * 针对七牛云和阿里云两种不同的oss服务, 对出口的图片地址进行了统一的webp格式转化
* download-feature: 下载服务由于之前没有关联到retrofit, 所有很多都是基于OK HTTP的即时实例进行下载

## custom-view-flow ##

每一个自定义控件正常都具有自己的UIModel一方面方便迁移到ViewModel, 一方面方便项目迁移/业务自己的逻辑处理, 具体的自定义控件内部只关注 控件的UIModel字段给出的具体流程和逻辑,
不应该直接嵌入业务到具体的控件内部实现

## tips ##

<li> 推送消息尽量走自定义通知, 云信服务的数据和自有服务需要考虑同步的问题, 会出现差异</li>
<li> keep.xml 因为有资源混淆的开启, 所有需要通过反射获取资源的代码请把资源名酌情考虑添加到各类keep相关的配置文件里 </li>
<li> 分享相关的功能如果涉及到deeplink需要考虑版本兼容问题 </li>
<li> TODO </li>


编码规范
资源命名
drawable以及layout资源统一用ic_/bg_/shape_/ripple_/anim_/activity_/view_等这类方式开头命名

1. 网络请求的签名加密迁移到异步处理中
2. 房间内事件线的分类处理
3. 房间麦位定向刷新
4. 高频事件流背压策略

## linux sdkmanager

https://developer.android.com/studio/releases/cmdline-tools?hl=zh-cn

sdkmanager "system-images;android-27;google_apis_playstore;x86"
platforms;android-35

support java8
JAVA_HOME=/usr/local/jdk1.8.0_161
```bash
sdkmanager --sdk_root=${ANDROID_HOME} "tools"
android-sdk-linux/tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null
sdkmanager --sdk_root=${ANDROID_HOME} "tools"
which sdkmanager
./sdkmanager --version
./sdkmanager --licenses
sdkmanager --install "cmdline-tools;5.0
sdkmanager "platform-tools" "platforms;android-35"
sdkmanager --install "cmdline-tools;5.0"
sdkmanager --install "cmdline-tools;latest"
sdkmanager --sdk_root
sdkmanager build-tools;23.0.1
sdkmanager --install "build-tools;35.0.0"

sdkmanager "platform-tools" "platforms;android-26"
sdkmanager 'cmdline-tools;latest' --channel=1
which sdkmanager
cd /data/soft/android-sdk/android-sdk-linux/tools/bin/sdkmanager
sdkmanager "system-images;android-27;google_apis_playstore;x86"

sdkmanager "platform-tools" "platforms;android-26"
yes | android-sdk-linux/tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} --licenses
 - android-sdk-linux/tools/bin/sdkmanager --sdk_root=${ANDROID_HOME} "platform-tools" "platforms;android-${ANDROID_COMPILE_SDK}" >/dev/null

sdkmanager --sdk_root=${ANDROID_HOME} "tools"
sdkmanager --uninstall "system-images;android-27;google_apis_playstore;x86"
sdkmanager "system-images;android-27;google_apis_playstore;x86"
sdkmanager 'cmdline-tools;latest' --channel=3
sdkmanager "system-images;android-31;google_apis_playstore;x86"
sdkmanager "system-images;android-31;google_apis_playstore;x86_6"
sdkmanager "system-images;android-31;google_apis_playstore;x86_64"

sdkmanager --install "ndk;16.1.4479499" --channel=0
sdkmanager --install "cmake;10.24988404"
```
* sdkmanager-env
java@1.8

* andorid-ndk-env
gradle java@11.0
ndk android-ndk-r16b 16.1.4479499
sdkmanager --install "cmake;3.6.4111459"


## 模块化迭代

https://firebase.google.com/docs/perf-mon/get-started-android?hl=zh-cn

qshell 是利用七牛文档上公开的API实现的一个方便开发者测试和使用七牛API服务的命令行工具。
https://developer.qiniu.com/kodo/1302/qshell
```bash
mac@MacBook-Pro Downloads % ./qshell account
Name: taohui
AccessKey: 77QyTeGDcCesh5He9vr0A23O5QRCXaC_-IUGxAlt
SecretKey: -012u5i-MYZ6FseMSSACrnZoJMlEjUoVZgWnVMzu

lightchat
./qshell account -- 77QyTeGDcCesh5He9vr0A23O5QRCXaC_-IUGxAlt -012u5i-MYZ6FseMSSACrnZoJMlEjUoVZgWnVMzu taohui
lami
./qshell account -- 77QyTeGDcCesh5He9vr0A23O5QRCXaC_-IUGxAlt -012u5i-MYZ6FseMSSACrnZoJMlEjUoVZgWnVMzu taohui

./qshell listbucket2 --min-file-size 8000000 --readable --mimetypes application/octet-stream --output-fields-sep ',' oeapp
./qshell listbucket2 --min-file-size 8000000 --readable --mimetypes application/octet-stream --output-fields-sep ',' applami

Key	FileSize	Hash	PutTime	MimeType	StorageType	EndUser
lhcgqYhhT0DvUOKLzHxPPPNMrM_g	57.02MB	lhcgqYhhT0DvUOKLzHxPPPNMrM_g	16394008747091557	application/octet-stream	0
lhoMPFvq3PwKE_PegE91ZTxiIf7b	23.58MB	lhoMPFvq3PwKE_PegE91ZTxiIf7b	16487098876337150	application/octet-stream	0
lhpFkCUuxtXKYdvwqreuZgGsJ4G8	15.34MB	lhpFkCUuxtXKYdvwqreuZgGsJ4G8	16393987302789387	application/octet-stream	0
loWK4hOJKVkERj7R9_rCU4hBW0mi	15.72MB	loWK4hOJKVkERj7R9_rCU4hBW0mi	16472451408140594	application/octet-stream	0
[I]  list success, remove cache status: /Users/mac/.qshell/users/taohui/listbucket2/3f3f5591e0e3bd745755709119e2989e/info.json

-------------------------------------------------------APPLAMI-------------------------------------------------------
Key,FileSize,Hash,PutTime,MimeType,StorageType,EndUser
lhcXt_jn2G3FX68uDmXp5HAhNfxZ,4.90MB,lhcXt_jn2G3FX68uDmXp5HAhNfxZ,16655580986006166,application/octet-stream,0,
li5_m-PV5NV7h_fboanliv8fA-TT,9.51MB,li5_m-PV5NV7h_fboanliv8fA-TT,16415236687738589,application/octet-stream,0,
liF8tBFpuiG_pUrsvBZscAivbbpa,8.57MB,liF8tBFpuiG_pUrsvBZscAivbbpa,16575911548122664,application/octet-stream,0,
lmbcR6Omm-Qqt-aa-qifIC3IH-In,8.80MB,lmbcR6Omm-Qqt-aa-qifIC3IH-In,16576446855101058,application/octet-stream,0,
-------------------------------------------------------END-------------------------------------------------------
```

现有问题
日志自动上报
多进程webview和ipc框架的方案
麦位刷新模块化拆分
上下游消息的高频计算逻辑迁移
koom的线上监控还是要有
七牛动效资源排查
高内存占用特效文件识别与处理流程

过度绘制的检测 卡顿掉帧
profile异常的场景监控和分析
线下环境关闭异常的静默模式
线上大房间测试

1. 后台根据礼物/头饰/座驾/勋章区分场景
2. 下架大于8M的礼物特效文件
3. 对于头饰的上传以及oss上的文件排除大于4m的
4. 勋章特效文件不能大于3m 

## bugly-symbol-tool-env
java@1.8

### android-project-env
gradle 7.0.2 
org.gradle.java.home=/usr/local/opt/openjdk@11

项目本地配置文件 package_cfg.json
```json
{
"app_version": 1880,
//desKey 每个版本的版本key
"des_key": "57688289343c1db5e1a0a251ba544f74e3",
//合法的APP包名
static const char *app_packageName_halo = "com.halo.mobile";
//合法的hashcode keystore文件的hash值
"app_signature_hash_code_halo": "-92214222"
"app_signature_hash_code_halo_local" : 1680080975
}
```


### release-notes
  face新增表情包加入click_dynamic_character_face埋点上报


[this is a#\n link](http://name.com)

ls | xargs -I {} aapt s -i {} -o ./{}2 

ndroid14的新权限：READ_MEDIA_VISUAL_USER_SELECTED当用户授权了此权限以后，READ_MEDIA_IMAGES和READ_MEDIA_VIDEO将会被默认拒绝。并且此时App将会永久获得用户可选的临时媒体文件访问权限。并且随后用户还能多次请求READ_MEDIA_IMAGES和READ_MEDIA_VIDEO来获得其他媒体文件的临时访问权限。



在Android中，`getString`和`getDrawable`这两个方法都是线程安全的，你可以在多线程环境下并发地使用它们来获取资源，而不必担心会出现并发问题。这是因为这些方法内部的实现已经考虑到了线程安全，它们并不会改变任何共享的状态或数据。
然而，需要注意的是，尽管这些方法本身是线程安全的，但是在多线程环境下使用它们时，仍然需要考虑到其他的线程安全问题。例如，如果你在一个线程中获取了一个Drawable对象，然后在另一个线程中修改这个对象，那么可能会出现并发问题。因此，在这种情况下，你需要自己确保线程安全。
另外，`getDrawable`方法返回的`Drawable`对象并不是共享的，每次调用这个方法都会创建一个新的`Drawable`对象。因此，即使在多线程环境下并发地调用这个方法，也不会出现并发问题。
总的来说，你可以在多线程环境下并发地使用`getString`和`getDrawable`方法来获取资源，但是在处理获取到的资源时，仍然需要考虑到线程安全问题。



`System.exit(0)`和`System.exit(9)`的主要区别在于它们的退出状态代码。
- `System.exit(0)`：这个方法会导致Java虚拟机正常终止，参数“0”是一个状态代码，它会传递给操作系统以表示该程序的退出状态。在这种情况下，状态代码为0表示程序执行成功，没有错误[1]。
- `System.exit(9)`：这个方法同样会导致Java虚拟机正常终止，但参数“9”是一个不同的状态代码。非零状态代码通常表示异常或错误的退出。所以，如果你的程序在执行过程中出现了错误或异常，并且你希望向操作系统报告这个错误，那么你可以使用`System.exit(9)`来终止程序[2]。
总的来说，`System.exit(0)`和`System.exit(9)`都会导致Java虚拟机正常终止，但它们的状态代码不同，分别表示程序执行成功和程序出现错误或异常。


在Android中，你有一些不同的选项可以重启应用，下面是几种常用的方法：

1. 使用 `FLAG_ACTIVITY_CLEAR_TOP` 标志。你可以创建一个新的Intent来启动你的应用的主Activity，并添加 `FLAG_ACTIVITY_CLEAR_TOP` 标志。这将清除在主Activity之上的所有Activity，并重新启动主Activity[1]。

```java
new Handler().postDelayed(new Runnable() {
    @Override
    public void run() {
        Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage(getApplication().getPackageName());
        LaunchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(LaunchIntent);
    }
}, 1000);
```

2. 使用 `ActivityManager`。你可以获取 `ActivityManager` 服务，并调用其 `restartPackage` 方法来重启你的应用[2]。

```java
ActivityManager manager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
manager.restartPackage("com.example.test");
```

3. 重新启动当前 Activity。你可以创建一个新的Intent来启动当前的Activity[3]。
```
Intent intent = new Intent(getApplicationContext(), MainActivity.class);
int mPendingIntentId = 123456;
PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId,    intent, PendingIntent.FLAG_CANCEL_CURRENT);
AlarmManager mgr = (AlarmManager)getApplicationContext().getSystemService(Context.ALARM_SERVICE);
mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
System.exit(0);
```

请注意，这些方法都会导致你的应用的当前状态丢失，因此在重启应用之前，你可能需要保存任何重要的用户数据或应用状态。
`Process.killProcess(Process.myPid())`和`System.exit(9)`这两个方法在Android系统中都可以用来结束当前进程，但它们的行为方式和结果有一些区别。
- `Process.killProcess(Process.myPid())`：该方法会结束包括所有活动在内的整个进程。它不会返回到先前的活动，而是结束在该进程中启动的所有活动。如果你不希望结束所有已启动的活动，那么就不应该使用这个方法[2]。
- `System.exit(9)`：这个方法会导致Java虚拟机正常终止，参数“9”是一个状态代码，它会传递给操作系统以表示该程序的退出状态。在这种情况下，非零状态代码通常表示异常或错误的退出[4]。
总的来说，两者的主要区别在于`Process.killProcess(Process.myPid())`会杀死所有活动，而`System.exit(9)`则会正常终止Java虚拟机并向操作系统返回一个状态代码。

`UnsupportedOperationException`是Java中的一个运行时异常，表示请求的操作不支持。

在你提供的堆栈跟踪中，`UnsupportedOperationException`是在尝试从`CopyOnWriteArrayList`的迭代器中删除元素时抛出的。`CopyOnWriteArrayList`是Java并发包中的一个线程安全的类，它的特点是每次修改操作（add、set、remove等）都会复制一份新的数据，以此来保证并发环境下的数据一致性。但是，为了保证数据的一致性和线程安全，`CopyOnWriteArrayList`的迭代器不支持`remove`操作。

因此，如果你需要在遍历的过程中修改列表，你需要使用其他方法，比如使用标准的for循环进行遍历和修改。或者，如果你需要在并发环境下进行修改，你可能需要使用其他线程安全的数据结构，如`ConcurrentLinkedQueue`等。


是的，`ConcurrentLinkedQueue`是设计为支持并发操作的。在Java中，`ConcurrentLinkedQueue`是一个适合于多线程共享访问的集合类。它是线程安全的，你可以在多个线程中同时进行添加和删除操作，而不需要进行额外的同步或使用锁。

然而，需要注意的是，`ConcurrentLinkedQueue`不允许使用null元素。尝试添加null元素将会抛出NullPointerException。此外，由于`ConcurrentLinkedQueue`的并发性质，任何时刻获取的队列状态都可能由于其他线程的操作而立即变得过时。因此，像`size()`和`isEmpty()`这样的方法返回的结果可能并不准确。

总的来说，如果你在多线程环境中需要一个支持并发操作的队列，`ConcurrentLinkedQueue`是一个很好的选择。[2] [3]


`ConcurrentLinkedQueue`并不会自动删除重复的元素。它是一个基于链表的无界非阻塞队列，遵循先进先出（FIFO）的规则。当你添加一个元素时，它会被添加到队列的末尾。当你获取一个元素时，它会返回队列头部的元素[4]。

如果你需要从`ConcurrentLinkedQueue`中删除特定的元素，你可以使用`remove(Object o)`方法，这个方法会移除队列中指定元素的单个实例（如果存在）[1]。另外，`ConcurrentLinkedQueue`还提供了`removeIf(Predicate<? super E> filter)`方法，这个方法可以删除队列中满足给定谓词过滤器的所有元素[2]。

但是，如果你需要一个不包含重复元素的队列，你可能需要使用`java.util.concurrent.ConcurrentSkipListSet`或者其他适合你需求的数据结构。


`ConcurrentSkipListSet`是Java并发包中的一个类，它是线程安全的，适用于在多线程环境下进行操作。这个类提供的功能类似于`TreeSet`，能够并发的访问有序的集合。因为`ConcurrentSkipListSet`是基于“跳跃列表（skip list）”实现的，只要多个线程没有同时修改集合的同一个部分，那么在正常读、写集合的操作中不会出现竞争现象[4]。

关于是否适用于Android，`ConcurrentSkipListSet`是Java标准库的一部分，因此在Android中同样可以使用。但是，需要注意的是，由于Android设备的资源限制，如果你的应用需要处理大量数据或者频繁进行并发操作，可能需要考虑其他更高效的解决方案。

至于是否会导致主线程阻塞，由于`ConcurrentSkipListSet`是非阻塞的，所以在正常情况下不会导致主线程阻塞。但是，如果你在主线程中进行了大量的操作或者长时间的操作，可能会导致主线程阻塞。因此，你应该尽量避免在主线程中进行耗时的操作，而应该将这些操作放在后台线程中进行。


在Android中，并没有特别为Android设计的数据结构来处理并发的插入、删除和读取操作。你可以使用Java并发包（`java.util.concurrent`）中提供的一些线程安全的集合类，例如`ConcurrentHashMap`，`CopyOnWriteArrayList`，`ConcurrentLinkedQueue`，`ConcurrentSkipListSet`等。

这些集合类都能够在多线程环境下提供良好的性能。但是，你需要根据你的具体需求来选择最合适的集合类。例如，如果你需要一个支持快速随机访问的线程安全列表，你可以选择`CopyOnWriteArrayList`；如果你需要一个支持快速插入和删除操作的线程安全队列，你可以选择`ConcurrentLinkedQueue`；如果你需要一个不包含重复元素的线程安全集合，你可以选择`ConcurrentSkipListSet`。

总的来说，虽然Android没有提供专门的并发集合类，但是Java并发包中提供的集合类已经足够满足大部分需求。在使用这些集合类时，你需要注意正确地管理线程和避免在主线程中进行耗时的操作，以保证应用的性能和响应速度。


这可能是因为OkHttp的重试机制导致的。

OkHttp有一个自动重试机制，当某个请求在一次尝试后失败时，会自动进行重试。这种重试通常发生在以下几种情况：

1. 连接到服务器时发生了错误。
2. 服务器无法成功处理请求（例如，服务器返回了HTTP 500错误）。

在弱网环境下，连接服务器可能会更加困难，因此OkHttp可能会尝试多次连接来确保请求被正确发送。这就可能导致看起来像是发送了两次请求。

需要注意的是，OkHttp的这种行为通常是透明的，也就是说，即使发生了重试，你的代码通常也只会收到一次响应。如果你观察到两次响应，那可能是因为你的代码中存在一些问题，例如可能在网络连接恢复后重新发送了请求。

如果你希望在弱网环境下禁用自动重试，可以通过设置OkHttpClient的retryOnConnectionFailure属性为false来实现。但需要注意的是，这可能会导致在网络连接不稳定时请求失败。

参考：
1. [Why does OkHttp perform two requests when network is slow?](https://stackoverflow.com/questions/38547612/why-does-okhttp-perform-two-requests-when-network-is-slow)
2. [OkHttp retryOnConnectionFailure](https://square.github.io/okhttp/4.x/okhttp/okhttp3/-ok-http-client/-builder/retry-on-connection-failure/)
   即使你设置了 `retryOnConnectionFailure` 为 `false`，仍然可能出现重复请求的情况。这可能是由于以下几个原因：

1. **自动重定向**：默认情况下，OkHttp会自动处理HTTP重定向（3xx响应）。如果服务器返回了一个重定向响应，那么OkHttp会自动发起一个新的请求到重定向的URL。你可以通过调用 `followRedirects(false)` 来禁止这种行为。

2. **身份验证**：如果服务器返回了一个需要身份验证的响应（例如HTTP 401或HTTP 407），那么OkHttp会自动尝试重新发送请求，这次请求将包含适当的身份验证头。你可以通过实现一个自定义的 `Authenticator` 来控制这种行为。

3. **重试和连接池**：即使 `retryOnConnectionFailure` 被设置为 `false`，OkHttp仍然可能会在其他情况下进行重试。例如，如果连接被服务器关闭，或者如果连接在空闲时被丢弃，那么OkHttp会尝试从连接池中获取一个新的连接，然后重新发送请求。

4. **应用程序级别的重试**：在你的应用程序代码中，可能存在一些逻辑会在请求失败后重新发送请求。例如，你可能有一个网络请求的封装类或者使用了某个库，这个类或者库在网络请求失败后会自动进行重试。

因此，如果你发现即使设置了 `retryOnConnectionFailure` 为 `false` 仍然出现重复请求的情况，你可能需要检查以上提到的其他可能的原因。

参考：
1. [Why does OkHttp perform duplicate requests?](https://stackoverflow.com/questions/38547612/why-does-okhttp-perform-two-requests-when-network-is-slow)
2. [OkHttp Documentation](https://square.github.io/okhttp/)


RxJava中的`Scheduler`主要用于控制线程，决定代码在哪个线程运行。在不指定线程的情况下，RxJava遵循的是线程不变的原则，即：在哪个线程调用 `subscribe()`，就在哪个线程生产事件；在哪个线程生产事件，就在哪个线程消费事件。如果需要切换线程，就需要用到 `Scheduler`[3][4]。

RxJava已经内置了几个`Scheduler`，它们已经适合大多数的使用场景[4]：
1. `Schedulers.immediate()`: 直接在当前线程运行，相当于不指定线程。
2. `Schedulers.newThread()`: 总是启动新线程。
3. `Schedulers.io()`: 用于I/O操作的调度器，如读写文件、数据库、网络等，内部维护了一个线程池，会复用空闲的线程，对于上限的线程数没有限制。
4. `Schedulers.computation()`: 用于计算任务的调度器，如CPU密集型计算，内部维护了一个线程池，线程数默认等于处理器的数量。
5. `AndroidSchedulers.mainThread()`: Android专用，在主线程运行。

根据不同的场景使用不同的调度策略确实可以提高性能。例如，对于I/O操作和计算任务，我们应该使用不同的调度器。I/O操作通常涉及等待，因此我们可以在等待期间运行其他任务，所以`Schedulers.io()`没有对线程数进行上限限制。而计算任务通常是CPU密集型的，如果启动的线程数过多，会导致线程上下文切换过多，反而降低性能，所以`Schedulers.computation()`的线程数默认等于处理器的数量[2]。

在Android中，保证集合读写同步的方式有多种。选择哪种方案取决于你的具体需求和场景。

1. **CopyOnWriteArrayList 或 ConcurrentHashMap**：对于读多写少的并发场景，可以使用CopyOnWriteArrayList或ConcurrentHashMap，它们是线程安全的并发容器，使用写时复制策略来保证列表的一致性。在修改增删改的过程中使用了独占锁，保证同时只有一个线程能够对列表数组进行修改。[1]
2. **Synchronized Collections**：Java 提供了一些同步的集合类，如 `Collections.synchronizedList(new ArrayList<T>())`。这些集合类在每个方法上都添加了同步块，因此它们在多线程环境中是安全的。然而，你需要注意的是，尽管单个方法是线程安全的，但复合操作（如迭代、跳转和条件运算）必须在用户控制的同步块中进行以防止并发修改。
3. **使用锁**：如果你需要更精细的控制，你可以使用锁来同步对集合的访问。例如，`ReentrantLock` 和 `ReentrantReadWriteLock` 可以提供更高级别的并发控制。这样可以允许多个读者和单个写者同时访问集合。
4. **使用线程安全的集合**：Java 并发库提供了一些线程安全的集合，如 `ConcurrentHashMap`、`ConcurrentSkipListMap` 和 `ConcurrentLinkedQueue` 等，这些集合在内部处理了所有并发问题，可以直接在多线程环境中使用。

总的来说，选择哪种方案取决于你的具体需求。如果你的应用主要是读操作，那么 `CopyOnWriteArrayList` 或 `ConcurrentHashMap` 可能是最好的选择。如果你需要更精细的控制，那么使用锁可能是更好的选择。如果你需要处理大量的写操作，那么使用 `Collections.synchronizedList()` 或其他线程安全的集合可能是最好的选择。[2]

```java
import java.io.*;
import java.security.*;

public class CheckFileIntegrity {
    public static String checkMD5(String filePath) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            FileInputStream fis = new FileInputStream(filePath);
            byte[] dataBytes = new byte[1024];
 
            int nread = 0; 
            while ((nread = fis.read(dataBytes)) != -1) {
              md.update(dataBytes, 0, nread);
            };
            byte[] mdbytes = md.digest();
 
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
            }
 
            fis.close();
 
            return sb.toString();
        } 
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
```
在这个例子中，`checkMD5`函数会为指定路径的文件生成一个MD5哈希值。如果文件没有发生改变，那么每次调用这个函数都会返回相同的哈希值。如果文件发生了改变，那么返回的哈希值也会发生改变。
请注意，虽然哈希函数可以用来验证文件的完整性，但它们并不能防止恶意篡改。如果有人故意修改了文件，并且他们知道你正在使用哪种哈希函数，那么他们可能能够生成一个新的文件，这个文件有着相同的哈希值，但是内容却完全不同。因此，在处理敏感信息时，请确保使用适当的安全措施。

```java
public class AvRoomDataManager {
    public void removeVipGuestMemberV1(long account) {
        if (mRoomVipGuestList == null || account == 0) {
            return;
        }
        mRoomVipGuestList.remove(account);
    }

    public void removeVipGuestMemberV2(long account) {
        if (mRoomVipGuestList == null || account == 0) {
            return;
        }
        mRoomVipGuestList.remove(account);
        ListIterator<Long> iterator = mRoomVipGuestList.listIterator();
        while (iterator.hasNext()) {
            long ac = iterator.next();
            if (account == ac) {
                iterator.remove();
                break;
            }
        }
    }
}
```

在Android编程中，`getDimension`和`getDimensionPixelOffset`两个方法都可以用来获取尺寸，但它们之间有一些细微的差别。

`getDimension`方法返回一个浮点数，这个数值是根据当前显示的度量进行调整的。这个方法将dp或sp值转换为基于当前屏幕密度的像素，这在你想要在Java中设置宽度或文本大小（它们只接受像素）时非常有用。但是，如果你需要原始的sp或dp，你可以进行"逆向工程"[2]。

而`getDimensionPixelOffset`方法返回的是一个整数。它与`getDimension`方法类似，但是会将结果四舍五入为整数，确保任何非零尺寸至少为一个像素大小。对于整数尺寸值，这两个方法返回的数字是相同的[1]。

因此，在设置控件的高度时，如果你需要一个更精确的值（可能包含小数部分），那么应该使用`getDimension`。如果你只关心像素值，并且不需要关心小数部分，那么可以使用`getDimensionPixelOffset`。

`getDimensionPixelSize`方法是从资源中获取尺寸，返回值是像素值的整数。这个方法会将浮点数四舍五入到最接近的整数。如果你在处理需要整数像素值的情况（例如设置控件的宽度或高度），那么这个方法就非常适用。

与`getDimensionPixelOffset`相比，`getDimensionPixelSize`在处理小数时的表现稍有不同。如果你有一个1.4dp的尺寸，`getDimensionPixelOffset`会返回1，而`getDimensionPixelSize`会返回2，因为它会四舍五入到最接近的整数。所以，如果你需要更精确地将尺寸转换为像素，那么你应该使用`getDimensionPixelSize`。

在设置控件的高度时，如果你需要一个整数像素值并且想要更精确地四舍五入，那么你应该使用`getDimensionPixelSize`。

sudo spctl --master-disable
sudo xattr -cr xxx



在Android中使用`systrace`来分析所有函数的运行时间，你需要按照以下步骤操作：

1. **启动Systrace**：
   首先，你需要启动Systrace工具。Systrace是Android性能分析的主要工具，它实际上是围绕其他工具的一个包装器[1]。你可以通过Android SDK提供的`systrace.py`脚本来启动它，或者在Android Studio中使用。

2. **命令行捕获系统跟踪**：
   使用命令行来调用Systrace工具，这可以让你在系统级别上收集并检查所有在你的设备上运行的进程的时序信息[2]。具体命令如下：
   ```bash
   python systrace.py --time=10 -o my_systrace.html sched gfx view -a my.app.package
   ```
   这里`--time=10`指定了要跟踪的时间长度（秒），`-o my_systrace.html`指定了输出文件，而`sched gfx view`是跟踪的类别，`-a my.app.package`用来指定你的应用包名。

3. **分析应用执行情况**：
   运行Systrace可以帮助你捕获并显示应用进程和其他Android系统进程的执行时间[3]。通过生成的报告，可以在一个共同的时间线上看到系统和应用进程的执行情况，从而分析你的应用性能。

4. **查看详细报告**：
   Systrace工具允许你从运行Android 4.1及更高版本的设备上生成高度详细、互动式的报告[4]。通过这些报告，你可以详细查看每个函数的运行时间和系统的其他活动。

请注意，如果你想要分析特定函数的运行时间，可能需要结合使用Traceview和dmtracedump等工具进行更深入的分析。Systrace提供了一个宏观的性能视图，而不是特定于单个函数的详细分析。


adb shell setprop persist.traced.enable 1

curl -O https://raw.githubusercontent.com/google/perfetto/master/tools/record_android_trace

chmod u+x record_android_trace 

./record_android_trace -o trace_file.perfetto-trace -t 10s -b 32mb \               
sched freq idle am wm gfx view binder_driver hal dalvik camera input res memory

1. **敏捷开发（Agile Development）**：
    - 采用Scrum或Kanban这样的敏捷开发框架，进行短周期的迭代开发。
    - 小团队合作，每个团队负责不同的模块或功能点，保持团队之间的高度协作和沟通。
    - 定期举行站会，快速反馈进度和问题。

2. **模块化开发**：
    - 将大型项目拆分为多个模块或组件，每个模块可以独立开发、测试和维护。
    - 促进代码重用，减少冗余工作。

3. **持续集成/持续部署（CI/CD）**：
    - 建立自动化的构建和测试流程，确保代码质量。
    - 快速定位问题并修复，减少整体的开发周期。

4. **版本管理**：
    - 使用Git等版本控制系统，合理规划分支策略。
    - 确保代码变更的可追踪性和可管理性。

5. **项目管理工具**：
    - 使用JIRA、Trello、Asana等项目管理工具来跟踪任务进度。
    - 通过看板等可视化工具来管理任务和优先级。

6. **代码审查（Code Review）**：
    - 定期进行代码审查，保证代码质量和一致性。
    - 促进知识共享和技术交流。

7. **文档管理**：
    - 编写和维护详尽的技术文档和API文档。
    - 确保团队成员能快速理解项目和代码库。

8. **优先级排序**：
    - 根据业务需求和目标，合理安排项目的优先级。
    - 避免资源浪费，在关键功能和关键时期投入更多的资源。

9. **人力资源管理**：
    - 根据项目需求和团队成员的技能进行合理分配。
    - 提供培训和学习机会，提升团队整体能力。

10. **沟通协调**：
    - 加强团队内部以及与其他部门之间的沟通。
    - 及时解决跨部门协作中可能出现的问题。

通过上述方法结合公司实际情况进行调整和优化，Android组可以有效地管理众多项目，并提高开发效率。

tcp.port==3016 && ip.addr == 192.168.3.2 && tcp.len > 200 && websocket



09:([4-5])([6-9])

1. **出勤率**：统计员工在一定时间周期内的实际出勤天数与应出勤天数的比例，出勤率低可能意味着频繁缺勤或请假。

2. **迟到早退次数**：追踪员工迟到和早退的频率。频繁的迟到和早退可能表明员工对工作的不负责任态度。

3. **加班记录**：分析员工的加班时间和频率，可以反映员工的工作负荷和工作态度。

4. **异常打卡记录**：如打卡时间异常波动、位置异常等，可能表明员工有代打卡或其他不诚信行为。

5. **请假记录**：员工的请假类型、频率及请假时长等，过多的病假或事假可能影响工作效率。

6. **规律性**：分析员工打卡记录的规律性，例如是否有固定的工作和休息模式，规律性强可能意味着较高的工作稳定性。

7. **综合工作表现**：结合实际工作表现和成果来综合评估。考勤记录良好但工作成果不佳，或许意味着虽然出勤但工作效率或质量不高。

8. **同期对比**：将员工的考勤数据与同期其他员工或团队平均水平进行对比，看是否存在显著差异。

9. **趋势分析**：长期跟踪员工的考勤记录，分析其变化趋势，以判断员工的稳定性及潜在问题。

10. **数据关联分析**：将考勤数据与业绩数据、客户反馈等其他数据进行关联分析，看是否存在某种相关性。

通过上述数据分析，结合具体工作内容和业绩目标，可以较为全面地评估员工的有效性。需要注意的是，单一的考勤数据不能完全代表员工的有效性，应该结合多方面因素综合评价。

# 开发组周报 - [日期]

周报内容,同时注明是
[fix]修复了什么什么问题/fix-tapd-buglist
[opt]优化了 xxx 模块的性能/优化了 xxx 模块的代码结构
[add]完成了哪些具体任务，如功能开发/完成了功能模块
[del]删除了那些模块的代码、流程、模块
[upd]更新了 xxx 框架版本
[adv]对新技术的调研情况，性能优化等/调研了 X 技术对现有项目的优化方案
[oth]1.0.0版本需求评审会议, 版本提测的具体时间
git格式化输出/或者自己抽取commit-log-list
git log --pretty="format:%s%cr"

## 本周完成工作
1. **项目名称** 
   - 完成了哪些具体任务，如功能开发、bug修复等。
   - 完成了功能模块 xxx 的开发
   - 修复了项目中的 xxx bug，提升了系统稳定性。
2. **技术研究**
   - 对新技术的调研情况，性能优化等。
   - 调研了 X 技术对现有项目的优化方案。
3. **其他工作** 
    - 其他需要汇报的工作。
    - 参加了 Y 项目的需求评审会议, 版本开始-提测-提审核的具体时间。

## 遇到的问题及解决方案
1. **问题描述**
   - 遇到的具体问题，如系统故障、需求变更等。
   - 在开发 xxx 功能时遇到瓶颈。
2. **解决方案** 
   - 针对问题采取的措施或解决方法。
   - 通过调整算法和优化数据库查询，解决了性能问题。

## 下周工作计划
1. **项目名称** - 下周计划完成的任务。
    - 计划完成 xxx 功能的开发和集成测试。
2. **学习与成长** - 计划学习的新知识或技能。
    - 学习关于新版 xxx 框架的高级特性。

## 需要支持和协助
- 描述需要团队或管理层支持和协助的事项。
    - 需要更多关于 V 系统环境配置的信息。

## 其他事项
- 任何其他需要汇报的事项。
    - 下周三下午参加 T 技术交流会议。

在使用 `super.dismiss()` 和 `super.dismissAllowingStateLoss()` 时，主要的区别在于它们如何处理与Fragment事务相关的状态保存。

`super.dismiss()` 会将DialogFragment从FragmentManager中移除，但在执行这个操作之前，它会检查是否可以安全地进行，即是否处于一个可以保存状态的时点。如果在执行 `dismiss()` 的时候Activity正在保存其状态（比如用户按下了Home键或者设备屏幕旋转），那么这个 `dismiss()` 调用可能会抛出一个IllegalStateException，因为状态已经保存完毕，此时再进行状态改变就不安全了。

另一方面，`super.dismissAllowingStateLoss()` 方法会忽略是否处于安全的状态保存时点，即使在Activity状态保存后它也会继续执行。因此，如果你的DialogFragment在dismiss时有大量的状态变化，而这些变化并没有随着Activity状态的保存而保存，使用 `dismissAllowingStateLoss()` 就不会因为状态丢失而抛出异常。

至于为什么使用 `super.dismiss()` 不会导致 `TransactionTooLargeException`，可能有以下几个原因：

1. 当你调用 `dismiss()` 而不是 `dismissAllowingStateLoss()` 时，可能恰好在允许状态保存的安全时点进行操作，因此没有大量的状态需要保存。
2. 使用 `dismiss()` 时，可能Activity的状态已经保存了，所以没有额外的大量数据需要在事务中传递。
3. 在某些情况下，系统可能有足够的资源来处理较大的事务，但这种情况不稳定，依赖于当前系统资源和事务时的数据量。

如果你想要避免 `TransactionTooLargeException` 异常，建议你：

- 减少DialogFragment中状态的大小。
- 避免在Fragment中传递大型数据结构。
- 在可能的情况下使用 `dismiss()` 而非 `dismissAllowingStateLoss()`，除非你确定在那个时刻Activity的状态不会被保存。

总之，`TransactionTooLargeException` 异常与传递给Bundle的数据量有关，而不是直接与使用 `dismiss()` 或 `dismissAllowingStateLoss()` 方法有关。当系统资源紧张或者Fragment事务包含大量数据时，这个异常就可能发生。

第二种方法使用`ArrayMap`并直接通过键值对检索来查找国家缩写，效率更高。

在第一种方法中，你需要遍历整个列表来查找匹配的国家缩写。这种线性搜索的时间复杂度是O(n)，其中n是列表中元素的数量。如果列表很长，这可能会变得相当耗时，特别是如果要查找的缩写位于列表的末尾，或者甚至不在列表中。

第二种方法利用了`ArrayMap`，它是一种优化的Map，专为Android设计，比标准的HashMap更节省内存。虽然`ArrayMap`的get操作在最坏情况下的时间复杂度也是O(n)，但在实际应用中，对于较小的数据集，其性能通常优于HashMap，并且如果缩写存在，它可以直接检索到对应的对象，不需要遍历整个集合。

因此，如果你关心内存优化并且数据集不是非常大，第二种方法通常是更好的选择。然而，如果你操作的数据集非常大，或者查询操作非常频繁，那么使用一个标准的HashMap可能会提供更快的查找性能，因为HashMap通常具有更快的平均查询时间（O(1)复杂度）。

`ArrayMap` 和 `HashMap` 在 Android 中有不同的使用场景和性能特点。

`ArrayMap` 是专为 Android 设计的，它在数据集较小（一般是几百项以下）时，内存使用上比 `HashMap` 更高效。这是因为 `ArrayMap` 内部使用两个数组来存储键和值，这种结构在元素较少时可以减少内存开销，因为它不需要像 `HashMap` 那样为每个插入的元素创建额外的 `Entry` 对象。然而，由于它使用线性查找来实现 `get` 和 `put` 方法（尽管它对键数组进行了排序以便使用二分查找），所以当数据集变得非常大时，其性能会下降。

具体到多大算是“非常大”，这取决于具体应用场景和性能要求。一般来说，如果你的数据集在几百项以下，`ArrayMap` 可能是一个更好的选择。如果你有成千上万的元素，`HashMap` 通常会有更好的性能。

`HashMap` 在 Java 中是一个非常常用的数据结构，它通过哈希表实现，理论上提供了常数时间的添加、查询和删除操作（即时间复杂度为 O(1)）。但是，这种性能是以额外的内存开销为代价的：每个插入的元素都需要一个额外的哈希表条目（`Entry` 对象），这在内存受限的设备上可能是个问题。

总结一下：

- **ArrayMap**：适合元素数量较少（几百项以下）的情况，节省内存但随着元素数量增加性能下降。
- **HashMap**：适合元素数量较多的情况，提供快速访问但使用更多内存。

选择哪种取决于你的具体需求：如果你优先考虑内存并且数据集不大，可以选择 `ArrayMap`；如果你优先考虑性能并且有较大的数据集，那么 `HashMap` 可能是更好的选择。

4.7‘’ wxga

网络运营商的DNS服务器中设置的TTL（Time to Live）值决定了DNS记录在缓存中保留的时间。TTL缓存可能导致以下问题：

1. **缓存刷新不受控**：当域名的IP地址发生变更时，如果TTL设置得较长，那么更新的信息需要较长时间才能传播到所有用户。在此期间，用户可能会被解析到旧的IP地址，从而无法访问新的服务器或服务，这可能导致服务中断或不可用的情况[2]。

2. **客户体验受影响**：由于TTL设置过长，导致域名变更后，用户在DNS缓存过期之前无法访问最新的服务，这会影响用户体验，并可能增加客户投诉[2]。

3. **解析权和缓存值被修改问题**：一些小运营商可能会将域名解析权转发给其他运营商，并可能修改缓存值为较长时间，这样做可能会导致域名解析的不稳定和不一致，进而影响服务的正常使用[2]。

4. **DNS劫持和污染**：如果运营商的DNS服务器存在安全问题，那么DNS劫持或污染的风险会增加。即使域名并未更改，攻击者也可能通过DNS缓存污染来引导用户访问恶意网站[3]。

为了避免这些问题，网站管理员和运营商需要合理设置TTL值，并确保在必要时能够快速更新DNS记录。对于重要变更，可能需要在变更前逐步降低TTL值，以便新的设置能够迅速生效。

作为Android开发者，要在客户端层面上优化或解决DNS服务器TTL缓存可能带来的问题，可以考虑以下几个策略：

1. **使用HTTPDNS**：
   - 可以采用HTTPDNS服务，这种服务通过HTTP协议直接请求DNS解析结果，绕过本地DNS解析的过程，可以减少因本地DNS缓存导致的问题。

2. **DNS预解析**：
   - 在应用启动或者在后台服务中，预先对关键域名进行DNS解析，并缓存结果。这样即使运营商的DNS信息更新不及时，应用也可以使用最新解析到的IP地址。

3. **动态配置DNS**：
   - 如果后端服务的IP地址经常变动，可以考虑实现一个动态配置系统。当IP地址变更时，通过这个系统及时通知客户端更新IP地址信息。

4. **备用IP地址或域名**：
   - 在应用中配置备用的IP地址或域名。当主要服务不可用时，可以快速切换到备用服务。

5. **重试机制**：
   - 在网络请求失败时，实现重试机制。重试前可以尝试清除本地DNS缓存或者重新进行DNS解析。

6. **监测网络状态**：
   - 监测网络状态变化，如Wi-Fi和移动网络之间的切换，在网络状态变化时重新进行DNS解析。

7. **用户反馈**：
   - 提供一个快速反馈渠道，当用户遇到网络访问问题时，可以报告问题。开发者可以根据反馈及时调整策略。

8. **日志记录**：
   - 记录详细的网络日志，包括DNS解析的结果、时间等信息，便于分析问题和快速响应。

9. **敏感操作确认**：
   - 对于涉及资金或重要数据操作的请求，可以在操作前后端双重确认IP地址的有效性，以防止因DNS问题导致的安全风险。

通过上述措施，虽然无法完全控制运营商的DNS服务器行为，但可以在一定程度上降低TTL缓存带来的影响，并提高应用的稳定性和用户体验。

解决方案：https://cloud.tencent.com/document/product/379

olamet-李章文
    ->gimme-潘穗龙
    ->hichat-泽丰
    ->yomet-TODO
lami 童寿洳
    ->yobi 黄振华
ohla
    ->olla
ligo-谢志颖
    ->fancy-TODO
haki-叶淼
    ->habby 庄秋果
bobbo 何泽辉
wego 李俊良



##### Bugly-Robot
- all apps 互乐安卓钉钉群组机器人
https://oapi.dingtalk.com/robot/send?access_token=943d423d9373a69a83c60e18df825584065c9f7d2c26cd1e60a9971da5e0b0e4

##### Jenkins-Robot

* LightChat-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=7f9b3e68ec89a746bb2ab398ac1ba1d7fa353ced267bb14367a9c2c72c7b1dd0
* Bobbo-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=6fe68129a28cbc6e14ff82d3f650b90020fe3844b5ec47431eb0934913ff3497
* Ditto-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=d6408c989cd5eae5762ef3b462121e16ef4ae2a327b9b0ca77612478a8709e22
* Fancy-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=719e366fb9afaccd7cb5d0876bd029ffb3108e6dfc3dac764632bf8a15e6e428
* Gimme-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=58d98fb64958dac67f3a44233fd48f56096f9d18f17201eeab63e16d3cd1cbaa
* Habby-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=c0a310ff00325f49b9bedd170ea12ee49882d9ced411ba4107d6a7dbb9cc5902
* Haki-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=06d416caa1e012851f1043aac2b0e973389ed1f2ed35e0a77ef56398c885dc3a
* Kiyo-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=cd6ae92347a2a6e640d8de0b633ab6ad8d79535dda986359242e01da83f1b240
* Lami-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=58aa620e358956201f0ecc58462989871aaa82adfaa0fa64e9a4a560c1d18720
* Ligo-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=0b28f6db6ee702629a7810b132bc39296ad90dbf0c5607c2082a79c8ec8a5b4e
* Olamet-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=5b2af82111d499af2985549140dc5829b44ddba1b178d0e478984c17e2a98fd1
* Oohla-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=748567364e4b15907fc5a400fa853295de8ab8440131bc1c8ac329d9e1975736
* Pago-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=8a2df9e66a3426ff6dccc64167d8ca3e0a948f0ba99dfd1b3d02cddf4f9d7794
* Wego-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=445da84c672f54181b3b0940288e740373336d43986932153a0db247c777ed44
* Yobi-DingTalk-Robot
https://oapi.dingtalk.com/robot/send?access_token=8f02f1eab315f44fa70be5a303401ebc789b8a6eca1491e848233f31ad0f1d4c

java -jar ~/Downloads/bundletool-all-0.11.0.jar build-apks \
--bundle=./erban_client-google-release.aab \
--output=./erban_client-google-release.apks

java -jar ~/Downloads/bundletool-all-0.11.0.jar build-apks \
--bundle=./erban_client-google-release-compress-native.aab \
--output=./erban_client-google-release-compress-native.apks \
--ks=/Users/mac/DevStation/hi_android/Upfun-Android/upfun-debug.jks \
--ks-key-alias=my-alias \
--ks-pass=pass:UPFUN_2024 \
--key-pass=pass:UPFUN_2024


JAVA_HOME="/usr/local/opt/openjdk@8" apkanalyzer -h apk compare ./Lightchat_release.apk ./Lightchat_release2.apk | grep 'MB'



curl -X POST -H "Content-Type: application/json" -H "job:CookieCam-Android" -d "version:1.0.0.0 key:3846ef3f74a3a3200a007cf5a9c9a25e" http://tools.shijianline.cn:9004/ndklib/compile

curl -X POST https://tools.shijianline.cn:9004/ndklib/compile \
     -H "job: Gimme-Android" \
     -H "Content-Type: text/plain" \
     -d 'version:2.0.1.5 key:7b688dca5386f637eb773f46fbad1de1'


./gradlew sonarqube -Dsonar.projectKey=bobbo_lite_android -Dsonar.projectName='Bobbo-Lite-Android' -Dsonar.host.url=http://tools.shijianline.cn:9005/ -Dsonar.token=8bd8282be89093795199aafa1cf60b6d093d1791
