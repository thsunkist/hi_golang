#!/usr/bin/env bash
source /etc/profile
cd /srv/hi_golang/
status_pid=` ps aux |grep go |grep main|grep bin|grep -v grep |wc -l`
if [ $status_pid -eq 0 ];then
        echo > nohup.out
        nohup /opt/go/bin/go run main.go > nohup.out 2>&1 &
fi
#ps aux|grep hi_golang|grep -v grep
